<?php

namespace URL;

class Parser {
    private $url;
    private $parsed_url = false;

    public function __construct($url = false) {

        $this->url = $url;
        if($this->url != false) $this->parseUrl();
    }

    public function parseUrl(){
        $this->parsed_url = array_values(array_filter(explode('/', ltrim($this->url))));
    }

    public function getIndex($index = null) {
        return $this->parsed_url[$index] ?? null;
    }

    public function getUrl() {
        return ltrim($this->url, '/');
    }

    public function getParsedUrl() {
        return $this->parsed_url;
    }
}