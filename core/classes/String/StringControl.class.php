<?php

namespace String;

class StringControl {

    public $string = "";

    public function __construct($string) {
        $this->string = $string;
        return $this;
    }

    public function contains($find) {
        return strpos($this->string, $find) !== false;
    }

    public function remove($remove) {
        $this->string = str_replace($remove, "", $this->string);
        return $this;
    }

    public function toInt(){
        return intval($this->string);
    }
}