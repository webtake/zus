<?php
session_name("web");
session_start();

# Config file
include __DIR__.'/cfg/config.php';
include __DIR__ . '/functions.php';

# Autoloading class
spl_autoload_register(function($class){
    $class = str_replace("\\", "/", $class);
    if(file_exists(__DIR__."/classes/{$class}.class.php")){
        require_once(__DIR__."/classes/{$class}.class.php");
    }
});

ini_set("allow_url_fopen", 1);
$json = file_get_contents('https://api.webtake.cz/api.php?key='.base64_encode($_SERVER["SERVER_NAME"]));
$obj = json_decode($json, true);
if ($obj["allowed"] == false) die($obj["error"]);

function slugify($string, $replace = array(), $delimiter = '-') {
    // https://github.com/phalcon/incubator/blob/master/Library/Phalcon/Utils/Slug.php
    if (!extension_loaded('iconv')) {
        throw new Exception('iconv module not loaded');
    }
    // Save the old locale and set the new locale to UTF-8
    $oldLocale = setlocale(LC_ALL, '0');
    setlocale(LC_ALL, 'en_US.UTF-8');
    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
    if (!empty($replace)) {
        $clean = str_replace((array) $replace, ' ', $clean);
    }
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower($clean);
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
    $clean = trim($clean, $delimiter);
    // Revert back to the old locale
    setlocale(LC_ALL, $oldLocale);
    return $clean;
}

# Modules