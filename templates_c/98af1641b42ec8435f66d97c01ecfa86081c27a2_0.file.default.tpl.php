<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:21:48
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/modules/navbar/templates/default.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40ab9cad39f5_71238176',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '98af1641b42ec8435f66d97c01ecfa86081c27a2' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/modules/navbar/templates/default.tpl',
      1 => 1543783222,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c40ab9cad39f5_71238176 (Smarty_Internal_Template $_smarty_tpl) {
?>

<?php echo '<script'; ?>
>
    $(document).ready(function() {
        $('body').on('mouseenter mouseleave','.dropdown',function(e){
          var _d=$(e.target).closest('.dropdown');_d.addClass('show');
          setTimeout(function(){
            _d[_d.is(':hover')?'addClass':'removeClass']('show');
            $('[data-toggle="dropdown"]', _d).attr('aria-expanded',_d.is(':hover'));
          },300);
        });
    });
<?php echo '</script'; ?>
>
<nav class="navbar navbar-expand-lg navbar-dark text-dark">
    <div class="container">
        <a class="navbar-brand" href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;?>
">
            <img src="/layout/images/logo.png" alt="<?php echo $_smarty_tpl->tpl_vars['W']->value['projectName'];?>
" />
        </a>
        <button class="open-sidenav" onclick="openNavbar()"><span>&#9776;</span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['menu']->value, 'item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>
                    <?php if (isset($_smarty_tpl->tpl_vars['item']->value['items']) == false) {?>
                        <?php if ($_smarty_tpl->tpl_vars['item']->value['url'] != $_smarty_tpl->tpl_vars['HOME']->value) {?>
                            <li class="nav-item">
                                <a class="nav-link<?php if ($_smarty_tpl->tpl_vars['active']->value == $_smarty_tpl->tpl_vars['item']->value['url']) {?> active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['item']->value['url'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
</a>
                            </li>
                        <?php } else { ?>
                            <li class="nav-item">
                                <a class="nav-link<?php if ($_smarty_tpl->tpl_vars['active']->value == $_smarty_tpl->tpl_vars['item']->value['url']) {?> active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
"><span class="d-none d-md-block"><span class="fa fa-home"></span></span><span class="d-md-none d-block"><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
</span></a>
                            </li>
                        <?php }?>
                    <?php } else { ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="/<?php echo $_smarty_tpl->tpl_vars['item']->value['url'];?>
" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>

                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['item']->value['items'], 'sub');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['sub']->value) {
?>
                                <a class="dropdown-item" href="/<?php echo $_smarty_tpl->tpl_vars['sub']->value['url'];?>
"><?php echo $_smarty_tpl->tpl_vars['sub']->value['title'];?>
</a>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                            </div>
                        </li>
                    <?php }?>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

            </ul>
            <ul class="navbar-nav nav-langs">
                <li class="nav-item sep">&nbsp;</li>
                <?php if (count($_smarty_tpl->tpl_vars['langs']->value) > 1) {?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['langs']->value, 'lang');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['lang']->value) {
?>
                    <li class="nav-item lang">
                        <a class="nav-link<?php if ($_smarty_tpl->tpl_vars['_SESSION']->value['lang'] == $_smarty_tpl->tpl_vars['lang']->value['ID']) {?> active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;?>
language/<?php echo $_smarty_tpl->tpl_vars['lang']->value['ID'];?>
"><?php echo $_smarty_tpl->tpl_vars['lang']->value['code'];?>
</a>
                        
                    </li>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                <?php }?>
            </ul>
        </div>
    </div>
</nav>
<div class="sidenav">
    <button class="closebtn" onclick="closeNavbar()"><span>&times;</span></button>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['menu']->value, 'item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>
        <?php if (isset($_smarty_tpl->tpl_vars['item']->value['items']) == false) {?>
            <?php if ($_smarty_tpl->tpl_vars['item']->value['url'] != $_smarty_tpl->tpl_vars['HOME']->value) {?>
                <li class="nav-item">
                    <a class="nav-link<?php if ($_smarty_tpl->tpl_vars['active']->value == $_smarty_tpl->tpl_vars['item']->value['url']) {?> active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['item']->value['url'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
</a>
                </li>
            <?php } else { ?>
                <li class="nav-item">
                    <a class="nav-link<?php if ($_smarty_tpl->tpl_vars['active']->value == $_smarty_tpl->tpl_vars['item']->value['url']) {?> active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['item']->value['url'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
</a>
                </li>
            <?php }?>
        <?php } else { ?>
            <li class="nav-item">
                <a class="nav-link<?php if ($_smarty_tpl->tpl_vars['active']->value == $_smarty_tpl->tpl_vars['item']->value['url']) {?> active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['item']->value['url'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
</a>
            </li>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['item']->value['items'], 'sub');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['sub']->value) {
?>
                <li>
                <a class="nav-link<?php if ($_smarty_tpl->tpl_vars['active']->value == $_smarty_tpl->tpl_vars['sub']->value['url']) {?> active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['sub']->value['url'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['sub']->value['title'];?>
">~ <?php echo $_smarty_tpl->tpl_vars['sub']->value['title'];?>
</a>
                </li>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

        <?php }?>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

</div><?php }
}
