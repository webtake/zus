<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:27:09
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/modules/custom_page/templates/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40acdd024e40_47678247',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ef1903886055a16c16a75bcc6c92f625aace0980' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/modules/custom_page/templates/index.tpl',
      1 => 1540498928,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:index.tpl' => 1,
  ),
),false)) {
function content_5c40acdd024e40_47678247 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14770661545c40acdcf41644_01330321', "title");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15232714295c40acdd0240e0_31821657', "content");
$_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:index.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block "title"} */
class Block_14770661545c40acdcf41644_01330321 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
echo $_smarty_tpl->tpl_vars['page']->value['title'];
}
}
/* {/block "title"} */
/* {block "content"} */
class Block_15232714295c40acdd0240e0_31821657 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    
    <section id="content">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['page']->value['blocks'], 'block', false, NULL, 'odstavce', array (
  'first' => true,
  'index' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['block']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_odstavce']->value['index']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_odstavce']->value['first'] = !$_smarty_tpl->tpl_vars['__smarty_foreach_odstavce']->value['index'];
?>
            <?php if ($_smarty_tpl->tpl_vars['block']->value['className'] != "odstavec-open" && $_smarty_tpl->tpl_vars['block']->value['className'] != "withImg") {?>
            <div id="odstavec_<?php echo $_smarty_tpl->tpl_vars['block']->value['ID'];?>
" class="odstavec_<?php echo $_smarty_tpl->tpl_vars['block']->value['ID'];
if ($_smarty_tpl->tpl_vars['block']->value['class'] != '') {?> <?php echo $_smarty_tpl->tpl_vars['block']->value['className'];
}?>">
                    <?php if ($_smarty_tpl->tpl_vars['block']->value['title'] != '') {?>
                    <h2><?php echo $_smarty_tpl->tpl_vars['block']->value['title'];?>
</h2>
                    <?php }?>
                <div class="<?php if ($_smarty_tpl->tpl_vars['block']->value['fullwidth'] == 0) {?>container<?php } else { ?>container-fluid<?php }?>">
                    <?php echo $_smarty_tpl->tpl_vars['block']->value['content'];?>

                </div>
            </div>
            <?php } elseif ($_smarty_tpl->tpl_vars['block']->value['className'] == "withImg") {?>
            <div id="odstavec_<?php echo $_smarty_tpl->tpl_vars['block']->value['ID'];?>
" class="odstavec_<?php echo $_smarty_tpl->tpl_vars['block']->value['ID'];
if ($_smarty_tpl->tpl_vars['block']->value['class'] != '') {?> <?php echo $_smarty_tpl->tpl_vars['block']->value['className'];
}?>" style="background-image: url('<?php echo $_smarty_tpl->tpl_vars['page']->value['og_img'];?>
')">
                <div class="<?php if ($_smarty_tpl->tpl_vars['block']->value['fullwidth'] == 0) {?>container<?php } else { ?>container-fluid<?php }?>">
                    <?php echo $_smarty_tpl->tpl_vars['block']->value['content'];?>

                </div>
                <button onClick="goDown('odstavec_<?php echo $_smarty_tpl->tpl_vars['block']->value['ID'];?>
')" class="btn-primary btn-fontawesome">
                    <span class="fa fa-angle-down"></span>
                </button>
            </div>
            <?php } elseif ($_smarty_tpl->tpl_vars['block']->value['className'] == "odstavec-open") {?>
            <div id="odstavec_<?php echo $_smarty_tpl->tpl_vars['block']->value['ID'];?>
" class="odstavec_<?php echo $_smarty_tpl->tpl_vars['block']->value['ID'];?>
 odstavec-open" id="accordion">
                <div class="<?php if ($_smarty_tpl->tpl_vars['block']->value['fullwidth'] == 0) {?>container<?php } else { ?>container-fluid<?php }?>">
                    <div class="card">
                        <div class="card-header" id="nadpis<?php echo $_smarty_tpl->tpl_vars['block']->value['ID'];?>
">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#cenik<?php echo $_smarty_tpl->tpl_vars['block']->value['ID'];?>
" aria-expanded="<?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_odstavce']->value['first']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_odstavce']->value['first'] : null) == true) {?>true<?php } else { ?>false<?php }?>" aria-controls="cenik<?php echo $_smarty_tpl->tpl_vars['block']->value['ID'];?>
">
                                <h3 class="mb-0">
                                    <?php echo $_smarty_tpl->tpl_vars['block']->value['title'];?>
<br >
                                </h3>
                            </button>
                        </div>
                        <div id="cenik<?php echo $_smarty_tpl->tpl_vars['block']->value['ID'];?>
" class="collapse<?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_odstavce']->value['first']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_odstavce']->value['first'] : null) == true) {?> show<?php }?>" aria-labelledby="nadpis<?php echo $_smarty_tpl->tpl_vars['block']->value['ID'];?>
" data-parent="#accordion">
                            <div class="card-body">
                            <?php echo $_smarty_tpl->tpl_vars['block']->value['content'];?>

                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <?php }?>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    </section>
<?php
}
}
/* {/block "content"} */
}
