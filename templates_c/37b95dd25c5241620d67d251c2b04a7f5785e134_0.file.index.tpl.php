<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:21:48
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/modules/carousel/templates/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40ab9cada8e8_51130634',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '37b95dd25c5241620d67d251c2b04a7f5785e134' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/modules/carousel/templates/index.tpl',
      1 => 1543490664,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c40ab9cada8e8_51130634 (Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if (!empty($_smarty_tpl->tpl_vars['banner']->value)) {?>
    <div id="slider" class="owl-carousel">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['banner']->value, 'b');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['b']->value) {
?>
        <div class="w-100 text-center">
            <img src="<?php echo $_smarty_tpl->tpl_vars['b']->value['img'];?>
" alt="Banner" class="background sm-none" />
            <div class="container">
                <?php echo $_smarty_tpl->tpl_vars['b']->value['banner_text'];?>

            </div>
            <button onClick="goDown('slider')" class="btn-primary btn-fontawesome">
                    <span class="fa fa-angle-down"></span>
            </button>
        </div>            
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    </div>
    <?php echo '<script'; ?>
>
        $("#slider").owlCarousel({
            items: 1,
            nav: true,
            dots: false,
            loop: true
        });
    <?php echo '</script'; ?>
>
<?php }
}
}
