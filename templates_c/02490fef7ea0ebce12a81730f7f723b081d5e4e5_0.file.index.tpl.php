<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:21:48
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/modules/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40ab9caa79f7_70284548',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '02490fef7ea0ebce12a81730f7f723b081d5e4e5' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/modules/index.tpl',
      1 => 1543784008,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:navbar/templates/default.tpl' => 1,
    'file:carousel/templates/index.tpl' => 1,
  ),
),false)) {
function content_5c40ab9caa79f7_70284548 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_replace')) require_once '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/vendor/smarty/plugins/modifier.replace.php';
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!doctype html>
<html lang="cs">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title><?php echo $_smarty_tpl->tpl_vars['W']->value['projectName'];?>
 - <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12067221825c40ab9ca813d3_77604146', "title");
?>
</title>
    <?php if (mb_strtoupper($_smarty_tpl->tpl_vars['W']->value['development'], 'UTF-8') != "ANO") {?>
        <meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['og']->value['description'];?>
" />
        <meta name="keywords" content="<?php echo $_smarty_tpl->tpl_vars['og']->value['description'];?>
" />
        <link rel="sitemap" type="application/xml" title="Sitemap" href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;?>
sitemap.xml" />
        <link rel="canonical" href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['url']->value;?>
" />
        <meta name="robots" content="index, all" />
    <?php }?>
    <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;?>
favicon.ico" />
    <meta name="author" content="Webzmoravy.cz" />
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['stylesheet']->value, 'style');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['style']->value) {
?>
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['W']->value['CSS'];
echo $_smarty_tpl->tpl_vars['style']->value;?>
" />
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href='<?php echo $_smarty_tpl->tpl_vars['W']->value['VENDOR'];?>
lightbox/css/lightbox.min.css' rel='stylesheet' />
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['W']->value['VENDOR'];?>
owlcarousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['W']->value['VENDOR'];?>
owlcarousel/assets/owl.theme.default.min.css">
    <meta property="og:title" content="<?php echo $_smarty_tpl->tpl_vars['og']->value['title'];?>
" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['url']->value;?>
" />
    <meta property="og:image" content="<?php echo $_smarty_tpl->tpl_vars['og']->value['image'];?>
" />
    <meta property="og:description" content="<?php echo $_smarty_tpl->tpl_vars['og']->value['description'];?>
" />
    <?php if (isset($_smarty_tpl->tpl_vars['W']->value['googleAnalytics']) && $_smarty_tpl->tpl_vars['W']->value['googleAnalytics'] != '') {?>
    
        <?php echo '<script'; ?>
 async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $_smarty_tpl->tpl_vars['W']->value['googleAnalytics'];?>
"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', '<?php echo $_smarty_tpl->tpl_vars['W']->value['googleAnalytics'];?>
');
        <?php echo '</script'; ?>
>
    
    <?php }?>
</head>
<body id="website" <?php if ($_smarty_tpl->tpl_vars['active']->value != $_smarty_tpl->tpl_vars['HOME']->value) {?> class="podstranka <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['active']->value,'-','_');?>
"<?php }?>>
<?php if ($_smarty_tpl->tpl_vars['active']->value == $_smarty_tpl->tpl_vars['HOME']->value) {?>
<div class="se-pre-con"></div>
<?php }
echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['W']->value['VENDOR'];?>
jquery/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['W']->value['VENDOR'];?>
popper/umd/popper.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['W']->value['VENDOR'];?>
bootstrap/bootstrap.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src='<?php echo $_smarty_tpl->tpl_vars['W']->value['VENDOR'];?>
lightbox/js/lightbox.min.js'><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['W']->value['VENDOR'];?>
owlcarousel/owl.carousel.min.js"><?php echo '</script'; ?>
>
<?php $_smarty_tpl->_subTemplateRender("file:navbar/templates/default.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php if ($_smarty_tpl->tpl_vars['active']->value == $_smarty_tpl->tpl_vars['HOME']->value) {?>
    <?php $_smarty_tpl->_subTemplateRender("file:carousel/templates/index.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php }
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7003262505c40ab9caa33a5_96040717', "content");
?>

<footer>
    <div class="col-12">
        <?php echo $_smarty_tpl->tpl_vars['VARS']->value['footer'];?>

    </div>
</footer>
<?php if (isset($_smarty_tpl->tpl_vars['VARS']->value['popup']) && $_smarty_tpl->tpl_vars['VARS']->value['popup'] != '') {?>
<div class="modal fade" id="modal_popup" tabindex="-1" role="dialog" aria-labelledby="NoLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <?php echo $_smarty_tpl->tpl_vars['VARS']->value['popup'];?>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zavřít</button>
      </div>
    </div>
  </div>
</div>
<?php }?>

<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['W']->value['JS'];?>
custom.js"><?php echo '</script'; ?>
>
</body>
</html><?php }
/* {block "title"} */
class Block_12067221825c40ab9ca813d3_77604146 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block "title"} */
/* {block "content"} */
class Block_7003262505c40ab9caa33a5_96040717 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block "content"} */
}
