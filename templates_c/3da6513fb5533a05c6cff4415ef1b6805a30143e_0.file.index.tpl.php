<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:30:25
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/modules/form/templates/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40ada12b0981_01966109',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3da6513fb5533a05c6cff4415ef1b6805a30143e' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/modules/form/templates/index.tpl',
      1 => 1542152214,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c40ada12b0981_01966109 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('form', $_smarty_tpl->tpl_vars['formVars']->value);
if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
    <p class="alert alert-<?php echo $_smarty_tpl->tpl_vars['message']->value['type'];?>
"><?php echo $_smarty_tpl->tpl_vars['message']->value['text'];?>
</p>
<?php }
$_smarty_tpl->_assignInScope('required', 0);
?>
<form id="contactForm_<?php echo $_smarty_tpl->tpl_vars['form']->value['ID'];?>
" method="post" action="#">
    <input type="hidden" name="form_id" value="<?php echo $_smarty_tpl->tpl_vars['form']->value['ID'];?>
" />
    <div class="row">
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['form']->value['inputs'], 'input');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['input']->value) {
?>
        <div class="col-12">
            <div class="form-group">

        <?php if ($_smarty_tpl->tpl_vars['input']->value['type'] != "select" && $_smarty_tpl->tpl_vars['input']->value['type'] != "textarea" && $_smarty_tpl->tpl_vars['input']->value['type'] != "checkbox") {?>
            <label for="input_<?php echo $_smarty_tpl->tpl_vars['input']->value['ID'];?>
"><?php echo $_smarty_tpl->tpl_vars['input']->value['name'];
if ($_smarty_tpl->tpl_vars['input']->value['required'] == 1) {?> <span class="required">*</span> <?php $_smarty_tpl->_assignInScope('required', $_smarty_tpl->tpl_vars['required']->value+1);
}?></label>
            <input type="<?php echo $_smarty_tpl->tpl_vars['input']->value['type'];?>
" name="input_<?php echo $_smarty_tpl->tpl_vars['input']->value['ID'];?>
" id="input_<?php echo $_smarty_tpl->tpl_vars['input']->value['ID'];?>
" class="form-control"<?php if ($_smarty_tpl->tpl_vars['input']->value['required'] == 1) {?> required <?php }?> />
        <?php } elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == "select") {?>
            <label for="input_<?php echo $_smarty_tpl->tpl_vars['input']->value['ID'];?>
"><?php echo $_smarty_tpl->tpl_vars['input']->value['name'];
if ($_smarty_tpl->tpl_vars['input']->value['required'] == 1) {?> <span class="required">*</span> <?php $_smarty_tpl->_assignInScope('required', $_smarty_tpl->tpl_vars['required']->value+1);
}?></label>
            <select name="input_<?php echo $_smarty_tpl->tpl_vars['input']->value['ID'];?>
" id="input_<?php echo $_smarty_tpl->tpl_vars['input']->value['ID'];?>
" class="form-control" <?php if ($_smarty_tpl->tpl_vars['input']->value['required'] == 1) {?> required <?php }?>>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['input']->value['values'], 'value');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['value']->value['value'];?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value['value'];?>
</option>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

            </select>
        <?php } elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == "textarea") {?>
            <label for="input_<?php echo $_smarty_tpl->tpl_vars['input']->value['ID'];?>
"><?php echo $_smarty_tpl->tpl_vars['input']->value['name'];
if ($_smarty_tpl->tpl_vars['input']->value['required'] == 1) {?> <span class="required">*</span> <?php $_smarty_tpl->_assignInScope('required', $_smarty_tpl->tpl_vars['required']->value+1);
}?></label>
            <textarea name="input_<?php echo $_smarty_tpl->tpl_vars['input']->value['ID'];?>
" id="input_<?php echo $_smarty_tpl->tpl_vars['input']->value['ID'];?>
" rows="5" class="form-control" <?php if ($_smarty_tpl->tpl_vars['input']->value['required'] == 1) {?> required <?php }?>></textarea>
        <?php } elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == "checkbox") {?>
            <label for="input_<?php echo $_smarty_tpl->tpl_vars['input']->value['ID'];?>
">
            <input type="<?php echo $_smarty_tpl->tpl_vars['input']->value['type'];?>
" name="input_<?php echo $_smarty_tpl->tpl_vars['input']->value['ID'];?>
" id="input_<?php echo $_smarty_tpl->tpl_vars['input']->value['ID'];?>
" <?php if ($_smarty_tpl->tpl_vars['input']->value['required'] == 1) {?> required <?php }?> />
            <?php if ($_smarty_tpl->tpl_vars['input']->value['link'] != '') {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['input']->value['link'];?>
" target=_blank><?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>
</a>
                <?php } else { ?>
                <?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>

            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['input']->value['required'] == 1) {?> <span class="required">*</span> <?php $_smarty_tpl->_assignInScope('required', $_smarty_tpl->tpl_vars['required']->value+1);
}?></label>
        <?php } else { ?>
            <p>Nesprávný typ pole.</p>
        <?php }?>
            </div>
        </div>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    </div>

    <?php if ($_smarty_tpl->tpl_vars['required']->value > 0) {?>
    <div class="row">
        <div class="col-6">
        <div class="form-group">
            <span class="required">* - <?php echo preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['VARS']->value['required']);?>
</span>
        </div>
        </div>
        <div class="col-6">
            
    <div class="form-group text-right">
        <button class="btn btn-primary btn-big" name="send"><?php echo $_smarty_tpl->tpl_vars['form']->value['sendValue'];?>
</button>
    </div>
        </div>
    </div>
    <?php } else { ?>
    <div class="form-group">
        <button class="btn btn-primary btn-big" name="send"><?php echo $_smarty_tpl->tpl_vars['form']->value['sendValue'];?>
</button>
    </div>
    <?php }?>
</form><?php }
}
