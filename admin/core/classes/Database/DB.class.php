<?php

namespace Database;

use PDO;
use PDOException;
class DB {
    private $host = DB_HOST;
    private $user = DB_USER;
    private $pass = DB_PASS;
    private $dbname = DB_NAME;
    private $stmt;
    private $dbh;

    public function __construct(){

        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname . ';charset=utf8';

        $options = array(
            PDO::ATTR_PERSISTENT            => true,
            PDO::ATTR_ERRMODE               => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE    => PDO::FETCH_ASSOC
        );

        try{
            $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
        }
        catch(PDOException $e){
            $this->error = $e->getMessage();
            echo $this->error;
            die;
        }
        return $this;
    }

    public function query($query){
        $this->stmt = $this->dbh->prepare($query);
        return $this;
    }

    public function bind($param, $value, $type = null){
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
        return $this;
    }

    public function bindArray($array){
        foreach($array as $key => $value){
            $this->stmt->bindValue($key, $value);
        }
        return $this;
    }

    public function execute(){
        return $this->stmt->execute();
    }

    public function fetchAll(){
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function fetch(){
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function fetchColumn(){
        return $this->stmt->fetchColumn();
    }

    public function rowCount(){
        return $this->stmt->rowCount();
    }

    public function lastInsertId(){
        return $this->dbh->lastInsertId();
    }

    public function nextID($table){
        $q = $this->dbh->prepare("SELECT `AUTO_INCREMENT`
                                    FROM  INFORMATION_SCHEMA.TABLES
                                    WHERE TABLE_SCHEMA = :db
                                    AND   TABLE_NAME   = :table;");
        $q->bindValue(":db", $this->dbname);
        $q->bindValue(":table", $table);
        $q->execute();
        return $q->fetchColumn();

    }
}
/*
class DB {
    private $db;

    function __construct() {
        $this->connect_database();
    }

    public function getInstance() {
        return $this->db;
    }

    private function connect_database() {
        try {
            $connection_string = "mysql:dbname=".DB_NAME.";host=".DB_HOST.";charset=utf8";
            $connection_array = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            );

            $this->db = new PDO($connection_string, DB_USER, DB_PASS, $connection_array);
        }
        catch(PDOException $e) {
            $this->db = null;
        }
        return $this->db;
    }
} */
