<?php
    require_once __DIR__."/../cfg/config.php";
    require_once __DIR__."/../classes/Database/DB.class.php";
    
    $db = new Database\DB;
    
    if(isset($_POST["action"])){
        switch($_POST["action"]) {
            case "moveItem":
                $itemid = $_POST["item"];
                
                $item = $db->query("SELECT * FROM ".$_POST["table"]." WHERE ID = :id LIMIT 0, 1");
                $item->bind(":id", $itemid);
                $item->execute();
                if($item->rowCount() == 1) {
                    $item = $item->fetch();
                    
                    $go = true;

                    if($item["position"] == 0 && $_POST["step"] == -1) $go = false;

                    $query = $db->query("SELECT `position` FROM ".$_POST["table"]." ORDER BY `position` DESC LIMIT 0, 1");
                    $query->bind(":id", $page["sub_of"]);
                    $query->execute();

                    $lastPosition = $query->fetchColumn();

                    if($item["position"] == $lastPosition && $_POST["step"] == 1) $go = false;

                    if($go) {
                        $query = $db->query("UPDATE ".$_POST["table"]." SET `position` = :newpos WHERE `position` = :pos");
                        $query->bind(":newpos", $item["position"]);
                        $query->bind(":pos", $item["position"] + $_POST['step']);
                        $query->execute();

                        $query = $db->query("UPDATE ".$_POST["table"]." SET `position` = :pos WHERE ID = :id");
                        $query->bind(":pos", $item["position"] + $_POST['step']);
                        $query->bind(":id", $item["ID"]);
                        $query->execute();
                        $resposne = [
                            'success'   => true,
                            'message'   => "Položka byla přesunuta."
                        ];
                    }else{
                        $response = [
                            'success'   => false,
                            'message'   => "Položku nebylo možné přesunout.",
                        ];
                    }
                    
                }else{
                    $response = [
                        'success'   => false,
                        'message'   => "Položka neexistuje."
                    ];
                }
                echo json_encode($response);
                break;
            case "movePage":
                $pageid = $_POST["page"];
                
                $page = $db->query("SELECT ID, sub_of, `position` FROM pages WHERE ID = :id LIMIT 0, 1");
                $page->bind(":id", $pageid);
                $page->execute();
                if($page->rowCount() == 1) {
                    $page = $page->fetch();
                    
                    $go = true;
                    
                    /**
                     * Pokud již je první, nemůže posouvat výše.
                     */
                    if($page["position"] == 0 && $_POST["step"] == -1) $go = false;
                    
                    /**
                     * Výběr posledního ID pod daným SUB_OF
                     */
                    $query = $db->query("SELECT `position` FROM pages WHERE sub_of = :id ORDER BY `position` DESC LIMIT 0, 1");
                    $query->bind(":id", $page["sub_of"]);
                    $query->execute();
                    
                    $lastPosition = $query->fetchColumn();
                    
                    if($page["position"] == $lastPosition && $_POST["step"] == 1) $go = false;
                    
                    if($go) {
                        $query = $db->query("UPDATE pages SET `position` = :newpos WHERE `position` = :pos AND sub_of = :sub");
                        $query->bind(":newpos", $page["position"]);
                        $query->bind(":pos", $page["position"] + $_POST['step']);
                        $query->bind(":sub", $page["sub_of"]);
                        $query->execute();
                        
                        $query = $db->query("UPDATE pages SET `position` = :pos WHERE ID = :id");
                        $query->bind(":pos", $page["position"] + $_POST['step']);
                        $query->bind(":id", $page["ID"]);
                        $query->execute();
                        $resposne = [
                            'success'   => true,
                            'message'   => "Stránka byla přesunuta."
                        ];
                    }else{
                        $response = [
                            'success'   => false,
                            'message'   => "Stránku nebylo možné přesunout.",
                        ];
                    }
                }else{
                    $response = [
                        'success'   => false,
                        'message'   => "Stránka neexistuje."  
                    ];
                }
                echo json_encode($response);
                break;
        }
    }