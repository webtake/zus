<?php

# Database connection
require_once __DIR__.'/../../../core/cfg/connection.php';


# Web variables
define('ROOT', ((isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off")) ? "https://" : "http://").$_SERVER['HTTP_HOST'].'/admin/');
define('VENDOR', ROOT.'vendor/');
define('CSS', ROOT.'layout/css/');
define('JS', ROOT.'layout/js/');
define('IMAGES', ROOT.'layout/images/');

# Project settings
define('HOME', 'dashboard');