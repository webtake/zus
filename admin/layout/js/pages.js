function movePage(page_id, step) {
    $.ajax({
        url     :     "/admin/core/ajax/index.php",
        method  :     "POST",
        data    :     {
            action  : "movePage",
            page    : page_id,
            step    : step
        },
        success : function(data) {
            //var json = jQuery.parseJSON(data);
            //console.log(json.message);
            console.log(data.message);
            location.reload();
        }

    });
}
function moveItem(table, item_id, step) {
    $.ajax({
        url     :     "/admin/core/ajax/index.php",
        method  :     "POST",
        data    :     {
            action  : "moveItem",
            table   : table,
            item    : item_id,
            step    : step
        },
        success : function(data) {
            //var json = jQuery.parseJSON(data);
            //console.log(json.message);
            console.log(data.message);
            location.reload();
        }

    });
}





function changeOrder(table_name, item_id, prev_order, new_order){
    $.ajax({
        url: "/admin/layout/js/change.php",
        method: "POST",
        data:{
            table: table_name,
            ID: item_id,
            prevOrder: prev_order,
            newOrder: new_order
        },
        success: function(){
            location.reload();
        }
    });
}
function changeInputOrder(form_id, item_id, prev_order, new_order){
    $.ajax({
        url: "/admin/layout/js/changeInputOrder.php",
        method: "POST",
        data:{
            form: form_id,
            ID: item_id,
            prevOrder: prev_order,
            newOrder: new_order
        },
        success: function(){
            location.reload();
        }
    });
}
function removeContent(id) {
    $.ajax({
        type: "POST",
        url: "/admin/layout/js/ajax/removeContent.php",
        data: {
            blockID: id
        },
        success: function() {
            $('#id_' + id).empty();
            $('#id_' + id).remove();
        },
        error: function(){
            alert('Failed.');
        }
    });
}
function removeImage(id) {
    $.ajax({
        type: "POST",
        url: "/admin/layout/js/ajax/removeImage.php",
        data: {
            blockID: id
        },
        success: function() {
            $('#id_' + id).empty();
            $('#id_' + id).remove();
        },
        error: function(){
            alert('Failed.');
        }
    });
}
function swapContent(element1, updown) {
    var element = $(element1).closest(".cntnr");
    var id1 = $(element).attr("id");
    id1 = id1.replace("id_", "");
    var element2 = (updown == 0) ? element.closest(".cntnr").prev(".cntnr") : element.closest(".cntnr").next(".cntnr");
    var id2 = $(element2).attr("id");
    id2 = id2.replace("id_", "");
    var ckEditor1 = CKEDITOR.instances['odstavec_' + id1].getData();
    var ckEditor2 = CKEDITOR.instances['odstavec_' + id2].getData();
    if(updown == 1){
        $(element).before(element2);
    }else{
        $(element2).before(element);
    }
    CKEDITOR.remove(CKEDITOR.instances['odstavec_' + id1]);
    CKEDITOR.remove(CKEDITOR.instances['odstavec_' + id2]);
    $("#cke_odstavec_" + id1).remove();
    $("#cke_odstavec_" + id2).remove();
    CKEDITOR.replace('odstavec_' + id1, {
            contentsCss: [CKEDITOR.basePath + 'contents.css', '/layout/css/styles.css', CKEDITOR.basePath + 'bootstrap.min.css'],
            allowedContent: true
        });
    CKEDITOR.replace('odstavec_' + id2, {
            contentsCss: [CKEDITOR.basePath + 'contents.css', '/layout/css/styles.css', CKEDITOR.basePath + 'bootstrap.min.css'],
            allowedContent: true
        });
    CKEDITOR.instances['odstavec_' + id1].setData(ckEditor1);
    CKEDITOR.instances['odstavec_' + id2].getData(ckEditor2);
}
function addContent(page, index) {
    $.ajax({
        type: "POST",
        url: "/admin/layout/js/ajax/addContent.php",
        data: {
            page: page,
            index: index
        },
        dataType: 'json',
        success: function(data){
            var out = "<div id='id_" + data.block + "' class='cntnr'>" +
                "<div class='form-group'>" +
                "<div class='col-xs-12'>" +
                "<div class='col-md-6'>" +
                "<a onClick='addContent(" + data.page + ", " + data.block + ")' class='btn btn-primary'><span class='fa fa-plus'></span> Přidat nový odstavec na tuto pozici</a>" +
                " <a onClick='removeContent(" + data.block + ")' class='btn btn-danger'><span class='fa fa-trash'></span> Odstranit odstavec</a>" +
                "</div>" +
                "<div class='col-md-6 text-right'>" +
                "<a onClick='swapContent(this, 0);' class='btn btn-info'><fa class='fa fa-angle-up'></fa></a>" +
                " <a onClick='swapContent(this, 1);' class='btn btn-info'><fa class='fa fa-angle-down'></fa></a>" +
                "</div>" +
                "</div>" +
                "</div>" +
                "<div class='form-group'>" +
                "<label class='col-sm-2 control-label'>Nadpis odstavce</label>" +
                "<div class='col-md-5'>" +
                "<input type='text' class='form-control' name='title_" + data.block + "'>" +
                "</div>" +
                "</div>" +
                "<div class='form-group'>" +
                "<label class='col-sm-2 control-label'>Je odstavec přes celou šířku?</label>" +
                "<div class='col-md-5'>" +
                "<input type='checkbox' name='fullwidth_" + data.block + "' " + (data.fullwidth == 1 ? " checked" : "") + ">" +
                "</div>" +
                "</div>" +
                "<div class='form-group'>" +
                "<label class='col-sm-2 control-label'>Styl odstavce</label>" +
                "<div class='col-md-5'>" +
                "<select name='class_" + data.block + "' class='form-control'>";
            $(data.classes).each(function (index) {
                out += "<option value='" + data.classes[index]["ID"] + "'>" + data.classes[index]["name"] + "</option>";
            });
            out += "</select>" +
                "</div>" +
                "</div>" +
                "<div class='form-group'>" +
                "<div class='col-md-12'>" +
                "<div class='col-sm-12'>" +
                "<textarea id='odstavec_" + data.block + "' name='odstavec_" + data.block + "' class='ckeditor' cols='30' rows='10'></textarea>" +
                "</div>" +
                "</div>" +
                "</div>" +
                "<hr>" +
                "</div>";
            if(index != 0){
                $(out).insertBefore($("#id_" + index));
            }else{
                $(out).insertBefore($("#nonameblock"));
            }
            CKEDITOR.replace('odstavec_' + data.block, {
            contentsCss: [CKEDITOR.basePath + 'contents.css', '/layout/css/styles.css', CKEDITOR.basePath + 'bootstrap.min.css'],
            allowedContent: true
        });
        },
        error: function () {
            alert('Failed.');
        }
    });
}