<?php
session_name("admin");
session_start();

require_once __DIR__.'/../../core/cfg/config.php';
require_once __DIR__.'/../../core/classes/Database/DB.class.php';

$db = new Database\DB();

$table = $_POST["table"];
$id =  $_POST["ID"];
$old = $_POST["prevOrder"];
$new = $_POST["newOrder"];

$max = $db->query("SELECT MAX(position) FROM ".$table." WHERE lang=:lang");
$max->bind(":lang", $_SESSION["lang"]);
$max->execute();
$max = $max->fetchColumn();

if($new > 0 && $new <= $max){
    $chOrder = $db->query("UPDATE ".$table." SET position = :prevOrder WHERE position = :newOrder AND lang=:lang");
    $chOrder->bind(":prevOrder", $old);
    $chOrder->bind(":newOrder", $new);
    $chOrder->bind(":lang", $_SESSION["lang"]);
    $chOrder->execute();

    $chOrder = $db->query("UPDATE ".$table." SET position = :newOrder WHERE position = :prevOrder AND ID = :id AND lang=:lang");
    $chOrder->bind(":newOrder", $new);
    $chOrder->bind(":prevOrder", $old);
    $chOrder->bind(":id", $id);
    $chOrder->bind(":lang", $_SESSION["lang"]);
    $chOrder->execute();
}