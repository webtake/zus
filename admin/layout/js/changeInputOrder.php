<?php
session_name("admin");
session_start();

require_once __DIR__.'/../../core/cfg/config.php';
require_once __DIR__.'/../../core/classes/Database/DB.class.php';

$db = new Database\DB();

$form = $_POST["form"];
$id =  $_POST["ID"];
$old = $_POST["prevOrder"];
$new = $_POST["newOrder"];

$max = $db->query("SELECT MAX(position) FROM form_inputs WHERE form_id = :id");
$max->bind(":id", $form);
$max->execute();
$max = $max->fetchColumn();

if($new > 0 && $new <= $max){
    $chOrder = $db->query("UPDATE form_inputs SET position = :prevOrder WHERE position = :newOrder AND form_id = :form_id");
    $chOrder->bind(":prevOrder", $old);
    $chOrder->bind(":newOrder", $new);
    $chOrder->bind(":form_id", $form);
    $chOrder->execute();

    $chOrder = $db->query("UPDATE form_inputs SET position = :newOrder WHERE position = :prevOrder AND ID = :id");
    $chOrder->bind(":newOrder", $new);
    $chOrder->bind(":prevOrder", $old);
    $chOrder->bind(":id", $id);
    $chOrder->execute();
}