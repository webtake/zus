<?php
    $id = $_POST["blockID"];

    require_once __DIR__.'/../../../core/cfg/config.php';
    require_once __DIR__.'/../../../core/classes/Database/DB.class.php';

    $db = new Database\DB();
    $sql = $db->query("DELETE FROM photogalleries_images WHERE ID = :id");
    $str = "DELETE FROM photogalleries_images WHERE ID = :id";
    $sql->bind(":id", $id);
    $sql->execute();
?>