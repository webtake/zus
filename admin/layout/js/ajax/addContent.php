<?php
$page = $_POST["page"];
$index = $_POST["index"];

require_once __DIR__.'/../../../core/cfg/config.php';
require_once __DIR__.'/../../../core/classes/Database/DB.class.php';


$db = new Database\DB();

if($index != 0){
    $pos = $db->query("SELECT pos FROM content_blocks WHERE ID = :id");
    $pos->bind(":id", $index);
    $pos->execute();
    $pos = $pos->fetchColumn();

    $pos = ($pos == 0) ? 1 : $pos;
}else{
    $pos = $db->query("SELECT MAX(pos) FROM content_blocks WHERE page_id = :id");
    $pos->bind(":id", $page);
    $pos->execute();
    $pos = $pos->fetchColumn();
    $pos++;
}
$update = $db->query("UPDATE content_blocks SET pos = pos + 1 WHERE pos >= :pos");
$update->bind(":pos", $pos);
$update->execute();

$sql = $db->query("INSERT INTO content_blocks(page_id, pos) VALUES(:page, :pos)");
$sql->bind(":page", $page);
$sql->bind(":pos", $pos);
$sql->execute();

$paraID = $db->lastInsertId();

$data["block"] = $paraID;
$data["page"] = $page;


$allPages = $db->query("SELECT * FROM content_classes");
$allPages->execute();
$data["classes"] = $allPages->fetchAll();

echo json_encode($data);
?>