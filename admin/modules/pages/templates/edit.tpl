{if isset($save)}
    {if $save == true}
        <p class="alert alert-success">Uloženo.</p>
        {else}
        <p class="alert alert-danger">Nepovedlo se uložit.</p>
    {/if}
{/if}

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nastavení podstránky</a></li>
        <li><a href="#content" data-toggle="tab">Obsah</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Název podstránky</label>
                        <div class="col-md-5">
                            <input id="title" type="text" name="title" class="form-control" value="{$page.title}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="url" class="col-sm-2 control-label">URL podstránky</label>
                        <div class="col-md-5">
                            <input id="url" type="text" name="url" class="form-control" value="{$page.url}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="target_id" class="col-sm-2 control-label">Přesměrovat na</small></label>
                        <div class="col-md-5">
                            <input id="target_id" type="text" name="target_id" value="{$page.target_id}" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="display" class="col-sm-2 control-label">Zobrazit v menu</label>
                        <div class="col-md-5">
                            <input type="checkbox" class="form-check-input" name="display" id="display"{if $page.display == 1} checked{/if} />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-sm-2 control-label">Popis podstránky<br /><small class="text-muted">(pro vyhledávače)</small></label>
                        <div class="col-md-5">
                            <input id="description" type="text" name="meta_description" class="form-control" value="{$page.meta_description}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sub_of" class="col-sm-2 control-label">Zařadit do struktury</label>
                        <div class="col-md-5">
                            <select name="sub_of" id="sub_of" class="form-control">
                                <option value="0">Hlavní menu</option>
                                {foreach item=$p from=$structure}
                                    <option value="{$p.ID}" {if $page.sub_of == $p.ID} selected{/if}>{$p.title}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="keywords" class="col-sm-2 control-label">Klíčová slova<br /><small class="text-muted">(pro vyhledávače)</small></label>
                        <div class="col-md-5">
                            <input id="keywords" type="text" name="meta_keywords" class="form-control" value="{$page.meta_keywords}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="og_img" class="col-sm-2 control-label">Obrázek<br /><small class="text-muted">(zobrazí se při sdílení)</small></label>
                        <div class="col-md-5">
                            <img src="{$page.og_img}" alt="Obrázek" style="max-width: 100px;" />
                            <input id="og_img" type="file" name="og_img" class="form-control" /><br />
                            <a class="btn btn-info" href="{$ROOT}{$active}/edit/{$page.ID}/delete_image"><span class="fa fa-trash"></span> Odstranit obrázek</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="content">
                <div class="box-body">
                    <div class="col-xs-12">
                    {foreach item=$block from=$blocks}
                        <div id="id_{$block.ID}" class="cntnr" name="id_{$block.ID}">
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <div class="col-md-6">
                                        <a onClick="addContent({$page.ID}, {$block.ID})" class="btn btn-primary"><span class="fa fa-plus"></span> Přidat nový odstavec na tuto pozici</a>
                                        <a onClick="removeContent({$block.ID})" class="btn btn-danger"><span class="fa fa-trash"></span> Odstranit odstavec</a>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <a onClick="swapContent(this, 0);" class="btn btn-info"><fa class="fa fa-angle-up"></fa></a>
                                        <a onClick="swapContent(this, 1);" class="btn btn-info"><fa class="fa fa-angle-down"></fa></a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Nadpis odstavce</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="title_{$block.ID}" value="{$block.title}" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Je odstavec přes celou šířku?</label>
                                <div class="col-md-5">
                                    <input type="checkbox" name="fullwidth_{$block.ID}" {if $block.fullwidth == 1} checked{/if}>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Styl odstavce</label>
                                <div class="col-md-5">
                                    <select name="class_{$block.ID}" class="form-control">
                                        {foreach item=$style from=$classes}
                                            <option value="{$style.ID}"{if $style.ID == $block.class} selected{/if}>{$style.name}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-sm-12">
                                        <textarea name="odstavec_{$block.ID}" class="ckeditor" id="odstavec_{$block.ID}" cols="30" rows="10">{$block.content}</textarea>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                    {/foreach}
                        <div name="nonameblock" id="nonameblock">
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <a onClick="addContent({$page.ID}, 0)" class="btn btn-primary"><span class="fa fa-plus"></span> Přidat nový odstavec na tuto pozici</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div>