{extends "index.tpl"}
{block "title"}Podstránky{/block}
{block "content"}
    <section class="content-header">
        {if $state == "summary"}
            <h1>Podstránky</h1>
        {elseif $state == "new"}
            <h1>Nová podstránka</h1>
        {elseif $state == "edit"}
            <h1>Úprava podstránky</h1>
        {elseif $state == "delete"}
            <h1>Odstranění podstránky</h1>
        {/if}
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-body">

                        {if $state == "summary"}
                            <a href="{$ROOT}{$active}/new" class="btn btn-success"><i class="fa fa-plus"></i></a>
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="60%">Název podstránky</th>
                                        <th width="10%">Pořadí</th>
                                        <th width="5%"><span class="fa fa-eye"></span></th>
                                        <th width="10%">Akce</th>
                                        <th width="10%"></th>
                                    </tr>
                                    {foreach item=item from=$pages}
                                        <tr>
                                            <td width="5%">
                                                {$item.ID}
                                            </td>
                                            <td width="60%">
                                                {$item.title}
                                            </td>
                                            <td width="10%">
                                                <button class="btn btn-warning" onClick="movePage({$item.ID}, -1);"><span class="fa fa-caret-up"></span></button>
                                                <button class="btn btn-warning" onClick="movePage({$item.ID}, +1);"><span class="fa fa-caret-down"></span></button>
                                            </td>
                                            <td width="5%">
                                                {if $item.display == 1}
                                                    <span class="fa fa-check" style="color: green"></span>
                                                {else}
                                                    <span class="fa fa-times" style="color: red"></span>
                                                {/if}
                                            </td>
                                            <td width="10%">
                                                <a href="{$ROOT}{$active}/edit/{$item["ID"]}" class="btn btn-info btn-sm"><span class="fa fa-pencil-square-o"></span></a>
                                                <a href="{$ROOT}{$active}/delete/{$item["ID"]}" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></a>
                                            </td>
                                            <td width="10%">
                                                {if !empty($item.items)}
                                                    <button data-toggle="collapse" class="btn btn-secondary" data-target="#drop_{$item.ID}"><span class="fa fa-angle-down"></span></button>
                                                    </td></tr></table>
                                                {function name=printItems}
                                                    <div id="drop_{$fnitem.ID}" class="collapse">
                                                        <table class="table table-hover">
                                                        {foreach item=$sub from=$fnitem.items}
                                                            <tr style="background-color: rgba(0, 0, 0, .05); height: 24px;">
                                                                <td width="5%">
                                                                    {$sub.ID}
                                                                </td>
                                                                <td width="60%">
                                                                    {for $foo = 0 to $fnitem.level}&ndash;{/for}&nbsp;{$sub.title}
                                                                </td>
                                                                <td width="10%">
                                                                    <button class="btn btn-warning" onClick="movePage({$sub.ID}, -1);"><span class="fa fa-caret-up"></span></button>
                                                                    <button class="btn btn-warning" onClick="movePage({$sub.ID}, +1);"><span class="fa fa-caret-down"></span></button>
                                                                </td>
                                                                <td width="5%">
                                                                    {if $item.display == 1 && $sub.display == 1}
                                                                        <span class="fa fa-check" style="color: green"></span>
                                                                    {else}
                                                                        <span class="fa fa-times" style="color: red"></span>
                                                                    {/if}
                                                                </td>
                                                                <td width="10%">
                                                                    <a href="{$ROOT}{$active}/edit/{$sub["ID"]}" class="btn btn-info btn-sm"><span class="fa fa-pencil-square-o"></span></a>
                                                                    <a href="{$ROOT}{$active}/delete/{$sub["ID"]}" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></a>
                                                                </td>
                                                                <td width="10%">
                                                                    {if !empty($sub.items)}
                                                                        <button data-toggle="collapse" class="btn btn-secondary" data-target="#drop_{$sub.ID}"><span class="fa fa-angle-down"></span></button>
                                                                        </td></tr></table>
                                                                        {call name=printItems fnitem=$sub}
                                                                        <table class="table table-hover"><tr>
                                                                    {/if}
                                                                </td>
                                                            </tr>
                                                        {/foreach}
                                                        </table>
                                                    </div>
                                                {/function}
                                                {call name=printItems fnitem=$item}
                                <table class="table table-hover"><tr>
                                                {/if}
                                            </td>
                                        </tr>
                                    {/foreach}
                                </table>
                            </div>
                        {elseif $state == "new"}
                            {include "./new.tpl"}
                        {elseif $state == "edit"}
                            {include "./edit.tpl"}
                        {elseif $state == "delete"}
                            {include "./delete.tpl"}
                        {/if}
                    </div>
                </div>
            </div>

            {if $state == "summary"}
            <div class="col-md-3">
                <div class="box">
                    <div class="box-body">
                        <h4>Vyhledávání</h4>
                        <input type="text" id="search" placeholder="Zadejte hledaný název stránky" class="form-control" />
                    </div>
                </div>
            </div>
            {/if}
        </div>
    </section>
    
    <style>
        .table {
            margin-bottom: 0 !important;
        }
    </style>
{/block}