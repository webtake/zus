{if isset($save)}
    {if $save == true}
        <p class="alert alert-success">Uloženo</p>
    {else}
        <p class="alert alert-danger">Neuloženo.</p>
    {/if}
{/if}
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nastavení podstránky</a></li>
    </ul>
    <form action="/admin/pages/edit" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Název podstránky</label>
                        <div class="col-md-5">
                            <input id="title" type="text" name="title" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="url" class="col-sm-2 control-label">URL podstránky <br /><small class="text-muted">Pokud nevyplníte, vygeneruje se automaticky</small></label>
                        <div class="col-md-5">
                            <input id="url" type="text" name="url" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="target_id" class="col-sm-2 control-label">Přesměrovat na</small></label>
                        <div class="col-md-5">
                            <input id="target_id" type="text" name="target_id" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="display" class="col-sm-2 control-label">Zobrazit v menu</label>
                        <div class="col-md-5">
                            <input type="checkbox" class="form-check-input" name="display" id="display" checked />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-sm-2 control-label">Popis podstránky<br /><small class="text-muted">(pro vyhledávače)</small></label>
                        <div class="col-md-5">
                            <input id="description" type="text" name="meta_description" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sub_of" class="col-sm-2 control-label">Zařadit do struktury</label>
                        <div class="col-md-5">
                            <select name="sub_of" id="sub_of" class="form-control">
                                <option value="0">Hlavní menu</option>
                                {foreach item=$p from=$structure}
                                    <option value="{$p.ID}">{$p.title}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="keywords" class="col-sm-2 control-label">Klíčová slova<br /><small class="text-muted">(pro vyhledávače)</small></label>
                        <div class="col-md-5">
                            <input id="keywords" type="text" name="meta_keywords" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="og_img" class="col-sm-2 control-label">Obrázek<br /><small class="text-muted">(zobrazí se při sdílení)</small></label>
                        <div class="col-md-5">
                            <input id="og_img" type="file" name="og_img" class="form-control" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div>