<?php
require_once __DIR__.'/../../index.php';


$smarty = new Smarty;

$state = $url->getIndex(1) ?? "summary";

$db = new Database\DB();

$module = new Modules\Module($url->getIndex(0));

$allPages = [];
getThemAll2(0, "", $_SESSION["lang"], 0, $allPages);
$smarty->assign("structure", $allPages);


/*
$allPages = [];
$allPages = getThemAll(0, "", $_SESSION["lang"]);
$smarty->assign("structure", $allPages);
*/
$classes = $db->query("SELECT * FROM content_classes");
$classes->execute();
$classes = $classes->fetchAll();

$smarty->assign("classes", $classes);

switch($state){
    case "summary":
        $allPages = [];

        $allPages = getThemAll(0, "", $_SESSION["lang"]);        

        $smarty->assign("pages", $allPages);
        break;
    case "new":
        require __DIR__."/new.php";
        break;
    case "edit":
        require __DIR__."/edit.php";
        break;
    case "delete":
        require __DIR__."/delete.php";
        break;
    default:
        header("Location: ".ROOT."404");
}


$smarty->assign("state", $state);
$smarty->assign($default);
$smarty->assign('vars', get_defined_vars());
$smarty->display($module->template);
