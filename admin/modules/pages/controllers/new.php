<?php
$db = new Database\DB();


if(isset($_POST["save"])){
    $save = false;

    $newPos = $db->query("SELECT MAX(position) as p FROM pages WHERE lang=:lang AND sub_of = :sub_of");
    $newPos->bind(":lang", $_SESSION["lang"]);
    $newPos->bind(":sub_of", $_POST["sub_of"]);
    $newPos->execute();
    $newPos = $newPos->fetchColumn();
    if($newPos == null) $newPos = 0;
    else $newPos += 1;
    
    $sql = $db->query("INSERT INTO pages(title, url, meta_description, meta_keywords, og_description, og_title, sub_of, lang, `position`, display, target_id) 
                       VALUES(:title, :url, :md, :mk, :od, :ot, :sub_of, :lang, :newPos, :display, :target_id)");
    $sql->bind(":title", $_POST["title"]);

    $url = ($_POST["url"] == "") ? slugify($_POST["title"]) : $_POST["url"];
    $url = uniqueUrl($url, null);
    
    $sql->bind(":url", $url);
    $sql->bind(":md", ($_POST["meta_description"] != "") ? $_POST["meta_description"] : $_POST["title"]);
    $sql->bind(":mk", ($_POST["meta_keywords"] != "") ? $_POST["meta_keywords"] : $_POST["title"]);
    $sql->bind(":od", ($_POST["meta_description"] != "") ? $_POST["meta_description"] : $_POST["title"]);
    $sql->bind(":ot", $_POST["title"]);
    $sql->bind(":sub_of", $_POST["sub_of"]);
    $sql->bind(":lang", $_SESSION["lang"]);
    $sql->bind(":newPos", $newPos);
    $sql->bind(":display", isset($_POST["display"]) ? 1 : 0);
    $sql->bind(":target_id", $_SESSION["target_id"]);


    $save = $sql->execute();

    if(is_uploaded_file($_FILES["og_img"]["tmp_name"])){
        $target_dir = $module->uploadPath;
        $imageFileType = strtolower(pathinfo($_FILES["og_img"]["name"],PATHINFO_EXTENSION));
        $filename = time() . "." . $imageFileType;
        $target_file = $target_dir . $filename;

        $upload = false;
        if (move_uploaded_file($_FILES["og_img"]["tmp_name"], $target_file)) {
            $id = $db->lastInsertId();

            $sql = $db->query("UPDATE pages SET og_img = :img WHERE ID = :id");
            $sql->bind("img", "/images/uploads/".$filename);
            $sql->bind(":id", $id);
            $upload = $sql->execute();
        } else {
            $upload = false;
        }

        $save = ($save && $upload);
        if($save) {
            //header("Location: ".ROOT."admin/pages/edit/" . $db->lastInsertId());
        }
    }

    $smarty->assign("save", $save);
}