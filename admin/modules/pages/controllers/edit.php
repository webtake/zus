<?php
$db = new Database\DB();

$id = $url->getIndex(2);

$image = $url->getIndex(3);

$referer = substr($_SERVER['HTTP_REFERER'], strrpos($_SERVER['HTTP_REFERER'], '/') + 1);

if($referer == "new") {
    $save = true;
    $smarty->assign("save", $save);
}


if($image != false){
    $temp = $db->query("UPDATE pages SET og_img = '' WHERE ID = :id");
    $temp->bind(":id", $id);
    $temp->execute();
}

if($id == false){
    include __DIR__."/new.php";
    header("Location: /admin/pages/edit/".$db->lastInsertId());
}else{
    if(isset($_POST["save"])){
        $save = false;
        $sql = $db->query("UPDATE pages SET 
                                    title = :title, 
                                    url = :url,
                                    meta_description = :md, 
                                    meta_keywords = :mk, 
                                    og_description = :od, 
                                    og_title = :ot, 
                                    sub_of = :sub_of,
                                    display = :display,
                                    target_id = :target_id
                                    WHERE ID = :id");

        $sql->bind(":title", $_POST["title"]);

        $url = ($_POST["url"] == "") ? slugify($_POST["title"]) : $_POST["url"];
        $url = uniqueUrl($url, $id);
        $sql->bind(":url", $url);
        $sql->bind(":md", $_POST["meta_description"]);
        $sql->bind(":mk", $_POST["meta_keywords"]);
        $sql->bind(":od", $_POST["meta_description"]);
        $sql->bind(":ot", $_POST["title"]);
        $sql->bind(":sub_of", $_POST["sub_of"]);
        $sql->bind(":display", isset($_POST["display"]) ? 1 : 0);
        $sql->bind(":target_id", $_POST["target_id"]);
        $sql->bind(":id", $id);

        $save = $sql->execute();

        $blocks = prefixedItems($_POST, "odstavec_");

        $position = 0;

        foreach($blocks as $block => $value){
            $position++;
            $name = new String\StringControl($block);

            $blockID = $name->remove("odstavec_")->toInt();
            $content = $_POST["odstavec_".$blockID];
            $class = $_POST["class_".$blockID];
            $title = $_POST["title_".$blockID];
            $fullwidth = isset($_POST["fullwidth_".$blockID]);
            $update = $db->query("UPDATE content_blocks SET content = :content, class = :class, title = :title, pos = :position, fullwidth = :fullwidth WHERE ID = :id");
            $update->bind(":content", $content);
            $update->bind(":class", $class);
            $update->bind(":title", $title);
            $update->bind(":position", $position);
            $update->bind(":fullwidth", $fullwidth ? 1 : 0);
            $update->bind(":id", $blockID);

            $save = $update->execute();
        }

        if(is_uploaded_file($_FILES["og_img"]["tmp_name"])){
            $target_dir = $module->uploadPath;
            $imageFileType = strtolower(pathinfo($_FILES["og_img"]["name"],PATHINFO_EXTENSION));
            $filename = time() . "." . $imageFileType;
            $target_file = $target_dir . $filename;
            $upload = false;
            if (move_uploaded_file($_FILES["og_img"]["tmp_name"], $target_file)) {
                $sql = $db->query("UPDATE pages SET og_img = :img WHERE ID = :id");
                $sql->bind(":img", "/images/uploads/".$filename);
                $sql->bind(":id", $id);
                $upload = $sql->execute();
            } else {
                $upload = false;
            }

            $save = ($save && $upload);
        }

        $smarty->assign("save", $save);
    }

    $sql = $db->query("SELECT * FROM pages WHERE ID = :id");
    $sql->bind(":id", $id);

    $sql->execute();

    if($sql->rowCount() == 1){
        $page = $sql->fetch();
        $smarty->assign("page", $page);
        $blocks = $db->query("SELECT * FROM content_blocks WHERE page_id = :id ORDER BY pos ASC");
        $blocks->bind(":id", $id);
        $blocks->execute();
        $blocks = $blocks->fetchAll();
        $smarty->assign("blocks", $blocks);
    }else{
        header("Location: ".ROOT."404");
    }
}
