{if isset($save)}
    {if $save == true}
        <p class="alert alert-success">Uloženo</p>
    {else}
        <p class="alert alert-danger">Neuloženo.</p>
    {/if}
{/if}
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nastavení modulu</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="module_title" class="col-sm-2 control-label">Název modulu</label>
                        <div class="col-md-5">
                            <input id="module_title" type="text" name="module_title" value="{$page.module_title}" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="module_folder" class="col-sm-2 control-label">Složka modulu</label>
                        <div class="col-md-5">
                            <input id="module_folder" type="text" name="module_folder" value="{$page.module_folder}" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="level" class="col-sm-2 control-label">Úroveň</label>
                        <div class="col-md-5">
                            <input id="level" type="number" name="level" min="0" value="{$page.level}" class="form-control" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div>