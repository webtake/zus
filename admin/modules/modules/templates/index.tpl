{extends "index.tpl"}
{block "title"}Nastavení modulů{/block}
{block "content"}
    <section class="content-header">
        {if $state == "summary"}
            <h1>Nastavení modulů</h1>
        {elseif $state == "edit"}
            <h1>Úprava modulu</h1>
        {/if}
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    {if $state == "summary"}
                        <a href="{$ROOT}{$active}/new" class="btn btn-success"><i class="fa fa-plus"></i></a>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>#</th>
                                    <th>Název modulu</th>
                                    <th>Složka modulu</th>
                                    <th>Minimální úroveň</th>
                                    <th>Akce</th>
                                </tr>
                                {foreach item=$item from=$pages}
                                    <tr>
                                        <td>{$item.ID}</td>
                                        <td>{$item.module_title}</td>
                                        <td>{$item.module_folder}</td>
                                        <td>{$item.level}</td>
                                        <td>
                                            <a href="{$ROOT}{$active}/edit/{$item["ID"]}" class="btn btn-info btn-sm"><span class="fa fa-pencil-square-o"></span></a>
                                        </td>
                                    </tr>
                                {/foreach}
                            </table>
                        </div>
                    {elseif $state == "edit"}
                        {include "./edit.tpl"}
                    {/if}
                </div>
            </div>
        </div>
    </section>
{/block}