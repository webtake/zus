<?php
$db = new Database\DB();

$id = $url->getIndex(2);
if(isset($_POST["save"])){
    $save = false;

    $sql = $db->query("UPDATE admin_modules SET 
                                module_title = :module_title,
                                module_folder = :module_folder,
                                level = :level
                                WHERE ID = :id");

    $sql->bind(":module_title", $_POST["module_title"]);
    $sql->bind(":module_folder", $_POST["module_folder"]);
    $sql->bind(":level", $_POST["level"]);

    $sql->bind(":id", $id);

    $save = $sql->execute();

    $smarty->assign("save", $save);
}

$sql = $db->query("SELECT * FROM admin_modules WHERE `show` = 1 AND ID = :id");
$sql->bind(":id", $id);

$sql->execute();

if($sql->rowCount() == 1){
    $page = $sql->fetch();
    $smarty->assign("page", $page);

}else{
    header("Location: ".ROOT."404");
}