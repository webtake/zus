<?php
require_once __DIR__.'/../../index.php';

$smarty = new Smarty;

$state = $url->getIndex(1) ?? "summary";

$db = new Database\DB();

$module = new Modules\Module($url->getIndex(0));

switch($state){
    case "summary":
        $sql = $db->query("SELECT * FROM admin_modules WHERE `show` = 1 ORDER BY module_title ASC");
        $sql->execute();
        $smarty->assign("pages", $sql->fetchAll());
        break;
    case "edit":
        require __DIR__."/edit.php";
        break;
    default:
        header("Location: ".ROOT."404");
}


$smarty->assign("state", $state);
$smarty->assign($default);
$smarty->assign('vars', get_defined_vars());
$smarty->display($module->template);
