<?php
$db = new Database\DB();


if(isset($_POST["save"])){
    $save = false;
    $sql = $db->query("INSERT INTO content_classes(name, class) 
                       VALUES(:name, :class)");
    $sql->bind(":name", $_POST["title"]);
    $sql->bind(":class", $_POST["class"]);

    $save = $sql->execute();

    $smarty->assign("save", $save);
}