<?php
$db = new Database\DB();

$id = $url->getIndex(2);

if(isset($_POST["save"])){
    $save = false;

    $sql = $db->query("UPDATE content_classes SET 
                                name = :title,
                                class = :class
                                WHERE ID = :id");

    $sql->bind(":title", $_POST["title"]);
    $sql->bind(":class", $_POST["class"]);

    $sql->bind(":id", $id);

    $save = $sql->execute();

    $smarty->assign("save", $save);
}

$sql = $db->query("SELECT * FROM content_classes WHERE ID = :id");
$sql->bind(":id", $id);

$sql->execute();

if($sql->rowCount() == 1){
    $page = $sql->fetch();
    $smarty->assign("page", $page);
}else{
    header("Location: ".ROOT."404");
}