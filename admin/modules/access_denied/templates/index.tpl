{extends "index.tpl"}
{block "title"}Přístup zamítnut!{/block}
{block "content"}
    <section class="content-header">
            <h1>Přístup zamítnut!</h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <p class="alert danger">Pro přístup na tuto stránku nemáte dostatečná oprávnění. Kontaktujte správce.</p>
                </div>
            </div>
        </div>
    </section>
{/block}