<?php
require __DIR__.'/../../index.php';

$db = new Database\DB();

$module = new Modules\Module($url->getIndex(0));
$smarty = new Smarty;

$smarty->assign($default);
$smarty->assign('vars', get_defined_vars());
$smarty->display($module->template);