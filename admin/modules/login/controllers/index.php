<?php
if(isset($_SESSION["user"]) && $_SESSION["user"]["fullname"] != "") header("Location:".ROOT);
$url    = new URL\Parser(str_replace("admin/", "", $_SERVER["REQUEST_URI"]));

$module = new Modules\Module("login");

$url->getParsedUrl();
$active = $url->getIndex(0);
if($url == false) $active = HOME;


$webVariables = [
    "CSS"       => CSS,
    "IMAGES"    => IMAGES,
    "JS"        => JS,
    "VENDOR"    => VENDOR
];

$db = new Database\DB();

$w_query = $db->query("SELECT * FROM settings");
$w_query->execute();
$w_query = $w_query->fetchAll();

foreach($w_query as $s){
    $webVariables[$s["rule"]] =  $s["value"];
}

$default = [
    "ROOT"          => ROOT,
    "active"        => $active,
    "url"           => $url,
    "W"             => $webVariables
];

$smarty = new Smarty;


if(isset($_POST["login"])){
    $username = $_POST["username"];
    $password = $_POST["password"];

    $hash = hash("sha512", $password);

    $db = $db->query("SELECT * FROM users WHERE username = :username AND password = :password");
    $db->bind(":username", $username);
    $db->bind(":password", $hash);
    $db->execute();
    if($db->rowCount() == 1){
        $_SESSION["user"] = $db->fetch();
        $_SESSION["lang"] = 1;
        header("Location: ".ROOT);
    }else{
        $smarty->assign("error", "Špatné přihlašovací údaje");
    }

}



$smarty->assign($default);

$smarty->assign('vars', get_defined_vars());

$smarty->display($module->template);
