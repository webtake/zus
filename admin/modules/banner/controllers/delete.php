<?php
$db = new Database\DB();

$id = $url->getIndex(2);

if(isset($_POST["delete"])) {
    $delete = $db->query("DELETE FROM banner WHERE ID = :id");
    $delete->bind(":id", $id);
    $success = $delete->execute();
    $smarty->assign("success", $success);
}