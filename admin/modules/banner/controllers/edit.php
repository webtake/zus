<?php
$db = new Database\DB();

$id = $url->getIndex(2);

if(isset($_POST["save"])){
    $save = false;
    $sql = $db->query("UPDATE banner SET 
                                banner_text = :text 
                                WHERE ID = :id");

    $sql->bind(":text", $_POST["page_text"]);
    $sql->bind(":id", $id);

    $save = $sql->execute();
    if(is_uploaded_file($_FILES["og_img"]["tmp_name"])){
        $target_dir = $module->uploadPath;
        $imageFileType = strtolower(pathinfo($_FILES["og_img"]["name"],PATHINFO_EXTENSION));
        $filename = time() . "." . $imageFileType;
        $target_file = $target_dir . $filename;

        $upload = false;
        if (move_uploaded_file($_FILES["og_img"]["tmp_name"], $target_file)) {
            $sql = $db->query("UPDATE banner SET img = :img WHERE ID = :id");
            $sql->bind(":img", "/images/uploads/".$filename);
            $sql->bind(":id", $id);
            $upload = $sql->execute();
        } else {
            $upload = false;
        }

        $save = ($save && $upload);
    }

    $smarty->assign("save", $save);
}

$sql = $db->query("SELECT * FROM banner WHERE ID = :id");
$sql->bind(":id", $id);

$sql->execute();

if($sql->rowCount() == 1){
    $page = $sql->fetch();
    $smarty->assign("page", $page);
}else{
    header("Location: ".ROOT."404");
}