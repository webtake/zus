<?php
$db = new Database\DB();


if(isset($_POST["save"])){
    $save = false;
    $sql = $db->query("INSERT INTO banner(banner_text, lang) 
                       VALUES(:text, :lang)");
    $sql->bind(":text", $_POST["page_text"]);
    $sql->bind(":lang", $_SESSION["lang"]);

    $save = $sql->execute();

    if(is_uploaded_file($_FILES["og_img"]["tmp_name"])){
        $target_dir = $module->uploadPath;
        $imageFileType = strtolower(pathinfo($_FILES["og_img"]["name"],PATHINFO_EXTENSION));
        $filename = time() . "." . $imageFileType;
        $target_file = $target_dir . $filename;

        $upload = false;
        if (move_uploaded_file($_FILES["og_img"]["tmp_name"], $target_file)) {
            $id = $db->lastInsertId();

            $sql = $db->query("UPDATE banner SET img = :img WHERE ID = :id");
            $sql->bind("img", "/images/uploads/".$filename);
            $sql->bind(":id", $id);
            $upload = $sql->execute();
        } else {
            $upload = false;
        }

        $save = ($save && $upload);
    }

    $smarty->assign("save", $save);
}