<?php
require_once __DIR__.'/../../index.php';

$smarty = new Smarty;

$state = $url->getIndex(1) ?? "summary";

$db = new Database\DB();

$module = new Modules\Module($url->getIndex(0));

switch($state){
    case "summary":
        $sql = $db->query("SELECT * FROM banner WHERE lang = :lang ORDER BY ID ASC");
        $sql->bind(":lang", $_SESSION["lang"]);
        $sql->execute();
        $smarty->assign("pages", $sql->fetchAll());
        break;
    case "new":
        require __DIR__."/new.php";
        break;
    case "edit":
        require __DIR__."/edit.php";
        break;
    case "delete":
        require __DIR__."/delete.php";
        break;
    default:
        header("Location: ".ROOT."404");
}


$smarty->assign("state", $state);
$smarty->assign($default);
$smarty->assign('vars', get_defined_vars());
$smarty->display($module->template);
