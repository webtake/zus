{if isset($save)}
    {if $save == true}
        <p class="alert alert-success">Uloženo</p>
    {else}
        <p class="alert alert-danger">Neuloženo.</p>
    {/if}
{/if}
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nastavení banneru</a></li>
        <li><a href="#content" data-toggle="tab">Obsah</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="og_img" class="col-sm-2 control-label">Obrázek<br /><small class="text-muted">(Doporučené rozlišení: 1920x800)</small></label>
                        <div class="col-md-5">
                            <input id="og_img" type="file" name="og_img" class="form-control" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="content">
                <div class="box-body">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label for="ckeditor" class="label-control col-sm-12">Text v banneru</label>
                            <div class="col-sm-12">
                                <textarea name="page_text" id="ckeditor" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div>