{if isset($save)}
    {if $save == true}
        <p class="alert alert-success">Uloženo.</p>
        {else}
        <p class="alert alert-danger">Nepovedlo se uložit.</p>
    {/if}
{/if}
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li><a href="#settings" data-toggle="tab">Nastavení formuláře</a></li>
        <li class="active"><a href="#inputs" data-toggle="tab">Vstupy formuláře</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Název formuláře</label>
                        <div class="col-md-5">
                            <input id="title" type="text" name="name" class="form-control" value="{$page.name}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Název tlačítka pro odeslání</label>
                        <div class="col-md-5">
                            <input id="title" type="text" name="sendValue" class="form-control" value="{$page.sendValue}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="variable" class="col-sm-2 control-label">Zpráva po úspěšném odeslání formuláře</label>
                        <div class="col-md-5">
                            <input id="variable" type="text" name="successMessage" class="form-control" value="{$page.successMessage}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="columns" class="col-sm-2 control-label">Počet sloupců formuláře</label>
                        <div class="col-md-5">
                            <input type="number" id="columns" name="columns" value="{$page.columns}" min="1" max="3" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane active" id="inputs">
                <div class="box-body">
                    <a href="{$ROOT}{$active}/new-input/{$page.ID}" class="btn btn-success"><i class="fa fa-plus"></i></a>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Název vstupu</th>
                                    <th>Pořadí</th>
                                    <th>Akce</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach item=$input from=$inputs}
                                    <tr>
                                        <td>
                                            {$input.ID}
                                        </td>
                                        <td>
                                            {$input.name}
                                        </td>
                                        <td>
                                            <button class="btn btn-warning" onClick="changeInputOrder({$page.ID}, {$input.ID}, {$input.position}, {$input.position-1});"><span class="fa fa-caret-up"></span></button>
                                            <button class="btn btn-warning" onClick="changeInputOrder({$page.ID}, {$input.ID}, {$input.position}, {$input.position+1});"><span class="fa fa-caret-down"></span></button>
                                        </td>
                                        <td>
                                            <a href="{$ROOT}{$active}/edit-input/{$input["ID"]}" class="btn btn-info btn-sm"><span class="fa fa-pencil-square-o"></span></a>
                                            <a href="{$ROOT}{$active}/delete-input/{$input["ID"]}" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></a>
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div>