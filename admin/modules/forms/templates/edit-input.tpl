{if isset($save)}
    {if $save == true}
        <p class="alert alert-success">Uloženo</p>
    {else}
        <p class="alert alert-danger">Neuloženo.</p>
    {/if}
{/if}
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nastavení vstupu</a></li>
        <li><a href="#values" data-toggle="tab">Hodnoty (pouze pro výběrový typ)</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Název vstupu</label>
                        <div class="col-md-5">
                            <input id="name" type="text" name="name" class="form-control" value="{$page.name}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cz_name" class="col-sm-2 control-label">Název vstupu pro email</label>
                        <div class="col-md-5">
                            <input id="cz_name" type="text" name="cz_name" class="form-control" value="{$page.cz_name}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="required" class="col-sm-2 control-label">Je pole povinné?</label>
                        <div class="col-md-5">
                            <input id="required" type="checkbox" name="required" {if $page.required == 1} checked{/if} />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="type" class="col-sm-2 control-label">Typ vstupu</label>
                        <div class="col-md-5">
                            <select name="type" id="type" class="form-control">
                                <option value="text"{if $page.type == "text"} selected{/if}>Krátký text</option>
                                <option value="email"{if $page.type == "email"} selected{/if}>E-mailová adresa</option>
                                <option value="number"{if $page.type == "number"} selected{/if}>Číslo (! neplatí pro telefon !)</option>
                                <option value="select"{if $page.type == "select"} selected{/if}>Výběr mezi hodnotami</option>
                                <option value="textarea"{if $page.type == "textarea"} selected{/if}>Dlouhý text (např. poznámky atd.)</option>
                                <option value="checkbox"{if $page.type == "checkbox"} selected{/if}>Zaškrtávací políčko</option>
                                <option value="date"{if $page.type == "date"} selected{/if}>Datum</option>
                                <option value="time"{if $page.type == "time"} selected{/if}>Čas</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="values">
                <div class="box-body">
                    <a href="{$ROOT}{$active}/new-value/{$page.ID}" class="btn btn-success"><i class="fa fa-plus"></i></a>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Hodnota</th>
                                <th>Akce</th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach item=$value from=$values}
                                <tr>
                                    <td>
                                        {$value.ID}
                                    </td>
                                    <td>
                                        {$value.value}
                                    </td>
                                    <td>
                                        <a href="{$ROOT}{$active}/edit-value/{$value["ID"]}" class="btn btn-info btn-sm"><span class="fa fa-pencil-square-o"></span></a>
                                        <a href="{$ROOT}{$active}/delete-value/{$value["ID"]}" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></a>
                                    </td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div>