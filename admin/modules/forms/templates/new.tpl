{if isset($save)}
    {if $save == true}
        <p class="alert alert-success">Uloženo</p>
    {else}
        <p class="alert alert-danger">Neuloženo.</p>
    {/if}
{/if}
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nastavení formuláře</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Název formuláře</label>
                        <div class="col-md-5">
                            <input id="title" type="text" name="name" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Název tlačítka pro odeslání</label>
                        <div class="col-md-5">
                            <input id="title" type="text" name="sendValue" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="variable" class="col-sm-2 control-label">Zpráva po úspěšném odeslání formuláře</label>
                        <div class="col-md-5">
                            <input id="variable" type="text" name="successMessage" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="columns" class="col-sm-2 control-label">Počet sloupců formuláře</label>
                        <div class="col-md-5">
                            <input type="number" id="columns" name="columns" min="1" max="3" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div>