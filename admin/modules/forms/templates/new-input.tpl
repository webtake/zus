{if isset($save)}
    {if $save == true}
        <p class="alert alert-success">Uloženo</p>
    {else}
        <p class="alert alert-danger">Neuloženo.</p>
    {/if}
{/if}
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nastavení vstupu</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Název vstupu</label>
                        <div class="col-md-5">
                            <input id="name" type="text" name="name" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cz_name" class="col-sm-2 control-label">Název vstupu pro email</label>
                        <div class="col-md-5">
                            <input id="cz_name" type="text" name="cz_name" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="required" class="col-sm-2 control-label">Je pole povinné?</label>
                        <div class="col-md-5">
                            <input id="required" type="checkbox" name="required" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="type" class="col-sm-2 control-label">Typ vstupu</label>
                        <div class="col-md-5">
                            <select name="type" id="type" class="form-control">
                                <option value="text">Krátký text</option>
                                <option value="email">E-mailová adresa</option>
                                <option value="number">Číslo (! neplatí pro telefon !)</option>
                                <option value="select">Výběr mezi hodnotami</option>
                                <option value="textarea">Dlouhý text (např. poznámky atd.)</option>
                                <option value="checkbox">Zaškrtávací políčko</option>
                                <option value="date">Datum</option>
                                <option value="time">Čas</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div>