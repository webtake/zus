{extends "index.tpl"}
{block "title"}Formuláře{/block}
{block "content"}
    <section class="content-header">
        {if $state == "summary"}
            <h1>Všechny formuláře</h1>
        {elseif $state == "new"}
            <h1>Nový formulář</h1>
        {elseif $state == "edit"}
            <h1>Úprava formuláře</h1>
        {elseif $state == "delete"}
            <h1>Odstranění formuláře</h1>
        {/if}
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    {if $state == "summary"}
                        <a href="{$ROOT}{$active}/new" class="btn btn-success"><i class="fa fa-plus"></i></a>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>#</th>
                                    <th>Název formuláře</th>
                                    <th>Využití na podstránce</th>
                                    <th>Akce</th>
                                </tr>
                                {foreach item=$item from=$pages}
                                    <tr>
                                        <td>{$item.ID}</td>
                                        <td>{$item.name}</td>
                                        <td>[[form_{$item.code_name}]]</td>
                                        <td>
                                            <a href="{$ROOT}{$active}/edit/{$item["ID"]}" class="btn btn-info btn-sm"><span class="fa fa-pencil-square-o"></span></a>
                                            <a href="{$ROOT}{$active}/delete/{$item["ID"]}" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></a>
                                        </td>
                                    </tr>
                                    {foreachelse}
                                    <tr>
                                        <td colspan="3">Žádné formuláře nenalezeny. Kliknutím na zelené <strong>+</strong> vytvořte formulář.</td>
                                    </tr>
                                {/foreach}
                            </table>
                        </div>
                    {elseif $state == "new"}
                        {include "./new.tpl"}
                    {elseif $state == "edit"}
                        {include "./edit.tpl"}
                    {elseif $state == "delete"}
                        {include "./delete.tpl"}
                    {elseif $state == "new-input"}
                        {include "./new-input.tpl"}
                    {elseif $state == "edit-input"}
                        {include "./edit-input.tpl"}
                    {elseif $state == "delete-input"}
                        {include "./delete-input.tpl"}
                    {elseif $state == "new-value"}
                        {include "./new-value.tpl"}
                    {elseif $state == "edit-value"}
                        {include "./edit-value.tpl"}
                    {elseif $state == "delete-value"}
                        {include "./delete-value.tpl"}
                    {/if}
                </div>
            </div>
        </div>
    </section>
{/block}