{if isset($success)}
    {if $success == true}
        <p class="alert alert-success">Odstraněno.</p>
    {else}
        <p class="alert alert-danger">Nepovedlo se odstranit.</p>
    {/if}
    {else}

    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <p>Opravdu chcete odstranit formulář <strong>{$page.name}</strong>?</p>
        <button class="btn btn-danger" name="delete"><span class="fa fa-trash"></span> Odstranit</button>
        <a href="{$ROOT}{$active}" class="btn btn-primary btn-flat">Vrátit se zpátky</a>
    </form>
{/if}