<?php
$db = new Database\DB();

$id = $url->getIndex(2);

if(isset($_POST["save"])){
    $save = false;

    $sql = $db->query("INSERT INTO form_values (input_id, value) 
                                VALUES(:input_id, :value)");

    $sql->bind(":input_id", $id);
    $sql->bind(":value", $_POST["value"]);

    $save = $sql->execute();

    $smarty->assign("save", $save);

    if($save){
        header("Location: /admin/forms/edit-input/$id#values");
    }
}