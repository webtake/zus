<?php
$db = new Database\DB();

$prevodni_tabulka = Array(
    'ä'=>'a',
    'Ä'=>'A',
    'á'=>'a',
    'Á'=>'A',
    'à'=>'a',
    'À'=>'A',
    'ã'=>'a',
    'Ã'=>'A',
    'â'=>'a',
    'Â'=>'A',
    'č'=>'c',
    'Č'=>'C',
    'ć'=>'c',
    'Ć'=>'C',
    'ď'=>'d',
    'Ď'=>'D',
    'ě'=>'e',
    'Ě'=>'E',
    'é'=>'e',
    'É'=>'E',
    'ë'=>'e',
    'Ë'=>'E',
    'è'=>'e',
    'È'=>'E',
    'ê'=>'e',
    'Ê'=>'E',
    'í'=>'i',
    'Í'=>'I',
    'ï'=>'i',
    'Ï'=>'I',
    'ì'=>'i',
    'Ì'=>'I',
    'î'=>'i',
    'Î'=>'I',
    'ľ'=>'l',
    'Ľ'=>'L',
    'ĺ'=>'l',
    'Ĺ'=>'L',
    'ń'=>'n',
    'Ń'=>'N',
    'ň'=>'n',
    'Ň'=>'N',
    'ñ'=>'n',
    'Ñ'=>'N',
    'ó'=>'o',
    'Ó'=>'O',
    'ö'=>'o',
    'Ö'=>'O',
    'ô'=>'o',
    'Ô'=>'O',
    'ò'=>'o',
    'Ò'=>'O',
    'õ'=>'o',
    'Õ'=>'O',
    'ő'=>'o',
    'Ő'=>'O',
    'ř'=>'r',
    'Ř'=>'R',
    'ŕ'=>'r',
    'Ŕ'=>'R',
    'š'=>'s',
    'Š'=>'S',
    'ś'=>'s',
    'Ś'=>'S',
    'ť'=>'t',
    'Ť'=>'T',
    'ú'=>'u',
    'Ú'=>'U',
    'ů'=>'u',
    'Ů'=>'U',
    'ü'=>'u',
    'Ü'=>'U',
    'ù'=>'u',
    'Ù'=>'U',
    'ũ'=>'u',
    'Ũ'=>'U',
    'û'=>'u',
    'Û'=>'U',
    'ý'=>'y',
    'Ý'=>'Y',
    'ž'=>'z',
    'Ž'=>'Z',
    'ź'=>'z',
    'Ź'=>'Z'
);

$lang_code = $db->query("SELECT icon FROM languages WHERE ID = :id");
$lang_code->bind(":id", $_SESSION["lang"]);
$lang_code->execute();
$lang_code = $lang_code->fetchColumn();



if(isset($_POST["save"])){
    $save = false;
    $sql = $db->query("INSERT INTO forms(name, code_name, sendValue, successMessage, lang, columns) 
                       VALUES(:name, :code_name, :sendValue, :successMessage, :lang, :columns)");
    $sql->bind(":name", $_POST["name"]);
    $sql->bind(":code_name", strtolower(str_replace(" ", "_", strtr($_POST["name"], $prevodni_tabulka)))."_".$lang_code);
    $sql->bind(":sendValue", $_POST["sendValue"]);
    $sql->bind(":successMessage", $_POST["successMessage"]);
    $sql->bind(":lang", $_SESSION["lang"]);
    $sql->bind(":columns", $_POST["columns"]);

    $save = $sql->execute();

    $smarty->assign("save", $save);

    $id = $db->lastInsertId();

    if($save){
        header("Location: /admin/forms/edit/$id/#inputs");
    }
}