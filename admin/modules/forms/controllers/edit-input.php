<?php
$db = new Database\DB();

$id = $url->getIndex(2);

if(isset($_POST["save"])){
    $save = false;

    $sql = $db->query("UPDATE form_inputs SET 
                                name = :name,
                                cz_name = :cz_name,
                                type = :type,
                                required = :required 
                                WHERE ID = :id");

    $sql->bind(":name", $_POST["name"]);
    $sql->bind(":type", $_POST["type"]);
    $sql->bind(":cz_name", $_POST["cz_name"]);
    $sql->bind(":required", isset($_POST["required"]) ? 1 : 0);

    $sql->bind(":id", $id);

    $save = $sql->execute();

    $smarty->assign("save", $save);
}

$sql = $db->query("SELECT * FROM form_inputs WHERE ID = :id");
$sql->bind(":id", $id);

$sql->execute();

if($sql->rowCount() == 1){
    $page = $sql->fetch();
    $smarty->assign("page", $page);

    $values = $db->query("SELECT * FROM form_values WHERE input_id = :form_id");
    $values->bind(":form_id", $page["ID"]);
    $values->execute();

    $values = $values->fetchAll();
    $smarty->assign("values", $values);

}else{
    header("Location: ".ROOT."404");
}