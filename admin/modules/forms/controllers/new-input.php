<?php
$db = new Database\DB();

$id = $url->getIndex(2);

if(isset($_POST["save"])){
    $save = false;

    $sql = $db->query("INSERT INTO form_inputs (name, form_id, type, cz_name, position, required) 
                                VALUES(:name, :form_id, :type, :cz_name, (SELECT MAX(position) FROM form_inputs f_i WHERE form_id = :form_id)+1, :required)");

    $sql->bind(":name", $_POST["name"]);
    $sql->bind(":form_id", $id);
    $sql->bind(":type", $_POST["type"]);
    $sql->bind(":cz_name", $_POST["cz_name"]);
    $sql->bind(":required", isset($_POST["required"]) ? 1 : 0);

    $save = $sql->execute();

    $smarty->assign("save", $save);

    if($save){
        header("Location: /admin/forms/edit/$id/#inputs");
    }
}