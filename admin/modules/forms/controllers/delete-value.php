<?php
$db = new Database\DB();

$id = $url->getIndex(2);

if(isset($_POST["delete"])){
    $delete = $db->query("DELETE FROM form_values WHERE ID = :id");
    $delete->bind(":id", $id);
    $success = $delete->execute();
    $smarty->assign("success", $success);
}else{
    $page = $db->query("SELECT value FROM form_values WHERE ID = :id");
    $page->bind(":id", $id);
    $smarty->assign("page", $page->fetch());
}