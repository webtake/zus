<?php
$db = new Database\DB();

$id = $url->getIndex(2);


$referer = substr($_SERVER['HTTP_REFERER'], strrpos($_SERVER['HTTP_REFERER'], '/') + 1);

if($referer == "new") {
    $save = true;
    $smarty->assign("save", $save);
}


if(isset($_POST["save"])){
    $save = false;

    $sql = $db->query("UPDATE forms SET 
                                name = :name,
                                successMessage = :successMessage,
                                sendValue = :sendValue,
                                columns = :columns
                                WHERE ID = :id");

    $sql->bind(":name", $_POST["name"]);
    $sql->bind(":successMessage", $_POST["successMessage"]);
    $sql->bind(":sendValue", $_POST["sendValue"]);
    $sql->bind(":columns", $_POST["columns"]);

    $sql->bind(":id", $id);

    $save = $sql->execute();

    $smarty->assign("save", $save);
}

$sql = $db->query("SELECT * FROM forms WHERE ID = :id");
$sql->bind(":id", $id);

$sql->execute();

if($sql->rowCount() == 1){
    $page = $sql->fetch();
    $smarty->assign("page", $page);

    $inputs_list = $db->query("SELECT * FROM form_inputs WHERE form_id = :form_id ORDER BY position ASC");
    $inputs_list->bind(":form_id", $id);
    $inputs_list->execute();

    $inputs_list = $inputs_list->fetchAll();
    $smarty->assign("inputs", $inputs_list);

}else{
    header("Location: ".ROOT."404");
}