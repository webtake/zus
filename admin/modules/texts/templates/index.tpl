{extends "index.tpl"}
{block "title"}Ostatní texty{/block}
{block "content"}
    <section class="content-header">
        {if $state == "summary"}
            <h1>Ostatní texty</h1>
        {elseif $state == "new"}
            <h1>Nový text</h1>
        {elseif $state == "edit"}
            <h1>Úprava textu</h1>
        {elseif $state == "delete"}
            <h1>Odstranění textu</h1>
        {/if}
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    {if $state == "summary"}
                        <a href="{$ROOT}{$active}/new" class="btn btn-success"><i class="fa fa-plus"></i></a>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>#</th>
                                    <th>Název textu</th>
                                    <th>Akce</th>
                                </tr>
                                {foreach item=$item from=$pages}
                                    <tr>
                                        <td>{$item.ID}</td>
                                        <td>{$item.title}</td>
                                        <td>
                                            <a href="{$ROOT}{$active}/edit/{$item["ID"]}" class="btn btn-info btn-sm"><span class="fa fa-pencil-square-o"></span></a>
                                            <a href="{$ROOT}{$active}/delete/{$item["ID"]}" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></a>
                                        </td>
                                    </tr>
                                    {foreachelse}
                                    <tr>
                                        <td colspan="3">Žádné texty nenalezeny. Kliknutím na zelené <strong>+</strong> vytvořte text.</td>
                                    </tr>
                                {/foreach}
                            </table>
                        </div>
                    {elseif $state == "new"}
                        {include "./new.tpl"}
                    {elseif $state == "edit"}
                        {include "./edit.tpl"}
                    {elseif $state == "delete"}
                        {include "./delete.tpl"}
                    {/if}
                </div>
            </div>
        </div>
    </section>
{/block}