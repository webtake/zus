<?php
$db = new Database\DB();


if(isset($_POST["save"])){
    $save = false;
    $sql = $db->query("INSERT INTO variables(title, variable, value, lang) 
                       VALUES(:title, :variable, :value, :lang)");
    $sql->bind(":title", $_POST["title"]);
    $sql->bind(":variable", $_POST["variable"]);
    $sql->bind(":value", $_POST["value"]);
    $sql->bind(":lang", $_SESSION["lang"]);

    $save = $sql->execute();

    $smarty->assign("save", $save);
}