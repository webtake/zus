{extends "index.tpl"}
{block "title"}Nastavení webových stránek{/block}
{block "content"}
    <section class="content-header">
        {if $state == "summary"}
            <h1>Nastavení webových stránek</h1>
        {elseif $state == "new"}
            <h1>Nové nastavení</h1>
        {elseif $state == "edit"}
            <h1>Úprava nastavení</h1>
        {elseif $state == "delete"}
            <h1>Odstranění nastavení</h1>
        {/if}
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    {if $state == "summary"}
                        <a href="{$ROOT}{$active}/new" class="btn btn-success"><i class="fa fa-plus"></i></a>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>#</th>
                                    <th>Název nastavení</th>
                                    <th>Akce</th>
                                </tr>
                                {foreach item=$item from=$pages}
                                    <tr>
                                        <td>{$item.ID}</td>
                                        <td>{$item.title}</td>
                                        <td>
                                            <a href="{$ROOT}{$active}/edit/{$item["ID"]}" class="btn btn-info btn-sm"><span class="fa fa-pencil-square-o"></span></a>
                                            <a href="{$ROOT}{$active}/delete/{$item["ID"]}" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></a>
                                        </td>
                                    </tr>
                                    {foreachelse}
                                    <tr>
                                        <td colspan="3">Žádné nastavení nenalezeny. Kliknutím na zelené <strong>+</strong> vytvořte nastavení.</td>
                                    </tr>
                                {/foreach}
                            </table>
                        </div>
                    {elseif $state == "new"}
                        {include "./new.tpl"}
                    {elseif $state == "edit"}
                        {include "./edit.tpl"}
                    {elseif $state == "delete"}
                        {include "./delete.tpl"}
                    {/if}
                </div>
            </div>
        </div>
    </section>
{/block}