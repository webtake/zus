<?php
$db = new Database\DB();


if(isset($_POST["save"])){
    $save = false;
    $sql = $db->query("INSERT INTO settings(title, rule, value, lang) 
                       VALUES(:title, :rule, :value, :lang)");
    $sql->bind(":title", $_POST["title"]);
    $sql->bind(":rule", $_POST["rule"]);
    $sql->bind(":value", $_POST["value"]);
    $sql->bind(":lang", $_SESSION["lang"]);

    $save = $sql->execute();

    $smarty->assign("save", $save);
}