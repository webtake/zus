<?php
$db = new Database\DB();

$id = $url->getIndex(2);

if(isset($_POST["save"])){
    $save = false;

    $sql = $db->query("UPDATE settings SET 
                                title = :title,
                                rule = :rule,
                                value = :value
                                WHERE ID = :id");

    $sql->bind(":title", $_POST["title"]);
    $sql->bind(":rule", $_POST["rule"]);
    $sql->bind(":value", $_POST["value"]);

    $sql->bind(":id", $id);

    $save = $sql->execute();

    $smarty->assign("save", $save);
}

$sql = $db->query("SELECT * FROM settings WHERE ID = :id");
$sql->bind(":id", $id);

$sql->execute();

if($sql->rowCount() == 1){
    $page = $sql->fetch();
    $smarty->assign("page", $page);
}else{
    header("Location: ".ROOT."404");
}