<?php
$db = new Database\DB();


if(isset($_POST["save"])){
    $save = false;

    $sql = $db->query("INSERT INTO languages(name, icon, code) 
                       VALUES(:name, :icon, :code)");
    $sql->bind(":name", $_POST["name"]);
    $sql->bind(":icon", $_POST["hidCflag"]);
    $sql->bind(":code", strtoupper($_POST["hidCflag"]));
    $save = $sql->execute();

    $smarty->assign("save", $save);
}else{

    $images = __DIR__."/../../../layout/flags/4x3/";

    $images = glob($images . "*.svg");

    $images_array = [];

    foreach($images as $image)
    {
        $img = basename($image);
        $img = pathinfo($img, PATHINFO_FILENAME);

        $check = $db->query("SELECT * FROM languages WHERE icon = :icon");
        $check->bind(":icon", $img);
        $check->execute();
        if($check->rowCount() == 0) $images_array[] = $img;
    }

    $smarty->assign("flags", $images_array);
}
