{if isset($save)}
    {if $save == true}
        <p class="alert alert-success">Uloženo</p>
    {else}
        <p class="alert alert-danger">Neuloženo.</p>
    {/if}
{/if}
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nastavení jazyku</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Vlastní název (pouze pro administraci)</label>
                        <div class="col-md-5">
                            <input id="name" type="text" name="name" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="flags" class="col-sm-2 control-label">Vyberte ikonu</label>
                        <input type="hidden" name="hidCflag" id="hidCflag" value="" />
                        <div class="col-md-5">
                            <select name="flags" id="flags">
                                {foreach item=$flag from=$flags}
                                    <option value="{$flag}" data-imagesrc="{$ROOT}layout/flags/4x3/{$flag}.svg">{$flag}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div>