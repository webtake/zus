{extends "index.tpl"}
{block "title"}Jazyky{/block}
{block "content"}
    <section class="content-header">
        {if $state == "summary"}
            <h1>Jazyky</h1>
        {elseif $state == "new"}
            <h1>Nový jazyk</h1>
        {elseif $state == "edit"}
            <h1>Úprava jazyku</h1>
        {elseif $state == "delete"}
            <h1>Odstranění jazyku</h1>
        {/if}
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    {if $state == "summary"}
                        <a href="{$ROOT}{$active}/new" class="btn btn-success"><i class="fa fa-plus"></i></a>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>#</th>
                                    <th>Vlajka</th>
                                    <th>Název</th>
                                    <th>Akce</th>
                                </tr>
                                {foreach item=$item from=$pages}
                                    <tr>
                                        <td>{$item.ID}</td>
                                        <td><img src="{$ROOT}layout/flags/4x3/{$item.icon}.svg" style="max-height: 32px;" alt="" /></td>
                                        <td>{$item.name}</td>
                                        <td>
                                            <a href="{$ROOT}{$active}/delete/{$item["ID"]}" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></a>
                                        </td>
                                    </tr>
                                    {foreachelse}
                                    <tr>
                                        <td colspan="3">Žádné jazyky nenalezeny. Kliknutím na zelené <strong>+</strong> přidejte jazyk.</td>
                                    </tr>
                                {/foreach}
                            </table>
                        </div>
                    {elseif $state == "new"}
                        {include "./new.tpl"}
                    {elseif $state == "delete"}
                        {include "./delete.tpl"}
                    {/if}
                </div>
            </div>
        </div>
    </section>
{/block}