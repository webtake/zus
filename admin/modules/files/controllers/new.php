<?php
$db = new Database\DB();

if(isset($_POST["save"])){
    $save = false;
    $sql = $db->query("INSERT INTO files(title, lang) 
                       VALUES(:title, :lang)");
    $sql->bind(":title", $_POST["title"] ?? md5(time()));
    $sql->bind(":lang", $_SESSION["lang"]);

    $save = $sql->execute();

    if(is_uploaded_file($_FILES["file"]["tmp_name"])){
        $target_dir = $module->uploadPath;
        $imageFileType = strtolower(pathinfo($_FILES["file"]["name"],PATHINFO_EXTENSION));
        $filename = md5(time()) . "-" . slugify($_POST["title"]) . "." . $imageFileType;
        $target_file = $target_dir . $filename;
        echo $target_file."<br />";
        $upload = false;
        if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
            $id = $db->lastInsertId();

            $sql = $db->query("UPDATE files SET url = :file WHERE ID = :id");
            echo "UPDATE files SET url = :file WHERE ID = :id";
            $sql->bind(":file", "/files/".$filename);
            $sql->bind(":id", $id);
            $upload = $sql->execute();
            echo "Upload dokončen";
        } else {
            echo $_FILES["file"]["error"];
        }

        $save = ($save && $upload);
    }else{
        echo $_FILES["file"]["error"];
    }

    $smarty->assign("save", $save);
}