<?php
$db = new Database\DB();

$id = $url->getIndex(2);

if(isset($_POST["delete"])) {
    $success = false;
    $file = $db->query("SELECT url FROM files WHERE ID = :id");
    $file->bind(":id", $id);
    $file->execute();

    if($file->rowCount() == 1){
        $file = $file->fetchColumn();
        if(file_exists(__DIR__."/../../../".$file)){
            $success = unlink(__DIR__."/../../../".$file);
        }else{
            $success = true;
        }
    }

    $delete = $db->query("DELETE FROM files WHERE ID = :id");
    $delete->bind(":id", $id);
    $success = ($success && $delete->execute());
    $smarty->assign("success", $success);
}else{
    $page = $db->query("SELECT title FROM files WHERE ID = :id");
    $page->bind(":id", $id);
    $smarty->assign("page", $page->fetch());
}