<?php
require_once __DIR__.'/../../index.php';

$smarty = new Smarty;

$state = $url->getIndex(1) ?? "summary";

$db = new Database\DB();

$module = new Modules\Module($url->getIndex(0));

if(isset($_POST["save"])){
    $newpass = hash("sha512", $_POST["password"]);

    $update = $db->query("UPDATE users SET password = :password WHERE ID = :id");
    $update->bind(":password", $newpass);
    $update->bind(":id", $_SESSION["user"]["ID"]);
    $save = $update->execute();
    $smarty->assign("save", $save);
}


$smarty->assign("state", $state);
$smarty->assign($default);
$smarty->assign('vars', get_defined_vars());
$smarty->display($module->template);
