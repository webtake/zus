{if isset($save)}
    {if $save == true}
        <p class="alert alert-success">Uloženo.</p>
    {else}
        <p class="alert alert-danger">Nepovedlo se uložit.</p>
    {/if}
{/if}
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nastavení uživatele</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">Nové heslo</label>
                        <div class="col-md-5">
                            <input id="password" type="text" name="password" class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div>