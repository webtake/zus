{extends "index.tpl"}
{block "title"}Nastavení uživatelského účtu{/block}
{block "content"}
    <section class="content-header">
        {if $state == "summary"}
            <h1>Nastavení uživatelského účtu</h1>
        {/if}
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    {include "./edit.tpl"}
                </div>
            </div>
        </div>
    </section>
{/block}