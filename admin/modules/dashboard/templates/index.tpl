{extends "index.tpl"}
{block "title"}Nástěnka{/block}
{block "content"}
    <section class="content-header">
        <h1>
            Nástěnka
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <p class="alert alert-success">Vítejte v administraci, <strong>{$user.fullname}</strong>. Pokračujte pomocí menu na levé straně.</p>
            </div>
        </div>
    </section>
{/block}