<?php
require_once __DIR__.'/../../index.php';

$smarty = new Smarty;

$module = new Modules\Module($url->getIndex(0) ?? HOME);

$smarty->assign($default);

$smarty->assign('vars', get_defined_vars());

$smarty->display($module->template);
