{if isset($save)}
    {if $save == true}
        <p class="alert alert-success">Uloženo.</p>
        {else}
        <p class="alert alert-danger">Nepovedlo se uložit.</p>
    {/if}
{/if}
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nastavení fotogalerie</a></li>
        <li><a href="#content" data-toggle="tab">Obsah</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Název fotogalerie</label>
                        <div class="col-md-5">
                            <input id="title" type="text" name="title" value="{$page.title}" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="category" class="col-sm-2 control-label">Kategorie</label>
                        <div class="col-md-5">
                            <select name="category" id="category" class="form-control">
                                {foreach from=$categories item=$cat}
                                    <option value="{$cat.ID}"{if $cat.ID == $page.category} selected{/if}>{$cat.title}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="url" class="col-sm-2 control-label">URL fotogalerie</label>
                        <div class="col-md-5">
                            <input id="url" type="text" name="url" value="{$page.url}" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="og_img" class="col-sm-2 control-label">Výchozí obrázek</label>
                        <div class="col-md-5">
                            <img src="{$page.og_img}" alt="Obrázek" style="max-width: 100px;" />
                            <input id="og_img" type="file" name="og_img" class="form-control" /><br />
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="content">
                <div class="box-body">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label for="upload_files" class="label-control col-sm-12">Nahrejte fotografie</label>
                            <div class="col-sm-12">
                                <input type="file" name="upload_files[]" id="upload_files" multiple /> 
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label for="ckeditor" class="label-control col-sm-12">Text (zobrazí se při detailním zobrazení)</label>
                            <div class="col-sm-12">
                                <textarea name="long_text" class="ckeditor" cols="30" rows="10">{$page.long_text}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="label-control col-sm-12">Nahrané fotografie</label>
                            <div class="col-sm-12">
                                {foreach from=$page.images item=$img}
                                    <div id="id_{$img.ID}" class="col-md-1" style="position: relative;">
                                        <img src="{$img.url}" alt="Obrázek" style="width: 100%; height: 100px; object-fit: cover; border: 1px #000 solid " />
                                        <a href="#" onClick="removeImage({$img.ID})" style="position: absolute; border-radius: 0; top: 0; right: 15px;" class="btn btn-danger btn-sm">&times;</a>
                                    </div>
                                {/foreach}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div>