{extends "index.tpl"}
{block "title"}Kategorie fotogalerií{/block}
{block "content"}
    <section class="content-header">
        {if $state == "summary"}
            <h1>Kategorie fotogalerií</h1>
        {elseif $state == "new"}
            <h1>Nová kategorie</h1>
        {elseif $state == "edit"}
            <h1>Úprava kategorie</h1>
        {elseif $state == "delete"}
            <h1>Odstranění kategorie</h1>
        {/if}
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-body">
                        {if $state == "summary"}
                            <a href="{$ROOT}{$active}/categories/new" class="btn btn-success"><i class="fa fa-plus"></i></a>
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tr>
                                        <th>#</th>
                                        <th>Název</th>
                                        <th>Pozice</th>
                                        <th>Akce</th>
                                    </tr>
                                    {foreach item=$item from=$pages}
                                        <tr>
                                            <td>{$item.ID}</td>
                                            <td>{$item.title}</td>
                                            <td>
                                                <button class="btn btn-warning" onClick="moveItem('photogalleries_categories', {$item.ID}, -1);"><span class="fa fa-caret-up"></span></button>
                                                <button class="btn btn-warning" onClick="moveItem('photogalleries_categories', {$item.ID}, +1);"><span class="fa fa-caret-down"></span></button>
                                            </td>
                                            <td>
                                                <a href="{$ROOT}{$active}/categories/edit/{$item["ID"]}" class="btn btn-info btn-sm"><span class="fa fa-pencil-square-o"></span></a>
                                                <a href="{$ROOT}{$active}/categories/delete/{$item["ID"]}" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></a>
                                            </td>
                                        </tr>
                                        {foreachelse}
                                        <tr>
                                            <td colspan="3">Žádné kategorie nenalezeny. Kliknutím na zelené <strong>+</strong> vytvořte kategorii.</td>
                                        </tr>
                                    {/foreach}
                                </table>
                            </div>
                        {elseif $state == "new"}
                            {include "./new.tpl"}
                        {elseif $state == "edit"}
                            {include "./edit.tpl"}
                        {elseif $state == "delete"}
                            {include "./delete.tpl"}
                        {/if}
                    </div>
                </div>
            </div>
            {if $state == "summary"}
            <div class="col-md-3">
                <div class="box">
                    <div class="box-body">
                        <h4>Vyhledávání</h4>
                        <input type="text" id="search" placeholder="Zadejte hledaný název fotogalerie / kategorie" class="form-control" />
                    </div>
                </div>
            </div>
            {/if}
        </div>
    </section>
{/block}