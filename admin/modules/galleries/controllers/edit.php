<?php
    $db = new Database\DB();

    $id = $url->getIndex(2);

    $image = $url->getIndex(3);

    if($image != false){
        $temp = $db->query("UPDATE photogalleries SET og_img = '' WHERE ID = :id");
        $temp->bind(":id", $id);
        $temp->execute();
    }
    if(isset($_POST["save"])){
        $save = false;
        $sql = $db->query("UPDATE photogalleries SET 
                                    title = :title, 
                                    category = :category,
                                    long_text = :long_text, 
                                    url = :url
                                    WHERE ID = :id");

        $sql->bind(":title", $_POST["title"]);
        $sql->bind(":category", $_POST["category"]);
        $sql->bind(":long_text", $_POST["long_text"]);


        $url = ($_POST["url"] == "") ? slugify($_POST["title"]) : $_POST["url"];
        $url = uniqueUrl($url, $id);

        $sql->bind(":url", $url);
        $sql->bind(":id", $id);

        $save = $sql->execute();

        if(is_uploaded_file($_FILES["og_img"]["tmp_name"])){
            $target_dir = $module->uploadPath;
            $imageFileType = strtolower(pathinfo($_FILES["og_img"]["name"],PATHINFO_EXTENSION));
            $filename = time() . "." . $imageFileType;
            $target_file = $target_dir . $filename;
            $upload = false;
            if (move_uploaded_file($_FILES["og_img"]["tmp_name"], $target_file)) {
                $sql = $db->query("UPDATE photogalleries SET og_img = :img WHERE ID = :id");
                $sql->bind(":img", "/images/uploads/".$filename);
                $sql->bind(":id", $id);
                $upload = $sql->execute();
            } else {
                $upload = false;
            }

            $save = ($save && $upload);
        }
        $countfiles = count($_FILES['upload_files']['name']);
        for($i=0;$i<$countfiles;$i++){
            
            $target_dir = $module->uploadPath;
            $imageFileType = strtolower(pathinfo($_FILES['upload_files']["name"][$i],PATHINFO_EXTENSION));
            $filename = time() . "_" . rand(1, 1024) . "." . $imageFileType;
            $target_file = $target_dir . $filename;
         
          // Upload file


            $upload = false;
            if (move_uploaded_file($_FILES['upload_files']["tmp_name"][$i], $target_file)) {

                $sql = $db->query("INSERT INTO photogalleries_images (ID_gallery, url) VALUES(:cat, :img)");
                $sql->bind(":cat", $id);
                $sql->bind(":img", "/images/uploads/".$filename);
                $upload = $sql->execute();
            } else {
                $upload = false;
            }
         
         }
        $smarty->assign("save", $save);
    }

    $sql = $db->query("SELECT * FROM photogalleries WHERE ID = :id");
    $sql->bind(":id", $id);

    $sql->execute();

    if($sql->rowCount() == 1){
        $page = $sql->fetch();

        $sql = $db->query("SELECT * FROM photogalleries_images WHERE ID_gallery = :id");
        $sql->bind(":id", $id);
        $sql->execute();

        $page["images"] = $sql->fetchAll();

        $smarty->assign("page", $page);
    }else{
        header("Location: ".ROOT."404");
    }

