<?php
$db = new Database\DB();


if(isset($_POST["save"])){
    $save = false;

    $sql = $db->query("INSERT INTO photogalleries(title, long_text, url, category, lang) 
                       VALUES(:title, :long_text, :url, :category, :lang)");
    $sql->bind(":title", $_POST["title"]);
    $sql->bind(":long_text", $_POST["long_text"]);

    $url = ($_POST["url"] == "") ? slugify($_POST["title"]) : $_POST["url"];
    $url = uniqueUrl($url, null);

    $sql->bind(":url", $url);
    $sql->bind(":category", $_POST["category"]);
    $sql->bind(":lang", $_SESSION["lang"]);

    $save = $sql->execute();

    $galleryID = $db->lastInsertId();
    if(is_uploaded_file($_FILES["og_img"]["tmp_name"])){
        $target_dir = $module->uploadPath;
        $imageFileType = strtolower(pathinfo($_FILES["og_img"]["name"],PATHINFO_EXTENSION));
        $filename = time() . "." . $imageFileType;
        $target_file = $target_dir . $filename;

        $upload = false;
        if (move_uploaded_file($_FILES["og_img"]["tmp_name"], $target_file)) {

            $sql = $db->query("UPDATE photogalleries SET og_img = :img WHERE ID = :id");
            $sql->bind("img", "/images/uploads/".$filename);
            $sql->bind(":id", $galleryID);
            $upload = $sql->execute();
        } else {
            $upload = false;
        }

        $save = ($save && $upload);
    }

    $countfiles = count($_FILES['upload_files']['name']);
    for($i=0;$i<$countfiles;$i++){
        
        $target_dir = $module->uploadPath;
        $imageFileType = strtolower(pathinfo($_FILES['upload_files']["name"][$i],PATHINFO_EXTENSION));
        $filename = time() . "_" . rand(1, 1024) . "." . $imageFileType;
        $target_file = $target_dir . $filename;
     
      // Upload file


        $upload = false;
        if (move_uploaded_file($_FILES['upload_files']["tmp_name"][$i], $target_file)) {

            $sql = $db->query("INSERT INTO photogalleries_images (ID_gallery, url) VALUES(:cat, :img)");
            $sql->bind(":cat", $galleryID);
            $sql->bind(":img", "/images/uploads/".$filename);
            $upload = $sql->execute();
        } else {
            $upload = false;
        }
     
     }

    $smarty->assign("save", $save);
}