<?php
$db = new Database\DB();


if(isset($_POST["save"])){
    $save = false;

    $newPos = $db->query("SELECT MAX(position) as p FROM photogalleries_categories WHERE lang=:lang");
    $newPos->bind(":lang", $_SESSION["lang"]);
    $newPos->execute();
    $newPos = $newPos->fetchColumn();
    if($newPos == null) $newPos = 0;
    else $newPos += 1;
    $sql = $db->query("INSERT INTO photogalleries_categories(title, description, lang, url, `position`) 
                       VALUES(:title, :description, :lang, :url, :position)");
    $sql->bind(":title", $_POST["title"]);
    $sql->bind(":description", $_POST["description"]);
    $sql->bind(":lang", $_SESSION["lang"]);

    $url = ($_POST["url"] == "") ? slugify($_POST["title"]) : $_POST["url"];
    $url = uniqueUrl($url, null);

    $sql->bind(":url", $url);
    $sql->bind(":position", $newPos);
    $save = $sql->execute();

    $smarty->assign("save", $save);
}