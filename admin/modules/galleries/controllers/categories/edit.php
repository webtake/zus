<?php
    $db = new Database\DB();

    $id = $url->getIndex(3);
    if(isset($_POST["save"])){
        $save = false;
        $sql = $db->query("UPDATE photogalleries_categories SET 
                                    title = :title, 
                                    description = :description,
                                     url = :url
                                    WHERE ID = :id");
        $sql->bind(":title", $_POST["title"]);
        $sql->bind(":description", $_POST["description"]);

        $url = ($_POST["url"] == "") ? slugify($_POST["title"]) : $_POST["url"];
        $url = uniqueUrl($url, null);

        $sql->bind(":url", $url);
        $sql->bind(":id", $id);

        $save = $sql->execute();

        $smarty->assign("save", $save);
    }

    $sql = $db->query("SELECT * FROM photogalleries_categories WHERE ID = :id");
    $sql->bind(":id", $id);

    $sql->execute();

    if($sql->rowCount() == 1){
        $page = $sql->fetch();
        $smarty->assign("page", $page);
    }else{
        header("Location: ".ROOT."404");
    }

