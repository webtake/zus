<?php
$db = new Database\DB();

$id = $url->getIndex(3);

if(isset($_POST["delete"])){
    $delete = $db->query("DELETE FROM photogalleries_categories WHERE ID = :id");
    $delete->bind(":id", $id);
    $success = $delete->execute();
    $smarty->assign("success", $success);
}else{
    $page = $db->query("SELECT title FROM photogalleries_categories WHERE ID = :id");
    $page->bind(":id", $id);
    $smarty->assign("page", $page->fetch());
}