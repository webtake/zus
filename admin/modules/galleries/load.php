<?php

$load = [
	"title" => "Fotogalerie",
	"icon"  => "image",
	"href"	=> "#",
	"url"	=> "galleries",
    "sub" 	=> [
    	[
    		"icon"	=> "circle-o",
	    	"title" => "Všechny fotogalerie",
			"url"   => "galleries",
			"href"  => "galleries/summary",
		],
		[
    		"icon"	=> "circle-o",
			"title"	=> "Kategorie fotogalerií",
			"url"	=> "galleries/categories",
			"href"	=> "galleries/categories/summary"
		]
    ]
];