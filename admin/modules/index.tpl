<!doctype html>
<html lang="cs">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>{$W.projectName} - Administrace - {block "title"}{/block}</title>
    <link rel="stylesheet" href="{$W.VENDOR}adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="{$W.VENDOR}adminlte/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="{$W.VENDOR}adminlte/css/AdminLTE.min.css">
    <link rel="stylesheet" href="{$W.VENDOR}adminlte/css/skins/skin-black-light.min.css">
    <link rel="stylesheet" href="{$W.VENDOR}datetimepicker/jquery.datetimepicker.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">

    <link rel="stylesheet" href="{$W.CSS}styles.css?v=2">
    <style>
        .images-container::after {
            content: "";
            display: table;
            clear: both; }

        .images-container .image-container {
            float: left;
            padding: 3px;
            margin-right: 10px;
            margin-bottom: 35px;
            position: relative;
            border: 1px solid #e6eaee;
            overflow: hidden; }
        .images-container .image-container.active {
            border-color: #7867A7; }
        .images-container .image-container:hover .controls {
            bottom: 0;
            opacity: 1; }

        .images-container .controls {
            position: absolute;
            left: 0;
            right: 0;
            opacity: 0;
            bottom: -35px;
            text-align: center;
            height: 35px;
            font-size: 24px;
            transition: bottom 0.2s ease, opacity 0.2s ease;
            background-color: #fff; }
        .images-container .controls::after {
            content: "";
            display: table;
            clear: both; }
        .images-container .controls .control-btn {
            display: inline-block;
            color: #4f5f6f;
            cursor: pointer;
            width: 35px;
            height: 35px;
            line-height: 35px;
            text-align: center;
            opacity: 0.5;
            transition: opacity 0.3s ease; }
        .images-container .controls .control-btn:hover {
            opacity: 1; }
        .images-container .controls .control-btn.move {
            cursor: move; }
        .images-container .controls .control-btn.star {
            color: #FFB300; }
        .images-container .controls .control-btn.star i:before {
            content: "\f006"; }
        .images-container .controls .control-btn.star.active i:before {
            content: "\f005"; }
        .images-container .controls .control-btn.remove {
            color: #FF4444; }

        .images-container .image {
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            width: 130px;
            height: 135px;
            line-height: 135px;
            text-align: center; }

        .images-container .image-container.main {
            border-color: #FFB300; }

        .images-container .image-container.new {
            opacity: 0.6;
            transition: opacity 0.3s ease;
            border-style: dashed;
            border: 1px #7867A7 solid;
            color: #7867A7; }
        .images-container .image-container.new .image {
            font-size: 2.5rem; }
        .images-container .image-container.new:hover {
            opacity: 1; }
    </style>
<body class="skin-black-light sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="{$ROOT}" class="logo">
            <span class="logo-mini"><b>ADM</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Administrace</b></span>
        </a>
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span>{$user.fullname}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                                <img src="{$W.IMAGES}default-avatar.png" class="img-circle" alt="User Image">

                                <p>
                                    {$user.fullname}
                                    <small>{$user.post}</small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{$ROOT|replace:'admin/':''}" target="_blank" class="btn btn-default btn-flat">Zobrazit web</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{$ROOT}logout" class="btn btn-default btn-flat">Odhlásit se</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <section class="sidebar">
            <ul class="sidebar-menu" data-widget="tree">
                {foreach from=$menu item=$menuitem}
                    <li class="header">{$menuitem.title}</li>
                    {foreach item=$item from=$menuitem.subs}
                        {if isset($item.sub)}
                            <li class="treeview menu{if $active == $item.url}-open active{/if}">
                                <a href="#">
                                    <i class="fa fa-{$item.icon}"></i> <span>{$item.title}</span>
                                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                                </a>
                                <ul class="treeview-menu">
                                    {foreach from=$item.sub item=$sub}
                                        <li>
                                            <a href="{$ROOT}{$sub.url}">
                                                <i class="fa fa-{$sub.icon}"></i>
                                                <span>{$sub.title}</span>
                                            </a>
                                        </li>
                                    {/foreach}
                                </ul>
                            </li>
                        {else}
                            <li{if $active == $item.url} class="active"{/if}><a href="{$ROOT}{$item.href}"><i class="fa fa-{$item.icon}"></i> <span>{$item.title}</span></a></li>
                        {/if}
                    {/foreach}
                {/foreach}
                <li class="header langs">
                    Jazyková verze
                </li>
                <li class="langs">
                    {foreach item=$lang from=$languages}
                        <a href="{$ROOT}language/{$lang.ID}" style="padding: 0; display: inline">
                            <i class="flag-icon flag-icon-{$lang.icon}{if $SESSION.lang == $lang.ID} active{/if}"></i>
                        </a>
                    {/foreach}
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        {block "content"}{/block}
    </div>
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
        </div>
        <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">Almsaeed Studio</a> & <a href="https://webtake.cz/">Webtake.cz</a>.</strong> All rights
        reserved.
    </footer>
</div>
<script src="{$W.VENDOR}adminlte/bower_components/jquery/dist/jquery.min.js"></script>
<script src="{$W.VENDOR}adminlte/bower_components/jquery-ui/jquery-ui.min.js"></script>
{*<script src="{$W.VENDOR}adminlte/bower_components/ckeditor/ckeditor.js"></script>*}
<script src="{$W.VENDOR}ckeditor/ckeditor.js?v=2"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="{$W.VENDOR}adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="{$W.VENDOR}adminlte/js/adminlte.min.js"></script>
<script src="{$W.VENDOR}datetimepicker/jquery.datetimepicker.full.min.js"></script>
<script src="{$W.JS}pages.js?v=2"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/prashantchaudhary/ddslick/master/jquery.ddslick.min.js" ></script>

<script>
    $(function () {
        CKEDITOR.replace('ckeditor', {
            contentsCss: [CKEDITOR.basePath + 'contents.css', '/layout/css/styles.css', CKEDITOR.basePath + 'bootstrap.min.css'],
            allowedContent: true
        });

    })
</script>
<script>
    $(document).ready(function(){
        $('#flags').ddslick({
            height: 350,
            width: "100%",
            selectText: "Vyberte jazyk...",
            onSelected: function(data){
                if(data.selectedIndex >= 0) {
                    $('#hidCflag').val(data.selectedData.value);
                }
            }
        });
    });
</script>
<script>
    $("#generate").click(function(){
       var chars    = "qwertzuiopasdfghjklyxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM1234567890";
       var pass     = "";
       var len      = 10;

       for(var i = 0; i < len; i++){
           var x = Math.floor(Math.random() * chars.length);
           pass += chars.charAt(x);
       }
       $("#password").val(pass);
    });
    $("#search").keyup(function () {
        var value = this.value.toLowerCase().trim();

        $("table tr").each(function (index) {
            if (!index) return;
            $(this).find("td").each(function () {
                var id = $(this).text().toLowerCase().trim();
                var not_found = (id.indexOf(value) == -1);
                $(this).closest('tr').toggle(!not_found);
                return not_found;
            });
        });
    });
</script>
<script>
    $(function () {
        $.datetimepicker.setLocale('cs');
        $('.datetimepicker').datetimepicker({
            format:'Y-m-d H:i',
            inline:true,
            step: 10
        });
        $('.publish').datetimepicker({
            format:'Y-m-d',
            timepicker: false,
            inline:true,
            step: 10
        });
    });
</script>
<script>
    $(function () {
        $("#search").keyup(function () {
            var value = this.value.toLowerCase().trim();

            $("table tr").each(function (index) {
                if (!index) return;
                $(this).find("td").each(function () {
                    var id = $(this).text().toLowerCase().trim();
                    var not_found = (id.indexOf(value) == -1);
                    $(this).closest('tr').toggle(!not_found);
                    if(not_found === false){
                        var table = $(this).closest('div.collapse');
                        if($(table).hasClass('in') == false){
                            $(table).addClass('in');
                            $(table).attr('aria-expanded', 'true');
                            $(table).removeAttr("style");
                        }
                    }
                    return not_found;
                });
            });
        });
    });
</script>
</body>
</html>