{if isset($save)}
    {if $save == true}
        <p class="alert alert-success">Uloženo</p>
    {else}
        <p class="alert alert-danger">Neuloženo.</p>
    {/if}
{/if}
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nový uživatelský účet</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="username" class="col-sm-2 control-label">Přihlašovací jméno</label>
                        <div class="col-md-5">
                            <input id="username" type="text" name="username" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fullname" class="col-sm-2 control-label">Jméno a příjmení</label>
                        <div class="col-md-5">
                            <input id="fullname" type="text" name="fullname" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="post" class="col-sm-2 control-label">Post</label>
                        <div class="col-md-5">
                            <input id="post" type="text" name="post" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="level" class="col-sm-2 control-label">Úroveň</label>
                        <div class="col-md-5">
                            <input id="level" type="number" name="level" min="0" max="{$user.level - 1}" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">Heslo</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control" id="password" name="password" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div>