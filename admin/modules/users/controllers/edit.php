<?php
$db = new Database\DB();

$id = $url->getIndex(2);
if($id != $_SESSION["user"]["ID"]){
    if(isset($_POST["save"])){
        $save = false;

        $sql = $db->query("UPDATE users SET 
                                    username = :username,
                                    fullname = :fullname,
                                    post = :post,
                                    level = :level
                                    WHERE ID = :id");

        $sql->bind(":username", $_POST["username"]);
        $sql->bind(":fullname", $_POST["fullname"]);
        $sql->bind(":post", $_POST["post"]);
        $sql->bind(":level", $_POST["level"]);

        $sql->bind(":id", $id);

        $save = $sql->execute();

        if($_POST["password"] != "") {
            $sql = $db->query("UPDATE users SET 
                                    password = :pass
                                    WHERE ID = :id");

            $sql->bind(":pass", hash("sha512", $_POST["password"]));

            $sql->bind(":id", $id);

            $sql->execute();
        }

        $smarty->assign("save", $save);
    }

    $sql = $db->query("SELECT * FROM users WHERE ID = :id");
    $sql->bind(":id", $id);

    $sql->execute();

    if($sql->rowCount() == 1){
        $page = $sql->fetch();
        $smarty->assign("page", $page);

        if($page["level"] > $_SESSION["user"]["level"]) header("Location: /admin/access_denied");
    }else{
        header("Location: ".ROOT."404");
    }
}else{
    header("Location: /admin/access_denied");
}