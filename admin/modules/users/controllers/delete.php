<?php
$db = new Database\DB();

$id = $url->getIndex(2);

if(isset($_POST["delete"])){
    $delete = $db->query("DELETE FROM users WHERE ID = :id");
    $delete->bind(":id", $id);
    $success = $delete->execute();
    $smarty->assign("success", $success);
}else{
    $page = $db->query("SELECT * FROM users WHERE ID = :id");
    $page->bind(":id", $id);
    $page = $page->fetch();
    if($page["level"] > $_SESSION["user"]["level"]) header("Location: /admin/access_denied");
    if($page["ID"] == $_SESSION["user"]["ID"]) header("Location: /admin/access_denied");
    $smarty->assign("page", $page);
}