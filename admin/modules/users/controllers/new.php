<?php
$db = new Database\DB;


if(isset($_POST["save"])) {
    $save = false;
    if ($_SESSION["user"]["level"] > $_POST["level"]) {
        $exists = $db->query("SELECT * FROM users WHERE username = :user LIMIT 0, 1");
        $exists->bind(":user", $_POST["username"]);
        $exists->execute();

        if($exists->rowCount() == 0){

            if($_POST["password"] != "") {

                if(is_numeric($_POST["level"])){
                    $sql = $db->query("INSERT INTO users(username, fullname, post, level, password) 
                                   VALUES(:username, :fullname, :post, :level, :password)");
                    $sql->bind(":username", $_POST["username"]);
                    $sql->bind(":fullname", $_POST["fullname"]);
                    $sql->bind(":post", $_POST["post"]);
                    $sql->bind(":level", $_POST["level"]);
                    $sql->bind(":password", hash("sha512", $_POST["password"]));

                    $save = $sql->execute();
                }
            }
        }
    }
    $smarty->assign("save", $save);
}