<?php

# MODULES AND FOLDERS IN modules/ CONTAINS ONLY INFO ABOUT TITLE, URL, HREF AND ICON FOR MENU. ALL OTHER FILES NEEDED ARE IN controllers/ and templates/.

# Module has same name as folder in 'modules/''
# To disable module put '#' in front of the module that u want to disable

$modules = [
    [
        'title'     => 'Obsahová část',
        'modules'   => [
            'dashboard',
            'banner',
            'articles',
            'pages',
            'galleries',
            'files',
            'forms',
        ]
    ],
    [
        'title'     => 'Uživatelská část',
        'modules'   => [
            'user',
            'users',
        ]
    ],
    [
        'title'     => 'Systémová část',
        'modules'   => [
            'languages',
            'texts',
            'settings',
            'modules',
            'custom_blocks',
        ]
    ]
];