<?php

$load = [
	"title" => "Články",
	"icon"  => "align-left",
	"href"	=> "#",
	"url"	=> "articles",
    "sub" 	=> [
    	[
    		"icon"	=> "circle-o",
	    	"title" => "Všechny články",
			"url"   => "articles",
			"href"  => "articles/summary",
		],
		[
    		"icon"	=> "circle-o",
			"title"	=> "Kategorie článků",
			"url"	=> "articles/categories",
			"href"	=> "articles/categories/summary"
		]
    ]
];