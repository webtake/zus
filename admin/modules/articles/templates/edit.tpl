
{if isset($save)}
    {if $save == true}
        <p class="alert alert-success">Uloženo.</p>
        {else}
        <p class="alert alert-danger">Nepovedlo se uložit.</p>
    {/if}
{/if}
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nastavení článku</a></li>
        <li><a href="#content" data-toggle="tab">Obsah</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Název článku</label>
                        <div class="col-md-5">
                            <input id="title" type="text" name="title" class="form-control" value="{$page.title}" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="category" class="col-sm-2 control-label">Kategorie</label>
                        <div class="col-md-5">
                            <select name="category" id="category" class="form-control">
                                {foreach from=$categories item=$cat}
                                    <option value="{$cat.ID}"{if $cat.ID == $page.category} selected{/if}>{$cat.title}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="url" class="col-sm-2 control-label">URL článku</label>
                        <div class="col-md-5">
                            <input id="url" type="text" name="url" class="form-control" value="{$page.url}" />
                        </div>
                    </div>
                   <div class="form-group">
                        <label for="og_img" class="col-sm-2 control-label">Obrázek<br /><small class="text-muted">(zobrazí se při sdílení)</small></label>
                        <div class="col-md-5">
                            <img src="{$page.og_img}" alt="Obrázek" style="max-width: 100px;" />
                            <input id="og_img" type="file" name="og_img" class="form-control" /><br />
                            <a class="btn btn-info" href="{$ROOT}{$active}/edit/{$page.ID}/delete_image"><span class="fa fa-trash"></span> Odstranit obrázek</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="publish_start" class="col-sm-2 control-label">Publikovat článek dne</label>
                        <div class="col-md-5">
                            <input id="publish_start" class="form-control publish" min="{$smarty.now|date_format:"Y-m-d"}" name="publish_start" value="{$page.publish_start|date_format:"Y-m-d"}" type="text" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="akce_start_date" class="col-sm-2 control-label">Začátek akce (Datum a čas)</label>
                        <div class="col-md-5">
                            <input class="form-control datetimepicker" name="akce_start_date" value="{$page.akce_start_date|date_format:"Y-m-d H:i"}" type="text" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="akce_end_date" class="col-sm-2 control-label">Konec akce (Datum a čas)</label>
                        <div class="col-md-5">
                            <input class="form-control datetimepicker" name="akce_end_date" value="{$page.akce_end_date|date_format:"Y-m-d H:i"}" type="text" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="misto_konani" class="col-sm-2 control-label">Místo konání</label>
                        <div class="col-md-5">
                            <input id="misto_konani" type="text" name="misto_konani" value="{$page.misto_konani}" class="form-control" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="content">
                <div class="box-body">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label for="ckeditor" class="label-control col-sm-12">Krátký text (zobrazí se při náhledu článku)</label>
                            <div class="col-sm-12">
                                <textarea name="short_text" class="ckeditor" cols="30" rows="10">{$page.short_text}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label for="ckeditor" class="label-control col-sm-12">Dlouhý text (zobrazí se při zobrazení článku)</label>
                            <div class="col-sm-12">
                                <textarea name="long_text" class="ckeditor" cols="30" rows="10">{$page.long_text}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div>