{extends "index.tpl"}
{block "title"}Články{/block}
{block "content"}
    <section class="content-header">
        {if $state == "summary"}
            <h1>Články</h1>
        {elseif $state == "new"}
            <h1>Nový článek</h1>
        {elseif $state == "edit"}
            <h1>Úprava článku</h1>
        {elseif $state == "delete"}
            <h1>Odstranění článku</h1>
        {/if}
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-body">
                        {if $state == "summary"}
                            <a href="{$ROOT}{$active}/new" class="btn btn-success"><i class="fa fa-plus"></i></a>
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tr>
                                        <th>#</th>
                                        <th>Název</th>
                                        <th>Kategorie</th>
                                        <th>Datum zveřejnění</th>
                                        <th>Akce</th>
                                    </tr>
                                    {foreach item=$item from=$pages}
                                        <tr>
                                            <td>{$item.ID}</td>
                                            <td>{$item.title}</td>
                                            <td>{$item.category}</td>
                                            <td>{$item.created_at|date_format:"d.m.Y H:i:s"}</td>
                                            <td>
                                                <a href="{$ROOT}{$active}/edit/{$item["ID"]}" class="btn btn-info btn-sm"><span class="fa fa-pencil-square-o"></span></a>&nbsp;
                                                <a href="{$ROOT}{$active}/delete/{$item["ID"]}" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></a>
                                            </td>
                                        </tr>
                                        {foreachelse}
                                        <tr>
                                            <td colspan="3">Žádné články nenalezeny. Kliknutím na zelené <strong>+</strong> vytvořte článek.</td>
                                        </tr>
                                    {/foreach}
                                </table>
                            </div>
                        {elseif $state == "new"}
                            {include "./new.tpl"}
                        {elseif $state == "edit"}
                            {include "./edit.tpl"}
                        {elseif $state == "delete"}
                            {include "./delete.tpl"}
                        {/if}
                    </div>
                </div>
            </div>

            {if $state == "summary"}
            <div class="col-md-3">
                <div class="box">
                    <div class="box-body">
                        <h4>Vyhledávání</h4>
                        <input type="text" id="search" placeholder="Zadejte hledaný název článku / kategorii" class="form-control" />
                    </div>
                </div>
            </div>
            {/if}
        </div>
    </section>
{/block}