<?php
    $db = new Database\DB();

    $id = $url->getIndex(2);

    $image = $url->getIndex(3);

    $referer = substr($_SERVER['HTTP_REFERER'], strrpos($_SERVER['HTTP_REFERER'], '/') + 1);

    if($referer == "new") {
        $save = true;
        $smarty->assign("save", $save);
    }

    
    if($image != false){
        $temp = $db->query("UPDATE articles SET og_img = '' WHERE ID = :id");
        $temp->bind(":id", $id);
        $temp->execute();
    }
    if($id == false){
        include __DIR__."/new.php";
        header("Location: /admin/articles/edit/".$db->lastInsertId());
    }else {
        if (isset($_POST["save"])) {
            $save = false;
            $sql = $db->query("UPDATE articles SET 
                                    title = :title, 
                                    short_text = :short_text,
                                    category = :category,
                                    long_text = :long_text, 
                                    url = :url,
                                    publish_start = :publish_start,
                                    akce_start_date = :akce_start_date,
                                    akce_end_date = :akce_end_date,
                                    misto_konani = :misto_konani
                                    WHERE ID = :id");
            $sql->bind(":title", $_POST["title"]);
            $sql->bind(":short_text", $_POST["short_text"]);
            $sql->bind(":category", $_POST["category"]);
            $sql->bind(":long_text", $_POST["long_text"]);
            $url = ($_POST["url"] == "") ? slugify($_POST["title"]) : $_POST["url"];
            $url = uniqueUrl($url, $id);
            $sql->bind(":url", $url);
            $sql->bind(":publish_start", strtotime($_POST["publish_start"]));
            $sql->bind(":akce_start_date", strtotime($_POST["akce_start_date"]));
            $sql->bind(":akce_end_date", strtotime($_POST["akce_end_date"]));
            $sql->bind(":misto_konani", $_POST["misto_konani"]);
            $sql->bind(":id", $id);
            $save = $sql->execute();
            if (is_uploaded_file($_FILES["og_img"]["tmp_name"])) {
                $target_dir = $module->uploadPath;
                $imageFileType = strtolower(pathinfo($_FILES["og_img"]["name"], PATHINFO_EXTENSION));
                $filename = time() . "." . $imageFileType;
                $target_file = $target_dir . $filename;
                $upload = false;
                if (move_uploaded_file($_FILES["og_img"]["tmp_name"], $target_file)) {
                    $sql = $db->query("UPDATE articles SET og_img = :img WHERE ID = :id");
                    $sql->bind(":img", "/images/uploads/" . $filename);
                    $sql->bind(":id", $id);
                    $upload = $sql->execute();
                } else {
                    $upload = false;
                }
                $save = ($save && $upload);
            }
            $smarty->assign("save", $save);
        }
    $sql = $db->query("SELECT * FROM articles WHERE ID = :id");
    $sql->bind(":id", $id);

    $sql->execute();

    if($sql->rowCount() == 1){
        $page = $sql->fetch();
        $smarty->assign("page", $page);
    }else{
        header("Location: ".ROOT."404");
    }

    }
