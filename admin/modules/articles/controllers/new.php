<?php
$db = new Database\DB();


if(isset($_POST["save"])){
    $save = false;

    $sql = $db->query("INSERT INTO articles(title, short_text, long_text, url, created_at, category, lang, publish_start, akce_start_date, akce_end_date, misto_konani) 
                       VALUES(:title, :short_text, :long_text, :url, :created_at, :category, :lang, :publish_start, :akce_start_date, :akce_end_date, :misto_konani)");
    $sql->bind(":title", $_POST["title"]);
    $sql->bind(":short_text", $_POST["short_text"]);
    $sql->bind(":long_text", $_POST["long_text"]);

    $url = ($_POST["url"] == "") ? slugify($_POST["title"]) : $_POST["url"];
    $url = uniqueUrl($url, null);

    $sql->bind(":url", $url);
    $sql->bind(":created_at", date("Y-m-d H:i:s", time()));
    $sql->bind(":category", $_POST["category"]);
    $sql->bind(":lang", $_SESSION["lang"]);
    $sql->bind(":publish_start", strtotime($_POST["publish_start"]));
    $sql->bind(":akce_start_date", strtotime($_POST["akce_start_date"]));
    $sql->bind(":akce_end_date", strtotime($_POST["akce_end_date"]));
    $sql->bind(":misto_konani", $_POST["misto_konani"]);

    $save = $sql->execute();

    if(is_uploaded_file($_FILES["og_img"]["tmp_name"])){
        $target_dir = $module->uploadPath;
        $imageFileType = strtolower(pathinfo($_FILES["og_img"]["name"],PATHINFO_EXTENSION));
        $filename = time() . "." . $imageFileType;
        $target_file = $target_dir . $filename;

        $upload = false;
        if (move_uploaded_file($_FILES["og_img"]["tmp_name"], $target_file)) {
            $id = $db->lastInsertId();

            $sql = $db->query("UPDATE articles SET og_img = :img WHERE ID = :id");
            $sql->bind("img", "/images/uploads/".$filename);
            $sql->bind(":id", $id);
            $upload = $sql->execute();
        } else {
            $upload = false;
        }

        $save = ($save && $upload);
    }

    $smarty->assign("save", $save);
}