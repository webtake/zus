<?php
require __DIR__.'/../../index.php';

$state = $url->getIndex(1) ?? "summary";

$db = new Database\DB();

$module = new Modules\Module($url->getIndex(0));
$smarty = new Smarty;

switch($state){
    case "summary":
        $sql = $db->query("SELECT articles.*, articles_categories.title AS category FROM articles, articles_categories WHERE articles_categories.ID = articles.category AND articles.lang = :lang ORDER BY ID DESC");
        $sql->bind(":lang", $_SESSION["lang"]);
        $sql->execute();
        $smarty->assign("pages", $sql->fetchAll());
        break;
    case "new":
        require __DIR__."/new.php";
        break;
    case "edit":
        require __DIR__."/edit.php";
        break;
    case "delete":
        require __DIR__."/delete.php";
        break;
    case "categories":
        require __DIR__."/categories/index.php";
        break;
    default:
        header("Location: ".ROOT."404");
}

if($state != "categories"){

    $sql = $db->query("SELECT * FROM articles_categories ORDER BY ID ASC");
    $sql->execute();
    $cats = $sql->fetchAll();

    $smarty->assign("categories", $cats);

    $smarty->assign("state", $state);
    $smarty->assign($default);
    $smarty->assign('vars', get_defined_vars());
    $smarty->display($module->template);
}