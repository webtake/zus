<?php

$state = $url->getIndex(2) ?? "summary";

$db = new Database\DB();

$module = new Modules\Module($url->getIndex(0), NULL, 'categories');

switch($state){
    case "summary":
        $sql = $db->query("SELECT * FROM articles_categories WHERE lang = :lang ORDER BY `position` ASC");
        $sql->bind(":lang", $_SESSION["lang"]);
        $sql->execute();
        $smarty->assign("pages", $sql->fetchAll());
        break;
    case "new":
        require __DIR__."/new.php";
        break;
    case "edit":
        require __DIR__."/edit.php";
        break;
    case "delete":
        require __DIR__."/delete.php";
        break;
    default:
        header("Location: ".ROOT."404");
}