<?php
if(!isset($_SESSION["user"]) || $_SESSION["user"]["fullname"] == "") header("Location:".ROOT."login");
$url    = new URL\Parser(str_replace("admin/", "", $_SERVER["REQUEST_URI"]));

$url->getParsedUrl();
$active = $url->getIndex(0);
if($url == false) $active = HOME;


$webVariables = [
    "CSS"       => CSS,
    "IMAGES"    => IMAGES,
    "JS"        => JS,
    "VENDOR"    => VENDOR
];

$db = new Database\DB();

$w_query = $db->query("SELECT * FROM settings");
$w_query->execute();
$w_query = $w_query->fetchAll();

foreach($w_query as $s){
    $webVariables[$s["rule"]] =  $s["value"];
}

$user = $_SESSION["user"];

require_once __DIR__."/load.php";


foreach($modules as $module){
    $temp = ["title" => $module["title"]];
    foreach($module["modules"] as $sub) {
        include __DIR__.'/../modules/'.$sub.'/load.php';
        $sql = $db->query("SELECT level FROM admin_modules WHERE module_folder = :url");
        $sql->bind(":url", $load["url"]);
        $sql->execute();

        if($sql->rowCount() > 0) {
            $level = $sql->fetchColumn();
            if($_SESSION["user"]["level"] >= $level) $temp["subs"][] = $load;

        }
    }
    $menu[] = $temp;
}

/*
foreach($modules as $module){
    include __DIR__.'/../modules/'.$module.'/load.php';
    $sql = $db->query("SELECT level FROM admin_modules WHERE module_folder = :url");
    $sql->bind(":url", $load["url"]);
    $sql->execute();

    if($sql->rowCount() > 0) {
        $level = $sql->fetchColumn();
        if($_SESSION["user"]["level"] >= $level) $menu[] = $load;

    }
}
*/

$langs = $db->query("SELECT * FROM languages ORDER BY ID ASC");
$langs->execute();
$langs = $langs->fetchAll();

$default = [
    "ROOT"          => ROOT,
    "active"        => $active,
    "url"           => $url,
    "W"             => $webVariables,
    "user"          => $user,
    "languages"     => $langs,
    "menu"          => $menu,
    "SESSION"      => $_SESSION
];