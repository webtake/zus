<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:30:57
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/texts/templates/edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40adc172d890_14347295',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9f31ded71375e8173d6d00d7a4ae0e09ac5f9e8b' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/texts/templates/edit.tpl',
      1 => 1547742283,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c40adc172d890_14347295 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['save']->value)) {?>
    <?php if ($_smarty_tpl->tpl_vars['save']->value == true) {?>
        <p class="alert alert-success">Uloženo.</p>
        <?php } else { ?>
        <p class="alert alert-danger">Nepovedlo se uložit.</p>
    <?php }
}?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nastavení textu</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Název textu</label>
                        <div class="col-md-5">
                            <input id="title" type="text" name="title" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['page']->value['title'];?>
" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="variable" class="col-sm-2 control-label">Název proměnné</label>
                        <div class="col-md-5">
                            <input id="variable" type="text" name="variable" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['page']->value['variable'];?>
" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ckeditor" class="col-sm-2 control-label">Text</label>
                        <div class="col-md-5">
                            <textarea id="ckeditor" name="value"><?php echo $_smarty_tpl->tpl_vars['page']->value['value'];?>
</textarea>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div><?php }
}
