<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:28:21
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/galleries/templates/categories/new.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40ad25f3b717_16817115',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3da28645d0db9e9b311e5476b83546cf95a24b04' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/galleries/templates/categories/new.tpl',
      1 => 1547742285,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c40ad25f3b717_16817115 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['save']->value)) {?>
    <?php if ($_smarty_tpl->tpl_vars['save']->value == true) {?>
        <p class="alert alert-success">Uloženo.</p>
        <?php } else { ?>
        <p class="alert alert-danger">Nepovedlo se uložit.</p>
    <?php }
}?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nastavení kategorie</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Název kategorie</label>
                        <div class="col-md-5">
                            <input id="title" type="text" name="title" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-sm-2 control-label">Popis kategorie</label>
                        <div class="col-md-5">
                            <input id="description" type="text" name="description" class="form-control" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div><?php }
}
