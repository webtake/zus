<?php
/* Smarty version 3.1.30, created on 2019-01-20 21:49:10
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/forms/templates/new.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c44ecd65ed027_71998898',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8c8de45da3695ff2a43a993df4350cd1ccd1d7a9' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/forms/templates/new.tpl',
      1 => 1547742279,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c44ecd65ed027_71998898 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['save']->value)) {?>
    <?php if ($_smarty_tpl->tpl_vars['save']->value == true) {?>
        <p class="alert alert-success">Uloženo</p>
    <?php } else { ?>
        <p class="alert alert-danger">Neuloženo.</p>
    <?php }
}?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nastavení formuláře</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Název formuláře</label>
                        <div class="col-md-5">
                            <input id="title" type="text" name="name" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Název tlačítka pro odeslání</label>
                        <div class="col-md-5">
                            <input id="title" type="text" name="sendValue" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="variable" class="col-sm-2 control-label">Zpráva po úspěšném odeslání formuláře</label>
                        <div class="col-md-5">
                            <input id="variable" type="text" name="successMessage" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="columns" class="col-sm-2 control-label">Počet sloupců formuláře</label>
                        <div class="col-md-5">
                            <input type="number" id="columns" name="columns" min="1" max="3" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div><?php }
}
