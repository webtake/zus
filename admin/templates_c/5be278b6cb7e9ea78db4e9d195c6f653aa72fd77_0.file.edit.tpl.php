<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:26:54
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/settings/templates/edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40acce23f312_30905692',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5be278b6cb7e9ea78db4e9d195c6f653aa72fd77' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/settings/templates/edit.tpl',
      1 => 1547742282,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c40acce23f312_30905692 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['save']->value)) {?>
    <?php if ($_smarty_tpl->tpl_vars['save']->value == true) {?>
        <p class="alert alert-success">Uloženo.</p>
        <?php } else { ?>
        <p class="alert alert-danger">Nepovedlo se uložit.</p>
    <?php }
}?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nastavení webové proměnné</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Název nastavení</label>
                        <div class="col-md-5">
                            <input id="title" type="text" name="title" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['page']->value['title'];?>
" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="rule" class="col-sm-2 control-label">Název proměnné</label>
                        <div class="col-md-5">
                            <input id="rule" type="text" name="rule" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['page']->value['rule'];?>
" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="value" class="col-sm-2 control-label">Hodnota</label>
                        <div class="col-md-5">
                            <input type="text" id="value" name="value" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['page']->value['value'];?>
" />
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div><?php }
}
