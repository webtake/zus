<?php
/* Smarty version 3.1.30, created on 2019-01-20 21:49:30
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/forms/templates/edit-input.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c44ecea608bd3_13105108',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '24f9b9f5bc976981c5eccc35acbb4e61cd718b46' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/forms/templates/edit-input.tpl',
      1 => 1547742278,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c44ecea608bd3_13105108 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['save']->value)) {?>
    <?php if ($_smarty_tpl->tpl_vars['save']->value == true) {?>
        <p class="alert alert-success">Uloženo</p>
    <?php } else { ?>
        <p class="alert alert-danger">Neuloženo.</p>
    <?php }
}?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nastavení vstupu</a></li>
        <li><a href="#values" data-toggle="tab">Hodnoty (pouze pro výběrový typ)</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Název vstupu</label>
                        <div class="col-md-5">
                            <input id="name" type="text" name="name" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['page']->value['name'];?>
" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cz_name" class="col-sm-2 control-label">Název vstupu pro email</label>
                        <div class="col-md-5">
                            <input id="cz_name" type="text" name="cz_name" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['page']->value['cz_name'];?>
" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="required" class="col-sm-2 control-label">Je pole povinné?</label>
                        <div class="col-md-5">
                            <input id="required" type="checkbox" name="required" <?php if ($_smarty_tpl->tpl_vars['page']->value['required'] == 1) {?> checked<?php }?> />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="type" class="col-sm-2 control-label">Typ vstupu</label>
                        <div class="col-md-5">
                            <select name="type" id="type" class="form-control">
                                <option value="text"<?php if ($_smarty_tpl->tpl_vars['page']->value['type'] == "text") {?> selected<?php }?>>Krátký text</option>
                                <option value="email"<?php if ($_smarty_tpl->tpl_vars['page']->value['type'] == "email") {?> selected<?php }?>>E-mailová adresa</option>
                                <option value="number"<?php if ($_smarty_tpl->tpl_vars['page']->value['type'] == "number") {?> selected<?php }?>>Číslo (! neplatí pro telefon !)</option>
                                <option value="select"<?php if ($_smarty_tpl->tpl_vars['page']->value['type'] == "select") {?> selected<?php }?>>Výběr mezi hodnotami</option>
                                <option value="textarea"<?php if ($_smarty_tpl->tpl_vars['page']->value['type'] == "textarea") {?> selected<?php }?>>Dlouhý text (např. poznámky atd.)</option>
                                <option value="checkbox"<?php if ($_smarty_tpl->tpl_vars['page']->value['type'] == "checkbox") {?> selected<?php }?>>Zaškrtávací políčko</option>
                                <option value="date"<?php if ($_smarty_tpl->tpl_vars['page']->value['type'] == "date") {?> selected<?php }?>>Datum</option>
                                <option value="time"<?php if ($_smarty_tpl->tpl_vars['page']->value['type'] == "time") {?> selected<?php }?>>Čas</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="values">
                <div class="box-body">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/new-value/<?php echo $_smarty_tpl->tpl_vars['page']->value['ID'];?>
" class="btn btn-success"><i class="fa fa-plus"></i></a>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Hodnota</th>
                                <th>Akce</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['values']->value, 'value');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
?>
                                <tr>
                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['value']->value['ID'];?>

                                    </td>
                                    <td>
                                        <?php echo $_smarty_tpl->tpl_vars['value']->value['value'];?>

                                    </td>
                                    <td>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/edit-value/<?php echo $_smarty_tpl->tpl_vars['value']->value["ID"];?>
" class="btn btn-info btn-sm"><span class="fa fa-pencil-square-o"></span></a>
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/delete-value/<?php echo $_smarty_tpl->tpl_vars['value']->value["ID"];?>
" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></a>
                                    </td>
                                </tr>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div><?php }
}
