<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:27:01
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/pages/templates/new.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40acd5dc51c5_18430815',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0192bb06798fe95c60625d4cf213677b2ccd4a3d' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/pages/templates/new.tpl',
      1 => 1547742281,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c40acd5dc51c5_18430815 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['save']->value)) {?>
    <?php if ($_smarty_tpl->tpl_vars['save']->value == true) {?>
        <p class="alert alert-success">Uloženo</p>
    <?php } else { ?>
        <p class="alert alert-danger">Neuloženo.</p>
    <?php }
}?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nastavení podstránky</a></li>
    </ul>
    <form action="/admin/pages/edit" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Název podstránky</label>
                        <div class="col-md-5">
                            <input id="title" type="text" name="title" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="url" class="col-sm-2 control-label">URL podstránky <br /><small class="text-muted">Pokud nevyplníte, vygeneruje se automaticky</small></label>
                        <div class="col-md-5">
                            <input id="url" type="text" name="url" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="display" class="col-sm-2 control-label">Zobrazit v menu</label>
                        <div class="col-md-5">
                            <input type="checkbox" class="form-check-input" name="display" id="display" checked />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-sm-2 control-label">Popis podstránky<br /><small class="text-muted">(pro vyhledávače)</small></label>
                        <div class="col-md-5">
                            <input id="description" type="text" name="meta_description" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sub_of" class="col-sm-2 control-label">Zařadit do struktury</label>
                        <div class="col-md-5">
                            <select name="sub_of" id="sub_of" class="form-control">
                                <option value="0">Hlavní menu</option>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['allPages']->value, 'p');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['p']->value['ID'];?>
"><?php echo $_smarty_tpl->tpl_vars['p']->value['title'];?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="keywords" class="col-sm-2 control-label">Klíčová slova<br /><small class="text-muted">(pro vyhledávače)</small></label>
                        <div class="col-md-5">
                            <input id="keywords" type="text" name="meta_keywords" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="og_img" class="col-sm-2 control-label">Obrázek<br /><small class="text-muted">(zobrazí se při sdílení)</small></label>
                        <div class="col-md-5">
                            <input id="og_img" type="file" name="og_img" class="form-control" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div><?php }
}
