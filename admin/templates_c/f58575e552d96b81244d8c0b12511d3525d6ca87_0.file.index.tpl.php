<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:31:59
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/modules/templates/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40adff798285_18109654',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f58575e552d96b81244d8c0b12511d3525d6ca87' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/modules/templates/index.tpl',
      1 => 1547742281,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:index.tpl' => 1,
    'file:./edit.tpl' => 1,
  ),
),false)) {
function content_5c40adff798285_18109654 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7587205775c40adff785901_26779626', "title");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15739475415c40adff797777_09412792', "content");
$_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:index.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block "title"} */
class Block_7587205775c40adff785901_26779626 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Nastavení modulů<?php
}
}
/* {/block "title"} */
/* {block "content"} */
class Block_15739475415c40adff797777_09412792 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <section class="content-header">
        <?php if ($_smarty_tpl->tpl_vars['state']->value == "summary") {?>
            <h1>Nastavení modulů</h1>
        <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "edit") {?>
            <h1>Úprava modulu</h1>
        <?php }?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <?php if ($_smarty_tpl->tpl_vars['state']->value == "summary") {?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/new" class="btn btn-success"><i class="fa fa-plus"></i></a>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>#</th>
                                    <th>Název modulu</th>
                                    <th>Složka modulu</th>
                                    <th>Minimální úroveň</th>
                                    <th>Akce</th>
                                </tr>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pages']->value, 'item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>
                                    <tr>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value['ID'];?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value['module_title'];?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value['module_folder'];?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value['level'];?>
</td>
                                        <td>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/edit/<?php echo $_smarty_tpl->tpl_vars['item']->value["ID"];?>
" class="btn btn-info btn-sm"><span class="fa fa-pencil-square-o"></span></a>
                                        </td>
                                    </tr>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                            </table>
                        </div>
                    <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "edit") {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:./edit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php }?>
                </div>
            </div>
        </div>
    </section>
<?php
}
}
/* {/block "content"} */
}
