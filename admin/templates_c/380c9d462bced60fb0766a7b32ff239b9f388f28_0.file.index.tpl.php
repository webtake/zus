<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:25:18
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/dashboard/templates/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40ac6e06d300_11121802',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '380c9d462bced60fb0766a7b32ff239b9f388f28' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/dashboard/templates/index.tpl',
      1 => 1547742277,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:index.tpl' => 1,
  ),
),false)) {
function content_5c40ac6e06d300_11121802 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18991466855c40ac6e068bf2_59270093', "title");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5172683295c40ac6e06c886_84141988', "content");
$_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:index.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block "title"} */
class Block_18991466855c40ac6e068bf2_59270093 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Nástěnka<?php
}
}
/* {/block "title"} */
/* {block "content"} */
class Block_5172683295c40ac6e06c886_84141988 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <section class="content-header">
        <h1>
            Nástěnka
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <p class="alert alert-success">Vítejte v administraci, <strong><?php echo $_smarty_tpl->tpl_vars['user']->value['fullname'];?>
</strong>. Pokračujte pomocí menu na levé straně.</p>
            </div>
        </div>
    </section>
<?php
}
}
/* {/block "content"} */
}
