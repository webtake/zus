<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:24:56
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40ac58d08789_44291121',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6cc045d545f770ff1801e5f56fcf604f5bd0e323' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/index.tpl',
      1 => 1547742272,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c40ac58d08789_44291121 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_replace')) require_once '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/vendor/smarty/plugins/modifier.replace.php';
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!doctype html>
<html lang="cs">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title><?php echo $_smarty_tpl->tpl_vars['W']->value['projectName'];?>
 - Administrace - <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13023240315c40ac58ce4d16_83435553', "title");
?>
</title>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['W']->value['VENDOR'];?>
adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['W']->value['VENDOR'];?>
adminlte/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['W']->value['VENDOR'];?>
adminlte/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['W']->value['VENDOR'];?>
adminlte/css/skins/skin-black-light.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" />

    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['W']->value['CSS'];?>
styles.css?v=2">
    <style>
        .images-container::after {
            content: "";
            display: table;
            clear: both; }

        .images-container .image-container {
            float: left;
            padding: 3px;
            margin-right: 10px;
            margin-bottom: 35px;
            position: relative;
            border: 1px solid #e6eaee;
            overflow: hidden; }
        .images-container .image-container.active {
            border-color: #7867A7; }
        .images-container .image-container:hover .controls {
            bottom: 0;
            opacity: 1; }

        .images-container .controls {
            position: absolute;
            left: 0;
            right: 0;
            opacity: 0;
            bottom: -35px;
            text-align: center;
            height: 35px;
            font-size: 24px;
            transition: bottom 0.2s ease, opacity 0.2s ease;
            background-color: #fff; }
        .images-container .controls::after {
            content: "";
            display: table;
            clear: both; }
        .images-container .controls .control-btn {
            display: inline-block;
            color: #4f5f6f;
            cursor: pointer;
            width: 35px;
            height: 35px;
            line-height: 35px;
            text-align: center;
            opacity: 0.5;
            transition: opacity 0.3s ease; }
        .images-container .controls .control-btn:hover {
            opacity: 1; }
        .images-container .controls .control-btn.move {
            cursor: move; }
        .images-container .controls .control-btn.star {
            color: #FFB300; }
        .images-container .controls .control-btn.star i:before {
            content: "\f006"; }
        .images-container .controls .control-btn.star.active i:before {
            content: "\f005"; }
        .images-container .controls .control-btn.remove {
            color: #FF4444; }

        .images-container .image {
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            width: 130px;
            height: 135px;
            line-height: 135px;
            text-align: center; }

        .images-container .image-container.main {
            border-color: #FFB300; }

        .images-container .image-container.new {
            opacity: 0.6;
            transition: opacity 0.3s ease;
            border-style: dashed;
            border: 1px #7867A7 solid;
            color: #7867A7; }
        .images-container .image-container.new .image {
            font-size: 2.5rem; }
        .images-container .image-container.new:hover {
            opacity: 1; }
    </style>
<body class="skin-black-light sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;?>
" class="logo">
            <span class="logo-mini"><b>ADM</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Administrace</b></span>
        </a>
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span><?php echo $_smarty_tpl->tpl_vars['user']->value['fullname'];?>
</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['W']->value['IMAGES'];?>
default-avatar.png" class="img-circle" alt="User Image">

                                <p>
                                    <?php echo $_smarty_tpl->tpl_vars['user']->value['fullname'];?>

                                    <small><?php echo $_smarty_tpl->tpl_vars['user']->value['post'];?>
</small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['ROOT']->value,'admin/','');?>
" target="_blank" class="btn btn-default btn-flat">Zobrazit web</a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;?>
logout" class="btn btn-default btn-flat">Odhlásit se</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <section class="sidebar">
            <ul class="sidebar-menu" data-widget="tree">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['menu']->value, 'menuitem');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['menuitem']->value) {
?>
                    <li class="header"><?php echo $_smarty_tpl->tpl_vars['menuitem']->value['title'];?>
</li>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['menuitem']->value['subs'], 'item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>
                        <?php if (isset($_smarty_tpl->tpl_vars['item']->value['sub'])) {?>
                            <li class="treeview menu<?php if ($_smarty_tpl->tpl_vars['active']->value == $_smarty_tpl->tpl_vars['item']->value['url']) {?>-open active<?php }?>">
                                <a href="#">
                                    <i class="fa fa-<?php echo $_smarty_tpl->tpl_vars['item']->value['icon'];?>
"></i> <span><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
</span>
                                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                                </a>
                                <ul class="treeview-menu">
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['item']->value['sub'], 'sub');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['sub']->value) {
?>
                                        <li>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['sub']->value['url'];?>
">
                                                <i class="fa fa-<?php echo $_smarty_tpl->tpl_vars['sub']->value['icon'];?>
"></i>
                                                <span><?php echo $_smarty_tpl->tpl_vars['sub']->value['title'];?>
</span>
                                            </a>
                                        </li>
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                                </ul>
                            </li>
                        <?php } else { ?>
                            <li<?php if ($_smarty_tpl->tpl_vars['active']->value == $_smarty_tpl->tpl_vars['item']->value['url']) {?> class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['item']->value['href'];?>
"><i class="fa fa-<?php echo $_smarty_tpl->tpl_vars['item']->value['icon'];?>
"></i> <span><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
</span></a></li>
                        <?php }?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                <li class="header langs">
                    Jazyková verze
                </li>
                <li class="langs">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'lang');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['lang']->value) {
?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;?>
language/<?php echo $_smarty_tpl->tpl_vars['lang']->value['ID'];?>
" style="padding: 0; display: inline">
                            <i class="flag-icon flag-icon-<?php echo $_smarty_tpl->tpl_vars['lang']->value['icon'];
if ($_smarty_tpl->tpl_vars['SESSION']->value['lang'] == $_smarty_tpl->tpl_vars['lang']->value['ID']) {?> active<?php }?>"></i>
                        </a>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7554146495c40ac58d02c88_37603824', "content");
?>

    </div>
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
        </div>
        <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">Almsaeed Studio</a> & <a href="https://webtake.cz/">Webtake.cz</a>.</strong> All rights
        reserved.
    </footer>
</div>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['W']->value['VENDOR'];?>
adminlte/bower_components/jquery/dist/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['W']->value['VENDOR'];?>
adminlte/bower_components/jquery-ui/jquery-ui.min.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['W']->value['VENDOR'];?>
ckeditor/ckeditor.js?v=2"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
    $.widget.bridge('uibutton', $.ui.button);
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['W']->value['VENDOR'];?>
adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['W']->value['VENDOR'];?>
adminlte/js/adminlte.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['W']->value['JS'];?>
pages.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="https://cdn.rawgit.com/prashantchaudhary/ddslick/master/jquery.ddslick.min.js" ><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
>
    $(function () {
        CKEDITOR.replace('ckeditor', {
            contentsCss: [CKEDITOR.basePath + 'contents.css', '/layout/css/styles.css', CKEDITOR.basePath + 'bootstrap.min.css'],
            allowedContent: true
        });

    })
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
    $(document).ready(function(){
        $('#flags').ddslick({
            height: 350,
            width: "100%",
            selectText: "Vyberte jazyk...",
            onSelected: function(data){
                if(data.selectedIndex >= 0) {
                    $('#hidCflag').val(data.selectedData.value);
                }
            }
        });
    });
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
    $("#generate").click(function(){
       var chars    = "qwertzuiopasdfghjklyxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM1234567890";
       var pass     = "";
       var len      = 10;

       for(var i = 0; i < len; i++){
           var x = Math.floor(Math.random() * chars.length);
           pass += chars.charAt(x);
       }
       $("#password").val(pass);
    });
    $("#search").keyup(function () {
        var value = this.value.toLowerCase().trim();

        $("table tr").each(function (index) {
            if (!index) return;
            $(this).find("td").each(function () {
                var id = $(this).text().toLowerCase().trim();
                var not_found = (id.indexOf(value) == -1);
                $(this).closest('tr').toggle(!not_found);
                return not_found;
            });
        });
    });
<?php echo '</script'; ?>
>
</body>
</html><?php }
/* {block "title"} */
class Block_13023240315c40ac58ce4d16_83435553 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block "title"} */
/* {block "content"} */
class Block_7554146495c40ac58d02c88_37603824 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block "content"} */
}
