<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:29:56
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/forms/templates/edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40ad84eba468_35359096',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fe532bc07a0f48851f4217f21895953f0ae12349' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/forms/templates/edit.tpl',
      1 => 1547742279,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c40ad84eba468_35359096 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['save']->value)) {?>
    <?php if ($_smarty_tpl->tpl_vars['save']->value == true) {?>
        <p class="alert alert-success">Uloženo.</p>
        <?php } else { ?>
        <p class="alert alert-danger">Nepovedlo se uložit.</p>
    <?php }
}?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li><a href="#settings" data-toggle="tab">Nastavení formuláře</a></li>
        <li class="active"><a href="#inputs" data-toggle="tab">Vstupy formuláře</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Název formuláře</label>
                        <div class="col-md-5">
                            <input id="title" type="text" name="name" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['page']->value['name'];?>
" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Název tlačítka pro odeslání</label>
                        <div class="col-md-5">
                            <input id="title" type="text" name="sendValue" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['page']->value['sendValue'];?>
" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="variable" class="col-sm-2 control-label">Zpráva po úspěšném odeslání formuláře</label>
                        <div class="col-md-5">
                            <input id="variable" type="text" name="successMessage" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['page']->value['successMessage'];?>
" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="columns" class="col-sm-2 control-label">Počet sloupců formuláře</label>
                        <div class="col-md-5">
                            <input type="number" id="columns" name="columns" value="<?php echo $_smarty_tpl->tpl_vars['page']->value['columns'];?>
" min="1" max="3" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane active" id="inputs">
                <div class="box-body">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/new-input/<?php echo $_smarty_tpl->tpl_vars['page']->value['ID'];?>
" class="btn btn-success"><i class="fa fa-plus"></i></a>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Název vstupu</th>
                                    <th>Pořadí</th>
                                    <th>Akce</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['inputs']->value, 'input');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['input']->value) {
?>
                                    <tr>
                                        <td>
                                            <?php echo $_smarty_tpl->tpl_vars['input']->value['ID'];?>

                                        </td>
                                        <td>
                                            <?php echo $_smarty_tpl->tpl_vars['input']->value['name'];?>

                                        </td>
                                        <td>
                                            <button class="btn btn-warning" onClick="changeInputOrder(<?php echo $_smarty_tpl->tpl_vars['page']->value['ID'];?>
, <?php echo $_smarty_tpl->tpl_vars['input']->value['ID'];?>
, <?php echo $_smarty_tpl->tpl_vars['input']->value['position'];?>
, <?php echo $_smarty_tpl->tpl_vars['input']->value['position']-1;?>
);"><span class="fa fa-caret-up"></span></button>
                                            <button class="btn btn-warning" onClick="changeInputOrder(<?php echo $_smarty_tpl->tpl_vars['page']->value['ID'];?>
, <?php echo $_smarty_tpl->tpl_vars['input']->value['ID'];?>
, <?php echo $_smarty_tpl->tpl_vars['input']->value['position'];?>
, <?php echo $_smarty_tpl->tpl_vars['input']->value['position']+1;?>
);"><span class="fa fa-caret-down"></span></button>
                                        </td>
                                        <td>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/edit-input/<?php echo $_smarty_tpl->tpl_vars['input']->value["ID"];?>
" class="btn btn-info btn-sm"><span class="fa fa-pencil-square-o"></span></a>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/delete-input/<?php echo $_smarty_tpl->tpl_vars['input']->value["ID"];?>
" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></a>
                                        </td>
                                    </tr>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div><?php }
}
