<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:36:25
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/user/templates/edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40af09d4d646_43975399',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd96e0fcc55cb37d6639831bc5271100c7dbcedca' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/user/templates/edit.tpl',
      1 => 1547742283,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c40af09d4d646_43975399 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['save']->value)) {?>
    <?php if ($_smarty_tpl->tpl_vars['save']->value == true) {?>
        <p class="alert alert-success">Uloženo.</p>
    <?php } else { ?>
        <p class="alert alert-danger">Nepovedlo se uložit.</p>
    <?php }
}?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nastavení uživatele</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">Nové heslo</label>
                        <div class="col-md-5">
                            <input id="password" type="text" name="password" class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div><?php }
}
