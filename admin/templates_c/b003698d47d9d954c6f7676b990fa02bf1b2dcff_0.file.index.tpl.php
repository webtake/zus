<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:25:18
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/login/templates/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40ac6e16cf24_54288647',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b003698d47d9d954c6f7676b990fa02bf1b2dcff' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/login/templates/index.tpl',
      1 => 1547742280,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c40ac6e16cf24_54288647 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Administrace</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['W']->value['VENDOR'];?>
adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['W']->value['VENDOR'];?>
adminlte/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['W']->value['VENDOR'];?>
adminlte/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['W']->value['VENDOR'];?>
adminlte/css/AdminLTE.min.css">

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="login-page">

<div class="login-box">
    <div class="login-logo">
        <a href="#"><b>Administrace</b></a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Přihlašte se prosím.</p>

        <form action="#" method="post">
            <div class="form-group has-feedback">
                <input type="text" name="username" class="form-control" placeholder="Uživatelské jméno">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="Heslo">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <!-- /.col -->
                <div class="col-xs-12 text-right">
                    <button type="submit" name="login" class="btn btn-primary btn-block btn-flat">Přihlásit se</button>
                </div>
            </div>
            <?php if (isset($_smarty_tpl->tpl_vars['error']->value)) {?>
                <div class="row">
                    <div class="col-xs-12">
                        <p class="alert alert-danger">
                            Špatně zadané přihlašovací údaje
                        </p>
                    </div>
                </div>
            <?php }?>
        </form>

    </div>
    </div>
</body>
</html>
<?php }
}
