<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:29:49
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/articles/templates/delete.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40ad7db2f5f2_33818638',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ac0ef5be6d10989e7e3a66f352524b11ed4df1f4' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/articles/templates/delete.tpl',
      1 => 1547742275,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c40ad7db2f5f2_33818638 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['success']->value)) {?>
    <?php if ($_smarty_tpl->tpl_vars['success']->value == true) {?>
        <p class="alert alert-success">Odstraněno.</p>
    <?php } else { ?>
        <p class="alert alert-danger">Nepovedlo se odstranit.</p>
    <?php }
} else { ?>

    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <p>Opravdu chcete odstranit článek <?php echo $_smarty_tpl->tpl_vars['page']->value['title'];?>
?</p>
        <button class="btn btn-danger" name="delete"><span class="fa fa-trash"></span> Odstranit</button>
        <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
" class="btn btn-primary btn-flat">Vrátit se zpátky</a>
    </form>
<?php }
}
}
