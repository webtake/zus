<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:29:31
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/articles/templates/edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40ad6b03fb79_73447840',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7d42b6c74781fa78553f41771879261f419db704' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/articles/templates/edit.tpl',
      1 => 1547742275,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c40ad6b03fb79_73447840 (Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if (isset($_smarty_tpl->tpl_vars['save']->value)) {?>
    <?php if ($_smarty_tpl->tpl_vars['save']->value == true) {?>
        <p class="alert alert-success">Uloženo.</p>
        <?php } else { ?>
        <p class="alert alert-danger">Nepovedlo se uložit.</p>
    <?php }
}?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nastavení článku</a></li>
        <li><a href="#content" data-toggle="tab">Obsah</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Název článku</label>
                        <div class="col-md-5">
                            <input id="title" type="text" name="title" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['page']->value['title'];?>
" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="category" class="col-sm-2 control-label">Kategorie</label>
                        <div class="col-md-5">
                            <select name="category" id="category" class="form-control">
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categories']->value, 'cat');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['cat']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['cat']->value['ID'];?>
"<?php if ($_smarty_tpl->tpl_vars['cat']->value['ID'] == $_smarty_tpl->tpl_vars['page']->value['category']) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['cat']->value['title'];?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="url" class="col-sm-2 control-label">URL článku</label>
                        <div class="col-md-5">
                            <input id="url" type="text" name="url" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['page']->value['url'];?>
" />
                        </div>
                    </div>
                   <div class="form-group">
                        <label for="og_img" class="col-sm-2 control-label">Obrázek<br /><small class="text-muted">(zobrazí se při sdílení)</small></label>
                        <div class="col-md-5">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['page']->value['og_img'];?>
" alt="Obrázek" style="max-width: 100px;" />
                            <input id="og_img" type="file" name="og_img" class="form-control" /><br />
                            <a class="btn btn-info" href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/edit/<?php echo $_smarty_tpl->tpl_vars['page']->value['ID'];?>
/delete_image"><span class="fa fa-trash"></span> Odstranit obrázek</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="content">
                <div class="box-body">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label for="ckeditor" class="label-control col-sm-12">Krátký text (zobrazí se při náhledu článku)</label>
                            <div class="col-sm-12">
                                <textarea name="short_text" class="ckeditor" cols="30" rows="10"><?php echo $_smarty_tpl->tpl_vars['page']->value['short_text'];?>
</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label for="ckeditor" class="label-control col-sm-12">Dlouhý text (zobrazí se při zobrazení článku)</label>
                            <div class="col-sm-12">
                                <textarea name="long_text" class="ckeditor" cols="30" rows="10"><?php echo $_smarty_tpl->tpl_vars['page']->value['long_text'];?>
</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div><?php }
}
