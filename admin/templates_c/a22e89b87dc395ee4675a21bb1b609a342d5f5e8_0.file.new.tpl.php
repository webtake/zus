<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:24:59
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/users/templates/new.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40ac5b0aea35_25597509',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a22e89b87dc395ee4675a21bb1b609a342d5f5e8' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/users/templates/new.tpl',
      1 => 1547742284,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c40ac5b0aea35_25597509 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['save']->value)) {?>
    <?php if ($_smarty_tpl->tpl_vars['save']->value == true) {?>
        <p class="alert alert-success">Uloženo</p>
    <?php } else { ?>
        <p class="alert alert-danger">Neuloženo.</p>
    <?php }
}?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nový uživatelský účet</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="username" class="col-sm-2 control-label">Přihlašovací jméno</label>
                        <div class="col-md-5">
                            <input id="username" type="text" name="username" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fullname" class="col-sm-2 control-label">Jméno a příjmení</label>
                        <div class="col-md-5">
                            <input id="fullname" type="text" name="fullname" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="post" class="col-sm-2 control-label">Post</label>
                        <div class="col-md-5">
                            <input id="post" type="text" name="post" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="level" class="col-sm-2 control-label">Úroveň</label>
                        <div class="col-md-5">
                            <input id="level" type="number" name="level" min="0" max="<?php echo $_smarty_tpl->tpl_vars['user']->value['level']-1;?>
" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">Heslo</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control" id="password" name="password" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div><?php }
}
