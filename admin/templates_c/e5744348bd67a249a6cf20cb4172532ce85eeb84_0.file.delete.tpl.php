<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:27:58
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/pages/templates/delete.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40ad0ead4f59_92099256',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e5744348bd67a249a6cf20cb4172532ce85eeb84' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/pages/templates/delete.tpl',
      1 => 1547742281,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c40ad0ead4f59_92099256 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['success']->value)) {?>
    <?php if ($_smarty_tpl->tpl_vars['success']->value == true) {?>
        <p class="alert alert-success">Odstraněno.</p>
    <?php } else { ?>
        <p class="alert alert-danger">Nepovedlo se odstranit.</p>
    <?php }
} else { ?>

    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <p>Opravdu chcete odstranit podstránku <strong><?php echo $_smarty_tpl->tpl_vars['page']->value['title'];?>
</strong>?</p>
        <button class="btn btn-danger" name="delete"><span class="fa fa-trash"></span> Odstranit</button>
        <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
" class="btn btn-primary btn-flat">Vrátit se zpátky</a>
    </form>
<?php }
}
}
