<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:24:56
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/users/templates/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40ac58cde790_48274095',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '35c671b1221b47d290ce7b5e7dba9b45e6d39d78' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/users/templates/index.tpl',
      1 => 1547742284,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:index.tpl' => 1,
    'file:./new.tpl' => 1,
    'file:./edit.tpl' => 1,
    'file:./delete.tpl' => 1,
  ),
),false)) {
function content_5c40ac58cde790_48274095 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13954968805c40ac58cc2ab8_39739895', "title");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6095199205c40ac58cddaf2_52833055', "content");
$_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:index.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block "title"} */
class Block_13954968805c40ac58cc2ab8_39739895 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Uživatelské účty<?php
}
}
/* {/block "title"} */
/* {block "content"} */
class Block_6095199205c40ac58cddaf2_52833055 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <section class="content-header">
        <?php if ($_smarty_tpl->tpl_vars['state']->value == "summary") {?>
            <h1>Uživatelské účty</h1>
        <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "new") {?>
            <h1>Nový uživatelský účet</h1>
        <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "edit") {?>
            <h1>Úprava uživatelského účtu</h1>
        <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "delete") {?>
            <h1>Odstranění uživatelského účtu</h1>
        <?php }?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <?php if ($_smarty_tpl->tpl_vars['state']->value == "summary") {?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/new" class="btn btn-success"><i class="fa fa-plus"></i></a>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>#</th>
                                    <th>Název uživatelského účtu</th>
                                    <th>Jméno a příjmení</th>
                                    <th>Post</th>
                                    <th>Úroveň</th>
                                    <th>Akce</th>
                                </tr>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pages']->value, 'item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>
                                    <tr>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value['ID'];?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value['username'];?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value['fullname'];?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value['post'];?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value['level'];?>
</td>
                                        <td>
                                            <?php if ($_smarty_tpl->tpl_vars['user']->value['level'] >= $_smarty_tpl->tpl_vars['item']->value['level'] && $_smarty_tpl->tpl_vars['user']->value['ID'] != $_smarty_tpl->tpl_vars['item']->value['ID']) {?>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/edit/<?php echo $_smarty_tpl->tpl_vars['item']->value["ID"];?>
" class="btn btn-info btn-sm"><span class="fa fa-pencil-square-o"></span></a>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/delete/<?php echo $_smarty_tpl->tpl_vars['item']->value["ID"];?>
" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></a>
                                                <?php } else { ?>
                                                <small>Nemáte dostatečná oprávnění na manipulaci s tímto uživatelským účtem.</small>
                                            <?php }?>
                                        </td>
                                    </tr>
                                    <?php
}
} else {
?>

                                    <tr>
                                        <td colspan="3">Žádní uživatelé nenalezeni. Kliknutím na zelené <strong>+</strong> vytvořte uživatele.</td>
                                    </tr>
                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                            </table>
                        </div>
                    <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "new") {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:./new.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "edit") {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:./edit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "delete") {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:./delete.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php }?>
                </div>
            </div>
        </div>
    </section>
<?php
}
}
/* {/block "content"} */
}
