<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:29:54
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/forms/templates/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40ad82e6c578_45754551',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8a97301c45313317016f1d8036c92283c5b17280' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/forms/templates/index.tpl',
      1 => 1547742279,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:index.tpl' => 1,
    'file:./new.tpl' => 1,
    'file:./edit.tpl' => 1,
    'file:./delete.tpl' => 1,
    'file:./new-input.tpl' => 1,
    'file:./edit-input.tpl' => 1,
    'file:./delete-input.tpl' => 1,
    'file:./new-value.tpl' => 1,
    'file:./edit-value.tpl' => 1,
    'file:./delete-value.tpl' => 1,
  ),
),false)) {
function content_5c40ad82e6c578_45754551 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21450881285c40ad82e4a865_00466361', "title");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11964221405c40ad82e6b786_00316402', "content");
$_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:index.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block "title"} */
class Block_21450881285c40ad82e4a865_00466361 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Formuláře<?php
}
}
/* {/block "title"} */
/* {block "content"} */
class Block_11964221405c40ad82e6b786_00316402 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <section class="content-header">
        <?php if ($_smarty_tpl->tpl_vars['state']->value == "summary") {?>
            <h1>Všechny formuláře</h1>
        <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "new") {?>
            <h1>Nový formulář</h1>
        <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "edit") {?>
            <h1>Úprava formuláře</h1>
        <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "delete") {?>
            <h1>Odstranění formuláře</h1>
        <?php }?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <?php if ($_smarty_tpl->tpl_vars['state']->value == "summary") {?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/new" class="btn btn-success"><i class="fa fa-plus"></i></a>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>#</th>
                                    <th>Název formuláře</th>
                                    <th>Využití na podstránce</th>
                                    <th>Akce</th>
                                </tr>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pages']->value, 'item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>
                                    <tr>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value['ID'];?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</td>
                                        <td>[[form_<?php echo $_smarty_tpl->tpl_vars['item']->value['code_name'];?>
]]</td>
                                        <td>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/edit/<?php echo $_smarty_tpl->tpl_vars['item']->value["ID"];?>
" class="btn btn-info btn-sm"><span class="fa fa-pencil-square-o"></span></a>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/delete/<?php echo $_smarty_tpl->tpl_vars['item']->value["ID"];?>
" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></a>
                                        </td>
                                    </tr>
                                    <?php
}
} else {
?>

                                    <tr>
                                        <td colspan="3">Žádné formuláře nenalezeny. Kliknutím na zelené <strong>+</strong> vytvořte formulář.</td>
                                    </tr>
                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                            </table>
                        </div>
                    <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "new") {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:./new.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "edit") {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:./edit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "delete") {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:./delete.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "new-input") {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:./new-input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "edit-input") {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:./edit-input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "delete-input") {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:./delete-input.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "new-value") {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:./new-value.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "edit-value") {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:./edit-value.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "delete-value") {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:./delete-value.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php }?>
                </div>
            </div>
        </div>
    </section>
<?php
}
}
/* {/block "content"} */
}
