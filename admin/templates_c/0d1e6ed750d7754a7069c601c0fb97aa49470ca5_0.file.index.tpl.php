<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:28:51
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/articles/templates/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40ad4354a5d7_21786311',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0d1e6ed750d7754a7069c601c0fb97aa49470ca5' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/articles/templates/index.tpl',
      1 => 1547742275,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:index.tpl' => 1,
    'file:./new.tpl' => 1,
    'file:./edit.tpl' => 1,
    'file:./delete.tpl' => 1,
  ),
),false)) {
function content_5c40ad4354a5d7_21786311 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/vendor/smarty/plugins/modifier.date_format.php';
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12643792775c40ad4352a246_67667742', "title");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16628207825c40ad43549a57_63506941', "content");
$_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:index.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block "title"} */
class Block_12643792775c40ad4352a246_67667742 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Články<?php
}
}
/* {/block "title"} */
/* {block "content"} */
class Block_16628207825c40ad43549a57_63506941 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <section class="content-header">
        <?php if ($_smarty_tpl->tpl_vars['state']->value == "summary") {?>
            <h1>Články</h1>
        <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "new") {?>
            <h1>Nový článek</h1>
        <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "edit") {?>
            <h1>Úprava článku</h1>
        <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "delete") {?>
            <h1>Odstranění článku</h1>
        <?php }?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <?php if ($_smarty_tpl->tpl_vars['state']->value == "summary") {?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/new" class="btn btn-success"><i class="fa fa-plus"></i></a>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>#</th>
                                    <th>Název</th>
                                    <th>Kategorie</th>
                                    <th>Datum zveřejnění</th>
                                    <th>Akce</th>
                                </tr>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pages']->value, 'item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>
                                    <tr>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value['ID'];?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value['category'];?>
</td>
                                        <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value['created_at'],"d.m.Y H:i:s");?>
</td>
                                        <td>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/edit/<?php echo $_smarty_tpl->tpl_vars['item']->value["ID"];?>
" class="btn btn-info btn-sm"><span class="fa fa-pencil-square-o"></span></a>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/delete/<?php echo $_smarty_tpl->tpl_vars['item']->value["ID"];?>
" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></a>
                                        </td>
                                    </tr>
                                    <?php
}
} else {
?>

                                    <tr>
                                        <td colspan="3">Žádné články nenalezeny. Kliknutím na zelené <strong>+</strong> vytvořte článek.</td>
                                    </tr>
                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                            </table>
                        </div>
                    <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "new") {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:./new.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "edit") {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:./edit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "delete") {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:./delete.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php }?>
                </div>
            </div>
        </div>
    </section>
<?php
}
}
/* {/block "content"} */
}
