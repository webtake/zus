<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:27:00
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/pages/templates/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40acd4507554_80030642',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'aa165260e2eb732ea6e0853fbf2919623f94af3c' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/pages/templates/index.tpl',
      1 => 1547742281,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:index.tpl' => 1,
    'file:./new.tpl' => 1,
    'file:./edit.tpl' => 1,
    'file:./delete.tpl' => 1,
  ),
),false)) {
function content_5c40acd4507554_80030642 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13627963515c40acd44dae82_46810861', "title");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14599928525c40acd45065d4_98644067', "content");
$_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:index.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block "title"} */
class Block_13627963515c40acd44dae82_46810861 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
Podstránky<?php
}
}
/* {/block "title"} */
/* {block "content"} */
class Block_14599928525c40acd45065d4_98644067 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <section class="content-header">
        <?php if ($_smarty_tpl->tpl_vars['state']->value == "summary") {?>
            <h1>Podstránky</h1>
        <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "new") {?>
            <h1>Nová podstránka</h1>
        <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "edit") {?>
            <h1>Úprava podstránky</h1>
        <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "delete") {?>
            <h1>Odstranění podstránky</h1>
        <?php }?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <?php if ($_smarty_tpl->tpl_vars['state']->value == "summary") {?>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/new" class="btn btn-success"><i class="fa fa-plus"></i></a>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tr>
                                    <th>#</th>
                                    <th>Název podstránky</th>
                                    <th>Pořadí</th>
                                    <th><span class="fa fa-eye"></span></th>
                                    <th>Akce</th>
                                    <th></th>
                                </tr>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pages']->value, 'item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>
                                    <tr>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value['ID'];?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
</td>
                                        <td>
                                            <button class="btn btn-warning" onClick="changeOrder('pages',<?php echo $_smarty_tpl->tpl_vars['item']->value['ID'];?>
, <?php echo $_smarty_tpl->tpl_vars['item']->value['position'];?>
, <?php echo $_smarty_tpl->tpl_vars['item']->value['position']-1;?>
);"><span class="fa fa-caret-up"></span></button>
                                            <button class="btn btn-warning" onClick="changeOrder('pages',<?php echo $_smarty_tpl->tpl_vars['item']->value['ID'];?>
, <?php echo $_smarty_tpl->tpl_vars['item']->value['position'];?>
, <?php echo $_smarty_tpl->tpl_vars['item']->value['position']+1;?>
);"><span class="fa fa-caret-down"></span></button>
                                        </td>
                                        <td>
                                            <?php if ($_smarty_tpl->tpl_vars['item']->value['display'] == 1) {?>
                                                <span class="fa fa-check" style="color: green"></span>
                                            <?php } else { ?>
                                                <span class="fa fa-times" style="color: red"></span>
                                            <?php }?>
                                        </td>
                                        <td>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/edit/<?php echo $_smarty_tpl->tpl_vars['item']->value["ID"];?>
" class="btn btn-info btn-sm"><span class="fa fa-pencil-square-o"></span></a>
                                            <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/delete/<?php echo $_smarty_tpl->tpl_vars['item']->value["ID"];?>
" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></a>
                                        </td>
                                        <?php if (isset($_smarty_tpl->tpl_vars['item']->value['subs'])) {?>
                                            <td>
                                                <button data-toggle="collapse" class="btn btn-default" data-target="#drop_<?php echo $_smarty_tpl->tpl_vars['item']->value['ID'];?>
"><span class="fa fa-angle-down"></span></button>
                                            </td>
                                            <tbody id="drop_<?php echo $_smarty_tpl->tpl_vars['item']->value['ID'];?>
" class="collapse">
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['item']->value['subs'], 'sub');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['sub']->value) {
?>
                                                <tr style="background-color: #eee; height: 24px;">
                                                    <td><?php echo $_smarty_tpl->tpl_vars['sub']->value['ID'];?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
 &rarr; <?php echo $_smarty_tpl->tpl_vars['sub']->value['title'];?>
</td>
                                                    <td>
                                                        <button class="btn btn-warning" onClick="changeOrder('pages',<?php echo $_smarty_tpl->tpl_vars['sub']->value['ID'];?>
, <?php echo $_smarty_tpl->tpl_vars['sub']->value['position'];?>
, <?php echo $_smarty_tpl->tpl_vars['sub']->value['position']-1;?>
);"><span class="fa fa-caret-up"></span></button>
                                                        <button class="btn btn-warning" onClick="changeOrder('pages',<?php echo $_smarty_tpl->tpl_vars['sub']->value['ID'];?>
, <?php echo $_smarty_tpl->tpl_vars['sub']->value['position'];?>
, <?php echo $_smarty_tpl->tpl_vars['sub']->value['position']+1;?>
);"><span class="fa fa-caret-down"></span></button>
                                                    </td>
                                                    <td>
                                                        <?php if ($_smarty_tpl->tpl_vars['item']->value['display'] == 1 && $_smarty_tpl->tpl_vars['sub']->value['display'] == 1) {?>
                                                            <span class="fa fa-check" style="color: green"></span>
                                                        <?php } else { ?>
                                                            <span class="fa fa-times" style="color: red"></span>
                                                        <?php }?>
                                                    </td>
                                                    <td>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/edit/<?php echo $_smarty_tpl->tpl_vars['sub']->value["ID"];?>
" class="btn btn-info btn-sm"><span class="fa fa-pencil-square-o"></span></a>
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/delete/<?php echo $_smarty_tpl->tpl_vars['sub']->value["ID"];?>
" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></a>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                                            </tbody>
                                            <?php } else { ?>
                                            <td></td>
                                        <?php }?>
                                    </tr>
                                    <?php
}
} else {
?>

                                    <tr>
                                        <td colspan="3">Žádné podstránky nenalezeny. Kliknutím na zelené <strong>+</strong> vytvořte podstránku.</td>
                                    </tr>
                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                            </table>
                        </div>
                    <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "new") {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:./new.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "edit") {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:./edit.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['state']->value == "delete") {?>
                        <?php $_smarty_tpl->_subTemplateRender("file:./delete.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                    <?php }?>
                </div>
            </div>
        </div>
    </section>
<?php
}
}
/* {/block "content"} */
}
