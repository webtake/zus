<?php
/* Smarty version 3.1.30, created on 2019-01-17 16:27:06
  from "/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/pages/templates/edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5c40acda8be6f3_75427362',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '22858c76ed07d27af5938680bcd348890808c6e7' => 
    array (
      0 => '/home/uv5w6s7b/projekty.webzmoravy.cz/zus/admin/modules/pages/templates/edit.tpl',
      1 => 1547742281,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c40acda8be6f3_75427362 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['save']->value)) {?>
    <?php if ($_smarty_tpl->tpl_vars['save']->value == true) {?>
        <p class="alert alert-success">Uloženo.</p>
        <?php } else { ?>
        <p class="alert alert-danger">Nepovedlo se uložit.</p>
    <?php }
}?>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#settings" data-toggle="tab">Nastavení podstránky</a></li>
        <li><a href="#content" data-toggle="tab">Obsah</a></li>
    </ul>
    <form action="#" method="POST" class="form-horizontal" enctype="multipart/form-data">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="box-body">
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Název podstránky</label>
                        <div class="col-md-5">
                            <input id="title" type="text" name="title" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['page']->value['title'];?>
" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="url" class="col-sm-2 control-label">URL podstránky</label>
                        <div class="col-md-5">
                            <input id="url" type="text" name="url" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['page']->value['url'];?>
" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="display" class="col-sm-2 control-label">Zobrazit v menu</label>
                        <div class="col-md-5">
                            <input type="checkbox" class="form-check-input" name="display" id="display"<?php if ($_smarty_tpl->tpl_vars['page']->value['display'] == 1) {?> checked<?php }?> />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-sm-2 control-label">Popis podstránky<br /><small class="text-muted">(pro vyhledávače)</small></label>
                        <div class="col-md-5">
                            <input id="description" type="text" name="meta_description" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['page']->value['meta_description'];?>
" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sub_of" class="col-sm-2 control-label">Zařadit do struktury</label>
                        <div class="col-md-5">
                            <select name="sub_of" id="sub_of" class="form-control">
                                <option value="0">Hlavní menu</option>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['allPages']->value, 'p');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['p']->value['ID'];?>
" <?php if ($_smarty_tpl->tpl_vars['page']->value['sub_of'] == $_smarty_tpl->tpl_vars['p']->value['ID']) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['p']->value['title'];?>
</option>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="keywords" class="col-sm-2 control-label">Klíčová slova<br /><small class="text-muted">(pro vyhledávače)</small></label>
                        <div class="col-md-5">
                            <input id="keywords" type="text" name="meta_keywords" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['page']->value['meta_keywords'];?>
" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="og_img" class="col-sm-2 control-label">Obrázek<br /><small class="text-muted">(zobrazí se při sdílení)</small></label>
                        <div class="col-md-5">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['page']->value['og_img'];?>
" alt="Obrázek" style="max-width: 100px;" />
                            <input id="og_img" type="file" name="og_img" class="form-control" /><br />
                            <a class="btn btn-info" href="<?php echo $_smarty_tpl->tpl_vars['ROOT']->value;
echo $_smarty_tpl->tpl_vars['active']->value;?>
/edit/<?php echo $_smarty_tpl->tpl_vars['page']->value['ID'];?>
/delete_image"><span class="fa fa-trash"></span> Odstranit obrázek</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="content">
                <div class="box-body">
                    <div class="col-xs-12">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['blocks']->value, 'block');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['block']->value) {
?>
                        <div id="id_<?php echo $_smarty_tpl->tpl_vars['block']->value['ID'];?>
" class="cntnr" name="id_<?php echo $_smarty_tpl->tpl_vars['block']->value['ID'];?>
">
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <div class="col-md-6">
                                        <a onClick="addContent(<?php echo $_smarty_tpl->tpl_vars['page']->value['ID'];?>
, <?php echo $_smarty_tpl->tpl_vars['block']->value['ID'];?>
)" class="btn btn-primary"><span class="fa fa-plus"></span> Přidat nový odstavec na tuto pozici</a>
                                        <a onClick="removeContent(<?php echo $_smarty_tpl->tpl_vars['block']->value['ID'];?>
)" class="btn btn-danger"><span class="fa fa-trash"></span> Odstranit odstavec</a>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <a onClick="swapContent(this, 0);" class="btn btn-info"><fa class="fa fa-angle-up"></fa></a>
                                        <a onClick="swapContent(this, 1);" class="btn btn-info"><fa class="fa fa-angle-down"></fa></a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Nadpis odstavce</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="title_<?php echo $_smarty_tpl->tpl_vars['block']->value['ID'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['block']->value['title'];?>
" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Je odstavec přes celou šířku?</label>
                                <div class="col-md-5">
                                    <input type="checkbox" name="fullwidth_<?php echo $_smarty_tpl->tpl_vars['block']->value['ID'];?>
" <?php if ($_smarty_tpl->tpl_vars['block']->value['fullwidth'] == 1) {?> checked<?php }?>>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Styl odstavce</label>
                                <div class="col-md-5">
                                    <select name="class_<?php echo $_smarty_tpl->tpl_vars['block']->value['ID'];?>
" class="form-control">
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['classes']->value, 'style');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['style']->value) {
?>
                                            <option value="<?php echo $_smarty_tpl->tpl_vars['style']->value['ID'];?>
"<?php if ($_smarty_tpl->tpl_vars['style']->value['ID'] == $_smarty_tpl->tpl_vars['block']->value['class']) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['style']->value['name'];?>
</option>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-sm-12">
                                        <textarea name="odstavec_<?php echo $_smarty_tpl->tpl_vars['block']->value['ID'];?>
" class="ckeditor" id="odstavec_<?php echo $_smarty_tpl->tpl_vars['block']->value['ID'];?>
" cols="30" rows="10"><?php echo $_smarty_tpl->tpl_vars['block']->value['content'];?>
</textarea>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                        <div name="nonameblock" id="nonameblock">
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <a onClick="addContent(<?php echo $_smarty_tpl->tpl_vars['page']->value['ID'];?>
, 0)" class="btn btn-primary"><span class="fa fa-plus"></span> Přidat nový odstavec na tuto pozici</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <button class="btn btn-success" name="save"><span class="fa fa-floppy-o"></span> Uložit</button>
    </form>
</div><?php }
}
