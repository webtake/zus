/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

// Register a templates definition set named "default".
CKEDITOR.addTemplates( 'default', {
	// The name of sub folder which hold the shortcut preview images of the
	// templates.
	imagesPath: CKEDITOR.getUrl( CKEDITOR.plugins.getPath( 'templates' ) + 'templates/images/' ),

	// The templates definitions.
	templates: [ {
		image: '',
		title: '2 sloupce (1:1)',
		description: '2 sloupce, na mobilní verzi zobrazení pod sebou.',
		html: '<div class="col-12">'+
'	<div class="row">'+
'		<div class="col-12 col-md-6">'+
'			<p>Obsah 1. sloupce</p>'+
'		</div>'+
'		<div class="col-12 col-md-6">'+
'			<p>Obsah 2. sloupce</p>'+
'		</div>'+
'	</div>'+
'</div>'
	},
	{
		title: '2 sloupce (1:2)',
		description: '2 sloupce, na mobilní verzi zobrazení pod sebou.',
		html: '<div class="col-12">'+
'	<div class="row">'+
'		<div class="col-12 col-md-3">'+
'			<p>Obsah 1. sloupce</p>'+
'		</div>'+
'		<div class="col-12 col-md-9">'+
'			<p>Obsah 2. sloupce</p>'+
'		</div>'+
'	</div>'+
'</div>'
	},
	{
		title: '2 sloupce (2:1)',
		description: '2 sloupce, na mobilní verzi zobrazení pod sebou.',
		html: '<div class="col-12">'+
'	<div class="row">'+
'		<div class="col-12 col-md-9">'+
'			<p>Obsah 1. sloupce</p>'+
'		</div>'+
'		<div class="col-12 col-md-3">'+
'			<p>Obsah 2. sloupce</p>'+
'		</div>'+
'	</div>'+
'</div>'
	},
	]
} );
