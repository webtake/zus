/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config
	// The toolbar groups arrangement, optimized for two toolbar rows.
    config.allowedContent = true;
    config.fillEmptyBlocks = false;
    config.autoParagraph = true;
    config.plugins = 'floatpanel,image,indent,indentlist,link,list,magicline,notification,panel,panelbutton,widget,undo,button,colorbutton,colordialog,dialog,dialogui';
	config.extraPlugins = 'ckawesome,justify,templates,stylesheetparser';
	config.fontawesomePath = 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css';
config.filebrowserUploadUrl = window.location.protocol + '//' + window.location.host + '/admin/vendor/ckeditor4.8/filebrowser/upload/upload.php';
    
	config.toolbarGroups = [
		{ name: 'CKAwesome',   groups: [ 'Image', 'ckawesome' ]},
		{ name: 'undo' },
		{ name: 'editing',     groups: [ 'find' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'colors' },
		{ name: 'others' },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'styles' },
	];
	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	config.removeButtons = 'pastefromword, paste';
};
