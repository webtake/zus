/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config )
{
    config.allowedContent = true;
    config.fillEmptyBlocks = false;
    config.autoParagraph = true;
    //config.filebrowserBrowseUrl = '/browser/browse.php';
    config.filebrowserUploadUrl = window.location.protocol + '//' + window.location.host + '/admin/vendor/adminlte/bower_components/ckeditor/filebrowser/upload/upload.php';
    config.toolbar =
[
    { name: 'document',    items : [ 'Source','Templates' ] },
    { name: 'clipboard',   items : [ 'Cut','Copy','PasteText','PasteFromWord','Undo','Redo','RemoveFormat' ] },
    { name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
    { name: 'insert',      items : [ 'Image', 'Table','SpecialChar' ] },
    { name: 'colors',      items : [ 'TextColor','BGColor' ] },
    { name: 'tools',       items : [ 'Maximize', 'ShowBlocks' ] },
    
    '/',
    { name: 'styles',      items : [ 'Format','FontSize' ] },
    { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-' ] },
    { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] }

];
};
