/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.fontawesomePath = 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css';
	config.filebrowserUploadUrl = window.location.protocol + '//' + window.location.host + '/admin/vendor/ckeditor/filebrowser/upload/upload.php';
	config.contentsCss = [CKEDITOR.basePath + 'contents.css', '/layout/css/styles.css', CKEDITOR.basePath + 'bootstrap.min.css'];
	config.filebrowserUploadMethod = 'form';
        
};
