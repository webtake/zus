<div class="owl-carousel owl-theme" id="udalosti">
    {foreach item=$udalost from=$templateVars.udalosti}
        <div class="col-md-12 udalost">
            <a href="/aktuality/udalosti-a-komentare/{$udalost.url}">
            <img src="{$udalost.og_img}" alt="{$udalost.title}">
            <div class="text">
                <h3>{$udalost.title}</h3>
                <p class="text-justify">{$udalost.short_text}</p>
            </div>
            </a>
        </div>
    {/foreach}
</div>
<script>
    $("#udalosti").owlCarousel({
        responsive: {
            0: {
                items: 1
            },
            901: {
                items: 3
            }

        },
        loop: true,
        nav: true,
        autoplay: false,
        autoplayHoverPause: true,
        smartSpeed: 1000,
        dots: true
    });
</script>