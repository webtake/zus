<div id="akce-container"><div class="col-12">
        {assign var="counter" value=0}
        {foreach item=$article from=$templateVars.akce}
            <div class="row akce">
                <div class="col-3">
                    <p class="date">
                        <span class="day">{$article.akce_start_date|date_format:"d"}.</span>
                        <span class="month">
                    {$article.akce_start_date|date_format:"%B"}
                    </span>
                        <span class="time">{$article.akce_start_date|date_format:"H:i"}</span>
                    </p>
                </div>
                <div class="col-9">
                    <h3>{$article.title}</h3>
                    <p>{$article.misto_konani}</p>
                </div>
            </div>
            {foreachelse}
            <p>Nebyly nalezeny žádné akce.</p>
        {/foreach}</div>
</div>