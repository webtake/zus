 <?php
 	$banner = $db->query("SELECT * FROM banner WHERE lang = :lang ORDER BY position DESC");
	$banner->bind(":lang", $_SESSION["lang"]);
	$banner->execute();
	$default["banner"] = $banner->fetchAll();