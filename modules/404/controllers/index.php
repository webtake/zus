<?php
require_once __DIR__.'/../../index.php';

$module = new Modules\Module("404");

$smarty = new Smarty;

$smarty->assign($default);

$smarty->display($module->template);
