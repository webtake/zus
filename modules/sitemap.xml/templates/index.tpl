<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:mobile="http://www.google.com/schemas/sitemap-mobile/1.0">
    {foreach item=$item from=$sitemap}
        <url>
            <loc>{$item.url}</loc>
            <priority>{$item.priority|string_format:"%.3f"}</priority>
        </url>
    {/foreach}
</urlset>