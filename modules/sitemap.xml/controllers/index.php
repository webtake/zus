<?php
require_once __DIR__.'/../../index.php';

$tables = [
    [
    	'table' 	=> 'pages', 
    	'preurl' 	=> '', 
    	'priority' 	=> 0.9
    ],
    [
    	'table'		=> 'articles',
    	'preurl'	=> 'clanek',
    	'priority'	=> 'clanek/'
    ]
];

$module = new Modules\Module("sitemap.xml");
$smarty = new Smarty;
$smarty->assign($default);

$db = new Database\DB;

$sitemap = [];

$sitemap[] = ['url' => ROOT, 'priority' => 1.0];

foreach($tables as $table){
    $temp = $db->query("SELECT * FROM ".$table['table']." ORDER BY ID ASC");
    $temp->execute();
    $temp = $temp->fetchAll();
    foreach($temp as $t){
        $sitemap[] = ['url' => ROOT.$table['preurl'].$t["url"], 'priority' => $table["priority"]];
    }
}

header ('Content-Type: text/xml');

$smarty->assign("sitemap", $sitemap);

$smarty->assign('vars', get_defined_vars());

$smarty->display($module->template);