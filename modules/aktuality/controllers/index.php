<?php
require_once __DIR__.'/../../index.php';

$module = new Modules\Module("aktuality");
$smarty = new Smarty;

$noList = [
  4, 5, 21
];
$sql = $db->query("SELECT * FROM articles_categories ORDER BY `position` ASC");
$sql->execute();

$cats = $sql->fetchAll();

$catIDS = [];

$realCats = [];

foreach($cats as $cat) {
    $catIDS[] = $cat["ID"];
    $realCats[$cat["ID"]] = $cat;
    $realCats[$cat["ID"]]["ignore"] = in_array($cat["ID"], $noList);
}

$query = sprintf("SELECT * FROM articles WHERE category IN (%s) AND publish_start <= UNIX_TIMESTAMP()", implode(",", $catIDS));

$sql = $db->query($query);
$sql->bind(":ids", implode(',', $catIDS));

$clanky = $sql->fetchAll();



foreach($clanky as $clanek) {
    if(empty($realCats[$clanek["category"]]["img"])) $realCats[$clanek["category"]]["img"] = $clanek["og_img"];
    if(empty($clanek["og_img"])) $clanek["og_img"] = "/layout/images/no-image.jpg";
    $realCats[$clanek["category"]]["items"][$clanek["url"]] = $clanek;
}

foreach($realCats as &$cat) {
    if(empty($cat["img"])) $cat["img"] = "/layout/images/no-image.jpg";
}


$data = $realCats;


$smarty->assign("data", $data);

$state = "categories";
$category = false;
$article = false;
if($url->getIndex(1) != false) {
    $state = "category";
    foreach($realCats as $cat) {
        if($cat["url"] == $url->getIndex(1)) {
            $category = $cat;
            break;
        }
    }
    $smarty->assign("category", $category);

}
if($url->getIndex(2) != false) {
    $state = "article";
    $article = $category["items"][$url->getIndex(2)];
    $smarty->assign("article", $article);
}

$smarty->assign("state", $state);

$smarty->assign($default);

$smarty->display($module->template);

