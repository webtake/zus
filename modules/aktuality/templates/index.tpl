{extends "index.tpl"}
{if $state == "categories"}{block "title"}Aktuality{/block}{/if}
{if $state == "category"}{block "title"}Aktuality - {$category.title}{/block}{/if}
{if $state == "article"}{block "title"}Aktuality - {$category.title} - {$article.title}{/block}{/if}
{block "content"}
    <div id="articles" class="teachers">
        <div class="container">
        <div class="row">
            <div class="col-12 pt-3">
                <div class="center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/"><span class="fa fa-home"></span></a></li>
                            {if $state != "categories"}
                                {if $category.ignore != true}
                                    <li class="breadcrumb-item">
                                        <a href="/aktuality">Aktuality</a>
                                    </li>
                                {/if}
                            {else}
                                <li class="breadcrumb-item active" aria-current="page">Aktuality</li>
                            {/if}
                            {if $state == "category" || $state == "article"}
                                {if $state == "article"}
                                    <li class="breadcrumb-item">
                                        <a href="/aktuality/{$category.url}">{$category.title}</a>
                                    </li>
                                {else}
                                    <li class="breadcrumb-item active" aria-current="page">{$category.title}</li>
                                {/if}
                                {if $state == "article"}
                                    <li class="breadcrumb-item active" aria-current="page">{$article.title}</li>
                                {/if}
                            {/if}
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
            <h1 class="text-center">
                {if $state == "categories"}Aktuality{/if}
                {if $state == "category"}{$category.title}{/if}
                {if $state == "article"}{$article.title}{/if}
            </h1>
        {if $state == "categories"}
            {assign var="counter" value=0}
            {foreach item=category from=$data}
                {if $category.ignore != true}
                    {if $counter == 0}
                        <div class="row">
                    {/if}
                    {assign var="counter" value=$counter+1}
                    <div class="col-md-3 item kategorie">
                        <a href="/aktuality/{$category.url}">
                            <img src="{$category.img}" alt="{$category.title}" />
                            <h3>{$category.title}</h3>
                        </a>
                    </div>
                    {if $counter == 4}
                        </div>
                        {assign var="counter" value=0}
                    {/if}

                {/if}
            {/foreach}
            {if $counter > 0}
                </div>
            {/if}
        {/if}
        {if $state == "category"}
            {assign var="counter" value=0}
            {foreach item=article from=$category.items}
                {if $counter == 0}
                    <div class="row">
                {/if}
                {assign var="counter" value=$counter+1}
                <div class="col-md-4 item clanek">
                    <a href="/aktuality/{$category.url}/{$article.url}">
                        <img src="{$article.og_img}" alt="{$article.title}" />
                        <h3>{$article.title}</h3>
                    </a>
                    <div class="row">
                        <div class="col-6">
                            <p class="date">
                                <span class="fa fa-calendar"></span>&nbsp;{$article.created_at|date_format:"d.m.Y"}
                            </p>
                        </div>
                    </div>
                    <hr>
                    <p>{$article.short_text}</p>
                    <div class="row">
                        <div class="col-12 text-right">
                            <a href="/aktuality/{$category.url}/{$article.url}" class="readmore">Číst více...</a>
                        </div>
                    </div>
                </div>
                {if $counter == 3}
                    </div>
                    {assign var="counter" value=0}
                {/if}
            {/foreach}
            {if $counter > 0}
                </div>
            {/if}
        {/if}
        {if $state == "article"}
            {assign var="counter" value=0}
            <div id="article">
                <p class="py-4">
                    <span class="fa fa-calendar"></span> <big class="text-blue">{$article.publish_start|date_format:'d.m.Y'}</big>
                </p>
                <div class="content">
                        <div class="col-12">
                            {$article.long_text}
                        </div>
                </div>
            </div>
        {/if}
        </div>
    </div>
{/block}