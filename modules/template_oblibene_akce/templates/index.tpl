<div id="oblibene-container">
    {assign var="counter" value=0}
        {foreach item=$article from=$templateVars.oblibene}
            {if $counter == 0}
                <div class="row">
            {/if}
            <div class="col-md-6 oblibene">
                <a href="/aktuality/oblibene-akce/{$article.url}" title="{$article.title}"><img src="{$article.og_img}" alt="{$article.title}" />
                <p class="date">{$article.akce_start_date|date_format:"d/m/Y"}</p>
                <h3 class="title">{$article.title}</h3></a>
            </div>
            {assign var="counter" value=$counter+1}
            {if $counter == 2}
                </div>
                {assign var="counter" value=0}
            {/if}
            {foreachelse}
            <p>Nebyly nalezeny žádné články.</p>
        {/foreach}
        {if $counter > 0}
    </div>
    {/if}
</div>