<div class="col-12 teachers">
    {assign var=counter value=0}

    {foreach item=ucitel from=$templateVars.obor}
        {if $counter == 0}
            <div class="row">
        {/if}
        <div class="col-12 col-md-3">
            <p><a href="/clanek/{$ucitel.url}"><img alt="{$ucitel.title}" src="{$ucitel.og_img}" style="width:100%" /></a></p>

            <h3 style="text-align:center"><a href="/clanek/{$ucitel.url}">{$ucitel.title}</a></h3>

            <p style="text-align:center"><a href="/clanek/{$ucitel.url}">{$ucitel.short_text|strip_tags}</a></p>
        </div>
        {assign var=counter value=$counter+1}
        {if $counter == 4}
            </div>
            {assign var=counter value=0}
        {/if}
    {/foreach}
</div>
