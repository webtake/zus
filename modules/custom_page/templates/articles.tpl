<div class="col-12 articles">
	<div class="row">
	{foreach item=$gallery from=$allArticles}
		<div class="col-6 col-sm-4 col-lg-3 article-item">
			<a href="clanek/{$gallery.url}">
				<img src="{$gallery.og_img}" class="img-fluid">
				<h3>{$gallery.title}</h3>
			</a>
		</div>
	{/foreach}
	</div>
</div>