<div class="col-12 gallery">
	<div class="row">
	{foreach item=$gallery from=$galleries}
		<div class="col-6 col-sm-4 col-lg-3 gallery-item">
			<a href="{$gallery.url}" data-lightbox="galerie">
				<img src="{$gallery.og_img}" class="img-fluid">
				<h3>{$gallery.title}</h3>
			</a>
		</div>
	{/foreach}
	</div>
</div>