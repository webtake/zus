<div class="col-12 gallery">
	<div class="row">
	{foreach item=$img from=$gallery}
		<div class="col-6 col-sm-4 col-lg-3 gallery-item">
			<a href="{$img.url}" data-lightbox="galerie">
				<img src="{$img.url}" class="img-fluid">
			</a>
		</div>
	{/foreach}
	</div>
</div>