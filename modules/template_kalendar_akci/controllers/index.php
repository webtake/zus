<?php

$db = new \Database\DB;

$query = $db->query("SELECT * FROM articles WHERE category = 4 OR category = 5 ORDER BY akce_start_date ASC");
$query->execute();

$udalosti = $query->fetchAll();

foreach($udalosti as &$event) {
    $event["month"] = date("n", $event["akce_start_date"]);
    $event["year"] = date("Y", $event["akce_start_date"]);
}



$templateVars["udalosti"] = $udalosti;