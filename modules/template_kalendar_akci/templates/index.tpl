{assign var="year"  value=$smarty.now|date_format:"Y"}

{assign var="yearCount" value=2}

<div id="calendar">
    <div class="row">
        <div class="years">
            {for $foo = 0 to $yearCount - 1}
                <button {if $foo == 0}class="active"{/if} data-year="{$year + $foo}">{$year+$foo}</button>
            {/for}
        </div>
    </div>
    {for $foo = 0 to $yearCount - 1}
        {assign var="year" value=$year+$foo}
        <div class="months {if $year == $smarty.now|date_format:"Y"}active{/if}" data-year="{$year}">
            <div class="row">
                <div class="months_buttons">
                    <button{if 1 == $smarty.now|date_format:"m"} class="active"{/if} data-target="month_1_{$year}">Leden</button>
                    <button{if 2 == $smarty.now|date_format:"m"} class="active"{/if} data-target="month_2_{$year}">Únor</button>
                    <button{if 3 == $smarty.now|date_format:"m"} class="active"{/if} data-target="month_3_{$year}">Březen</button>
                    <button{if 4 == $smarty.now|date_format:"m"} class="active"{/if} data-target="month_4_{$year}">Duben</button>
                    <button{if 5 == $smarty.now|date_format:"m"} class="active"{/if} data-target="month_5_{$year}">Květen</button>
                    <button{if 6 == $smarty.now|date_format:"m"} class="active"{/if} data-target="month_6_{$year}">Červen</button>
                    <button{if 7 == $smarty.now|date_format:"m"} class="active"{/if} data-target="month_7_{$year}">Červenec</button>
                    <button{if 8 == $smarty.now|date_format:"m"} class="active"{/if} data-target="month_8_{$year}">Srpen</button>
                    <button{if 9 == $smarty.now|date_format:"m"} class="active"{/if} data-target="month_9_{$year}">Září</button>
                    <button{if 10 == $smarty.now|date_format:"m"} class="active"{/if} data-target="month_10_{$year}">Říjen</button>
                    <button{if 11 == $smarty.now|date_format:"m"} class="active"{/if} data-target="month_11_{$year}">Listopad</button>
                    <button{if 12 == $smarty.now|date_format:"m"} class="active"{/if} data-target="month_12_{$year}">Prosinec</button>
                </div>
            </div>
            {assign var="monthNumber"  value=1}
            {for $bar=1 to 12}
                <div class="month{if $bar == $smarty.now|date_format:"m"} active{/if}" id="month_{$bar}_{$year}">
                    <div id="akce-container">
                        {assign var="counter" value=0}
                        {foreach item=$article from=$templateVars.udalosti}
                            {if $article.month == $bar && $article.year == $year}
                                {assign var="counter" value=$counter+1}
                                <div class="row akce">
                                    <div class="col-2">
                                        <p class="date">
                                            <span class="day">{$article.akce_start_date|date_format:"d"}.</span>
                                            <span class="month">
                                            {$article.akce_start_date|date_format:"%B"}
                                        </span>
                                            <span class="time">{$article.akce_start_date|date_format:"H:i"}</span>
                                        </p>
                                    </div>
                                    <div class="col-9">
                                        <h3>{$article.title}</h3>
                                        <p>{$article.misto_konani}</p>
                                    </div>
                                </div>
                            {/if}
                        {/foreach}
                        {if $counter == 0}
                            <p>Nebyly nalezeny žádné akce.</p>
                        {/if}
                    </div>

                </div>
            {/for}
        </div>
    {/for}

    {*
    <div class="months" data-year="{$year+1}">
        <button data-target="month_1_{$year+1}">Leden</button>
        <button data-target="month_2_{$year+1}">Únor</button>
        <button data-target="month_3_{$year+1}">Březen</button>
        <button data-target="month_4_{$year+1}">Duben</button>
        <button data-target="month_5_{$year+1}">Květen</button>
        <button data-target="month_6_{$year+1}">Červen</button>
        <button data-target="month_7_{$year+1}">Červenec</button>
        <button data-target="month_8_{$year+1}">Srpen</button>
        <button data-target="month_9_{$year+1}">Září</button>
        <button data-target="month_10_{$year+1}">Říjen</button>
        <button data-target="month_11_{$year+1}">Listopad</button>
        <button data-target="month_12_{$year+1}">Prosinec</button>
    </div>
    *}

</div>