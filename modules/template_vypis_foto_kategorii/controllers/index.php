<?php
$db = new Database\DB;

if($url->getIndex(1) == false){
    $banner = $db->query("SELECT * FROM photogalleries_categories ORDER BY `position` DESC");
    $banner->execute();
    $banner = $banner->fetchAll();
    $templateVars["categories"] = true;
    foreach($banner as &$item) {
        $sql = $db->query("SELECT ID FROM photogalleries WHERE category = :cat_id LIMIT 0, 1");
        $sql->bind(":cat_id", $item["ID"]);
        $sql->execute();
        $cat = $sql->fetchColumn();

        $img = $db->query("SELECT url FROM photogalleries_images WHERE ID_gallery = :gal_id LIMIT 0, 1");
        $img->bind(":gal_id", $cat);
        $img->execute();

        $img = $img->fetchColumn();

        $item["img"] = $img;
    }
    $templateVars["kategorie"] = $banner;
}else{
    if($url->getIndex(2) == false) {
        $templateVars["categories"]         = false;
        $templateVars["category"]           = true;

        $cat = $db->query("SELECT * FROM photogalleries_categories WHERE url = :url");
        $cat->bind(":url", $url->getIndex(1));
        $cat->execute();
        $cat = $cat->fetch();

        $templateVars["parent"] = $cat;

        $templateVars["parentCat"] = $cat;

        $banner = $db->query("SELECT * FROM photogalleries WHERE category = :cat ORDER BY title ASC");
        $banner->bind(":cat", $cat["ID"]);
        $banner->execute();
        $banner = $banner->fetchAll();

        foreach($banner as &$item) {
            $img = $db->query("SELECT url FROM photogalleries_images WHERE ID_gallery = :gal_id LIMIT 0, 1");
            $img->bind(":gal_id", $item["ID"]);
            $img->execute();

            $img = $img->fetchColumn();

            $item["img"] = $img;
        }

        $templateVars["kategorie"] = $banner;
    }else{
        if($url->getIndex(3) == false) {
            $cat = $db->query("SELECT * FROM photogalleries_categories WHERE url = :url");
            $cat->bind(":url", $url->getIndex(1));
            $cat->execute();
            $cat = $cat->fetch();
            $templateVars["parentCat"] = $cat;

            $parent = $db->query("SELECT * FROM photogalleries WHERE url = :url AND category = :cat_id LIMIT 0, 1");
            $parent->bind(":url", $url->getIndex(2));
            $parent->bind(":cat_id", $cat["ID"]);
            $parent->execute();
            $parent = $parent->fetch();

            $templateVars["parent"] = $parent;

            $images = $db->query("SELECT * FROM photogalleries_images WHERE ID_gallery = :id ORDER BY ID ASC");
            $images->bind(":id", $parent["ID"]);
            $images->execute();
            $images = $images->fetchAll();

            $templateVars["images"] = $images;

            $templateVars["categories"]     = false;
            $templateVars["category"]       = false;
            $templateVars["photogallery"]   = true;
        }
    }
}