<div class="center">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/"><span class="fa fa-home"></span></a></li>
            {if $templateVars.categories == false}
                <li class="breadcrumb-item">
                    <a href="/fotogalerie">Fotogalerie</a>
                </li>
            {else}
                <li class="breadcrumb-item active" aria-current="page">Fotogalerie</li>
            {/if}
            {if $templateVars.category == true || $templateVars.photogallery == true}
                {if $templateVars.category == false}
                    <li class="breadcrumb-item">
                        <a href="/fotogalerie/{$templateVars.parentCat.url}">{$templateVars.parentCat.title}</a>
                    </li>
                {else}
                    <li class="breadcrumb-item active" aria-current="page">{$templateVars.parentCat.title}</li>
                {/if}
                {if $templateVars.photogallery == true}
                    <li class="breadcrumb-item active" aria-current="page">{$templateVars.parent.title}</li>
                {/if}
            {/if}
        </ol>
    </nav>
</div>
<div class="col-12" id="galleries">
    {assign var=counter value=0}
    {if $templateVars.categories == true}
        <h1 class="text-center mb-5">Fotogalerie</h1>
        {foreach item=kategorie from=$templateVars.kategorie}
            {if $counter == 0}
                <div class="row">
            {/if}
            <div class="col-12 col-md-3 category-box" onClick="window.location.href='/fotogalerie/{$kategorie.url}'">
                <h3 style="text-align:center"><a href="/fotogalerie/{$kategorie.url}">{$kategorie.title}</a></h3>
            </div>
            {assign var=counter value=$counter+1}
            {if $counter == 4}
                </div>
                {assign var=counter value=0}
            {/if}
        {/foreach}
    {/if}
    {if $templateVars.category == true}
        <h1 class="text-center mb-5">{$templateVars.parent.title}</h1>
        {foreach item=kategorie from=$templateVars.kategorie}
            {if $counter == 0}
                <div class="row">
            {/if}
            <div class="col-12 col-md-3">
                <p><a href="/fotogalerie/{$templateVars.parent.url}/{$kategorie.url}"><img alt="{$kategorie.title}" src="{$kategorie.img}" style="width:100%" /></a></p>

                <h3 style="text-align:center"><a href="#">{$kategorie.title}</a></h3>
            </div>
            {assign var=counter value=$counter+1}
            {if $counter == 4}
                </div>
                {assign var=counter value=0}
            {/if}
        {/foreach}
    {/if}
    {if $templateVars.photogallery == true}
        <h1 class="text-center mb-5">{$templateVars.parent.title}</h1>
        {foreach item=img from=$templateVars.images}
            {if $counter == 0}
                <div class="row">
            {/if}
            <div class="col-12 col-md-3">
                <p><a href="{$img.url}" data-lightbox="{$templateVars.parent.title}"><img alt="{$templateVars.parent.title}" src="{$img.url}" /></a></p>
            </div>
            {assign var=counter value=$counter+1}
            {if $counter == 4}
                </div>
                {assign var=counter value=0}
            {/if}
        {/foreach}
    {/if}
</div>
